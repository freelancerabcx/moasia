<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$params['bootstrap'][] = 'debug';
$params['modules']['debug'] = 'yii\debug\Module';

use \yii\web\Request;
$baseUrl = str_replace('/sadmin/web', '/admin', (new Request)->getBaseUrl());

return [
    'id' => 'app-sadmin',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'debug'],
    'defaultRoute' => 'site',
    'controllerNamespace' => 'sadmin\controllers',
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
        ],
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_sadminUser', // unique for backend
                'path'=>'/sadmin/web'  // correct path for the backend app.
            ]
        ],
        'session' => [
            'name' => '_sadminSessionId', // unique for backend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => [
            'baseUrl' => $baseUrl,
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => []
        ]// backend, under components array
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];