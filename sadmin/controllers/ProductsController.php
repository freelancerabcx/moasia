<?php

namespace sadmin\controllers;

use Yii;
use common\models\TblProducts;
use common\models\TblProducstSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\generalHelper;
use common\models\TblPrices;
use common\models\User;

/**
 * ProductsController implements the CRUD actions for TblProducts model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view', 'update', 'delete', 'create'],
                        'roles'   => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::ROLE_SADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TblProducts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblProducstSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblProducts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblProducts();

        if(!empty($_POST) && $_POST['TblProducts']['alias'] == ""){
            $_POST['TblProducts']['alias'] = generalHelper::alias($_POST['TblProducts']['name']);
        }

        if ($model->load($_POST) && $model->save()) {
            if($_FILES['img']['name'] != "")
                move_uploaded_file($_FILES["img"]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/sadmin/web/files/img/product/".$model->id.".jpg");
            
            if(!empty($_POST) && $_POST['TblProducts']['prices'] != null){
                \Yii::$app
                ->db
                ->createCommand()
                ->delete('tbl_prices', ['product_id' => $model->id])
                ->execute();
                foreach ($_POST['TblProducts']['prices'] as $k => $v) {
                    if($v != ''){
                        $md = new TblPrices;
                        $md->product_id = $model->id;
                        $md->country_id = $_POST['TblProducts']['country_ids'][$k];
                        $md->price = $v;
                        $md->price_pv = $_POST['TblProducts']['prices_pv'][$k];
                        $md->updated_at = time();
                        $md->save();
                    }
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
               'model' => array(
                        "md" => $model,
                        "prices" => []
                    )
            ]);
        }
    }

    /**
     * Updates an existing TblProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(!empty($_POST) && $_POST['TblProducts']['alias'] == "")
            $_POST['TblProducts']['alias'] = generalHelper::alias($_POST['TblProducts']['name']);

        if(!empty($_POST) && $_POST['TblProducts']['prices'] != null){
            \Yii::$app
            ->db
            ->createCommand()
            ->delete('tbl_prices', ['product_id' => $id])
            ->execute();
            foreach ($_POST['TblProducts']['prices'] as $k => $v) {
                if($v != ''){
                    $md = new TblPrices;
                    $md->product_id = $id;
                    $md->country_id = $_POST['TblProducts']['country_ids'][$k];
                    $md->price = $v;
                    $md->price_pv = $_POST['TblProducts']['prices_pv'][$k];
                    $md->updated_at = time();
                    $md->save();
                }
            }
        }

        if ($model->load($_POST) && $model->save()) {
            if($_FILES['img']['name'] != "")
                move_uploaded_file($_FILES["img"]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/sadmin/web/files/img/product/".$model->id.".jpg");
            

            return $this->redirect(['index']);
        } else {

            return $this->render('update', [
                'model' => array(
                            "md" => $model,
                            "prices" => TblPrices::getList($id)
                        )
            ]);
        }
    }

    /**
     * Deletes an existing TblProducts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
