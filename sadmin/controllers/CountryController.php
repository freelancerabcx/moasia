<?php

namespace sadmin\controllers;

use Yii;
use common\helpers\processHelper;
use common\models\TblProductOrders;
use common\models\TblCountry;
use common\models\TblCountrySearch;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CountryController implements the CRUD actions for TblCountry model.
 */
class CountryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view', 'update', 'delete', 'create'],
                        'roles'   => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::ROLE_SADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TblCountry models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblCountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblCountry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblCountry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblCountry();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'data_person' => array(
                    "unit" => 0,
                    "amount" => 0,
                    "total" => 0,
                    "bronze" => array(),
                    "silver" => array(),
                    "gold" => array(),
                    "platinum" => array(),
                    "diamond" => array(),
                )
            ]);
        }
    }

    /**
     * Updates an existing TblCountry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $country = TblCountry::find()->where("id = ".$id)->one();
            $users = User::find()
                    ->select([
                        'tbl_users.*',
                        'SUM(con_amount) AS total'
                    ])
                    ->andWhere(['=','tbl_users.country_id',$id])
                    ->leftJoin('tbl_product_orders','`tbl_product_orders`.`user_id` = `tbl_users`.`id`')
                    ->groupBy('`tbl_users`.`id`')
                    ->all();
            $data_person = processHelper::mlnListUser($users,$country);
            return $this->render('update', [
                'model' => $model,
                'data_person' => $data_person
            ]);
        }
    }


    /**
     * Deletes an existing TblCountry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblCountry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblCountry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblCountry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
