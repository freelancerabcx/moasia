<?php

namespace sadmin\controllers;

use Yii;
use common\models\TblProductOrders;
use common\models\TblProductOrdersSearch;
use common\models\TblProductReceivedLogs;
use common\models\TblProductReceivedLogsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;

/**
 * OrdersController implements the CRUD actions for TblProductOrders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view', 'update', 'delete', 'create', 'received-history'],
                        'roles'   => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (in_array(Yii::$app->user->identity->role_id, User::getRoleID(true)) && Yii::$app->user->identity->role_id != User::ROLE_ADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TblProductOrders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblProductOrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all TblProductOrders models.
     * @return mixed
     */
    public function actionReceivedHistory($id)
    {
        $searchModel = new TblProductReceivedLogsSearch();
        $params = Yii::$app->request->queryParams;
        $params['TblProductReceivedLogsSearch']['order_id'] = $id;
        $dataProvider = $searchModel->search($params);

        return $this->render('receivedHistory', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'order_id' => $id,
        ]);
    }

    /**
     * Displays a single TblProductOrders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblProductOrders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblProductOrders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TblProductOrders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->quantity == $model->received_quantity)
        {
            Yii::$app->session->setFlash('error', 'This Order('.$id.") has been completely delivered!");
            return $this->redirect('index');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $md = new TblProductReceivedLogs();
            $md->product_id = $model->product_id;
            $md->order_id = $model->id;
            $md->quantity = $model->received_quantity;
            $md->total_ordered = $model->quantity;
            $md->created_at = time();
            $md->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TblProductOrders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblProductOrders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblProductOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblProductOrders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
