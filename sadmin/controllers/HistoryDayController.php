<?php

namespace sadmin\controllers;

use Yii;
use common\models\User;
use common\models\TblHistoryDay;
use common\models\TblHistoryDaySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * HistoryDayController implements the CRUD actions for TblHistoryDay model.
 */
class HistoryDayController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::ROLE_SADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                ],
            ],
        ];
    }
    /**
     * Lists all TblHistoryDay models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblHistoryDaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Finds the TblHistoryDay model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblHistoryDay the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblHistoryDay::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
