<?php
namespace sadmin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Request;
use yii\data\Pagination;
use common\models\LoginForm;
use common\models\User;
use common\models\TblPaidTransaction;
use common\models\TblWallets;
use common\models\TblRank;
use common\models\TblConfig;
use common\models\TblHistoryBonus;
use common\models\TblProducts;
use common\models\TblProductOrders;
use common\models\TblCountry;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\helpers\generalHelper;
use common\helpers\processHelper;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['dashboard', 'product', 'income','history','update-bonus','export'],
                        'allow' => true,
                        'roles'   => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::ROLE_SADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionProduct()
    {
        $countries = TblCountry::find()->all();
        foreach($countries as $key => $country){
            $products = TblProducts::find()->joinWith(["prices"])->where("country_id = ".$country->id)->all();
            foreach ($products as $k => $v) {
                $products[$k]->today_orders = count(TblProductOrders::find()->where("product_id = ".$v->id." AND created_at >= ".strtotime(date('Y-m-d')))->all());
            }
            $countries[$key]->today_orders = $products;
        }
        return $this->render('product',[
            'data' => $countries
        ]);
    }

    private $tm = [];

    public function actionUp()
    {
        $this->recursive(9);
        $ids = implode(",", $this->tm);
        Yii::$app->db->createCommand('UPDATE tbl_users SET level = level + 1 WHERE id IN ('.$ids.')')->execute();
        //return $this->render('index');
    }

    function recursive($id){
        $x = User::find()->where('id = '.$id)->one();
        if($x){
            $this->tm[] = $x->parent_id;
            if($x->parent_id >0)
                $this->recursive($x->parent_id);    
        }
    }

    public function actionIndex()
    {
        if(Yii::$app->user->identity->role_id == User::ROLE_ADMIN)
            return $this->redirect(['user/index']);

        if(Yii::$app->user->identity->role_id == User::ROLE_PADMIN)
            return $this->redirect(['orders/index']);

        $countries = TblCountry::find()->all();
        foreach($countries as $key => $country){
            $country_data = processHelper::countryAna($country->id);
            $countries[$key]->today_orders = $country_data['order_today'];
            $countries[$key]->after_bonus = $country_data['bonus_today'];
            $countries[$key]->today_registers = count(User::find()->where(['>=','created_at',strtotime(date('Y-m-d'))])->andWhere(['=','tbl_users.country_id', $country->id])->all());
            $countries[$key]->total_user = count(User::find()->where(['=','tbl_users.country_id', $country->id])->all());
            $countries[$key]->active_user = count(User::find()->where(['=','tbl_users.country_id', $country->id])->andWhere(['>','rank_id',0])->all());
        }
        return $this->render('index',[
            'data' => $countries
        ]);
    }

    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionHistory()
    {
        $query = TblHistoryBonus::find()->orderBy('created_at desc');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();

        return $this->render('history', [
             'models' => $models,
             'pages' => $pages,
        ]);
    }
    public function actionUpdateBonus()
    {
        $id = $_GET['type'];
        if($id==1){
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction(); //get parent user
            try{
                $data = TblHistoryBonus::find()->select([
                    '*','SUM(amount) AS total'
                ])->where(['=','payment_status','1'])
                  ->andWhere(['=','wallet',TblWallets::TYPE_BONUS])
                  ->groupBy('user_id')->all();
                foreach($data as $user_bonus){
                    $wallet = TblWallets::find()->where(['user_id' => $user_bonus->user_id])
                                            ->andWhere([ 'type'=> TblWallets::TYPE_BONUS ])->one();
                    $wallet->amount -= $user_bonus->total;
                    $wallet->save();
                }
                TblHistoryBonus::updateAll(['payment_status' => '2'],[ 'payment_status' => '1']);
                $transaction->commit();
            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }else{
            TblHistoryBonus::updateAll(['payment_status' => '1'],[ 'payment_status' => '0']);
        }
        echo 'Done <input action="action" type="button" value="Back to report" onclick="history.go(-1);" />';
    }
    public function actionExport()
    {
        $countries = TblCountry::find()->all();
        $objPHPExcel = new \PHPExcel();
        foreach($countries as $key => $country){
            $bonus_history_processing = TblHistoryBonus::find()->select([
                    '*',
                    'SUM(amount) AS total'
                ])->leftJoin('tbl_users','`tbl_users`.`id` = `tbl_history_bonus`.`user_id`')
                  ->andWhere(['=','payment_status','1'])
                  ->andWhere(['=','wallet',TblWallets::TYPE_BONUS])
                  ->andWhere(['=','tbl_users.country_id',$country->id])
                  ->groupBy('user_id')->orderBy('total desc')->all();
            $this->excelSheet($key,$bonus_history_processing,$objPHPExcel,$country->name);
        }
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.date("d-m-Y").'-report.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
    function excelSheet($key,$data,&$objPHPExcel,$title){
        $objWorkSheet = $objPHPExcel->createSheet($key); //Setting index when creating
        //Write cells
        $objWorkSheet->setCellValue('A1', '#')
                    ->setCellValue('B1', 'User')
                    ->setCellValue('C1', 'Fullname')
                    ->setCellValue('D1', 'Bank Name')
                    ->setCellValue('E1', 'Bank Account')
                    ->setCellValue('F1', 'Country')
                    ->setCellValue('G1', 'Before Tax (USD)')
                    ->setCellValue('H1', 'After Tax (USD)')
                    ->setCellValue('I1', 'Tax (%)');
        foreach($data as $key => $his){
            if($his->total >0){
                $user = $his->getUser()->one();
                $country =  $user->getTblCountry()->one();
                $bank = $user->getTblBanks()->one();
                $sheet = $key + 2;
                $objWorkSheet->setCellValue('A'.$sheet, $key+1)
                            ->setCellValue('B'.$sheet, isset($user->username) ? $user->username : '')
                            ->setCellValue('C'.$sheet, isset($user->fullname) ? $user->fullname : '')
                            ->setCellValue('D'.$sheet, isset($bank->name) ? $bank->name : '')
                            ->setCellValue('E'.$sheet, isset($user->bank_account) ? " ".$user->bank_account." " : '')
                            ->setCellValue('F'.$sheet, isset($country->name) ? $country->name : '')
                            ->setCellValue('G'.$sheet, isset($his->total) ? " ".$his->total." " : 0)
                            ->setCellValue('H'.$sheet, isset($his->total) ? " ".($his->total - $his->total * $country->tax /100)." " : 0)
                            ->setCellValue('I'.$sheet, isset($country->tax) ? $country->tax : '');
            }
        }
       
        $objWorkSheet->setTitle($title);
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm;
        $msg = "";
        if (!empty($_POST) && !empty($_POST['PasswordResetRequestForm']['username'])) {
                $user = User::find()->where(["username" => $_POST['PasswordResetRequestForm']['username']])->one();
                if($user == null || ($user != null && in_array($user->role_id, User::getRoleID(false))))
                    $msg = "Can not find this User!";
                else{
                    $time = time();
                    $token = md5($time."moasia".$_POST['PasswordResetRequestForm']['username']);
                    $str = "http://".$_SERVER['SERVER_NAME'].Url::to(['site/reset-password'])."?token=".$token."&t=".$time."&u=".$_POST['PasswordResetRequestForm']['username'];
                    $cnt = "
                    To make sure this is your request, click this link bellow to comfirm and change new password:
                    <br>
                    <a href='".$str."'>".$str."</a>";
                    $x = Yii::$app->mailer->compose()
                    ->setHtmlBody($cnt)
                    ->setFrom(['info@moringnaasia.com' => "Moringna Asia"])
                    ->setTo($user->email)
                    ->setSubject("Request password reset")
                    ->send();
                    $msg = "A new request password has been sent to your email!";
                }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
            "msg" => $msg
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword()
    {
        if(!empty($_GET)){
            $otoken = $_GET['token'];
            $time = $_GET['t'];
            $u = $_GET['u'];
        }
        else {
            $otoken = $_POST['ResetPasswordForm']['token'];
            $time = $_POST['ResetPasswordForm']['t'];
            $u = $_POST['ResetPasswordForm']['u'];            

        }

        $token = md5($time."moasia".$u);
        $msg = "";
        $model = new ResetPasswordForm();
        if(!($otoken == $token) ){
            die("Wrong request!");
        }
        else{
            if($time <= time() - 3600)
                die("Your request is overtime! Please create new request password reset!");
            else{
                if(!empty($_POST)){
                        $user = User::find()->where(["username" => $u])->one();
                        if($user == null)
                            $msg = "Can not find this User!";
                        else{
                            if(!in_array($user->role_id, User::getRoleID(true)))
                                $msg = "Do not permission!";
                            else{
                                $user->password = generalHelper::fnEncrypt($_POST['ResetPasswordForm']['password']);
                                $user->save();
                                $msg = "Your password has been changed! Will redirect to login page in 5 seconds... <meta http-equiv='refresh' content='5;URL=".Url::to(['site/login'])."'>";
                            }
                        }
                }
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
            "msg" => $msg
        ]);
    }
}