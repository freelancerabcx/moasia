<?php

namespace sadmin\controllers;

use Yii;
use common\models\TblWallets;
use common\models\User;
use common\models\TblUserSearch;
use common\models\TblHistoryBonus;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\TblBanks;

use yii\helpers\Json;
use yii\db\Query;
use common\helpers\generalHelper;
use common\helpers\processHelper;
/**
 * UserController implements the CRUD actions for TblUsers model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->role_id == User::ROLE_SADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                    [
                        'actions' => ['index', 'update', 'view', 'get-bank', 'get-parents'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (in_array(Yii::$app->user->identity->role_id, User::getRoleID(true)) && Yii::$app->user->identity->role_id != User::ROLE_PADMIN) {
                                return true;
                            }
                            return false;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblUsers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->role_id != User::ROLE_SADMIN)
            $this->redirect(["site/index"]);

        $model = new User();
        $model->setScenario('isRequired');
        $model->setScenario('hasToCheck');
        //echo generalHelper::fnEncrypt("zxcvbn1994");
        $tb = $_POST;
        if(!empty($tb)){
            $tb['User']['password'] = generalHelper::fnEncrypt($tb['User']['password']);
            $tb['User']['password_repeat'] = generalHelper::fnEncrypt($tb['User']['password_repeat']);
            $tb['User']['level'] = 0;
            if(!empty($tb['User']['parent_id'])){
                $parent = User::find()->where("id = ".$tb['User']['parent_id'])->one();
                if($parent->level >= 0){
                    $tb['User']['level'] = $parent->level + 1;
                }
            }
        }

        if ($model->load($tb) && $model->save()) {
            $obj = [
                "TblWallets" => [
                    "user_id" => $model->id,
                    "amount" => 0,
                    "type" => 0,
                    "updated_at" => time()
                ]
            ];

            if($_POST['User']['register'] > 0)
                processHelper::addHistory(Yii::$app->user->identity->id, $model->id, $_POST['User']['register'], TblHistoryBonus::TYPE_REG, TblWallets::TYPE_RESGI);


            if($_POST['User']['propoint'] > 0)
                processHelper::addHistory(Yii::$app->user->identity->id, $model->id, $_POST['User']['propoint'], TblHistoryBonus::TYPE_REG, TblWallets::TYPE_POINT);


            if($_POST['User']['bonus'] > 0)
                processHelper::addHistory(Yii::$app->user->identity->id, $model->id, $_POST['User']['bonus'], TblHistoryBonus::TYPE_REG, TblWallets::TYPE_BONUS);

            $md = new TblWallets;
            $md->load($obj);
            $md->amount = $_POST['User']['register'];
            $md->save($obj);


            $obj["TblWallets"]["type"] = 1;
            $md = new TblWallets;
            $md->load($obj);
            $md->amount = $_POST['User']['propoint'];
            $md->save($obj);

            $obj["TblWallets"]["type"] = 2;
            $md = new TblWallets;
            $md->load($obj);
            $md->amount = $_POST['User']['bonus'];
            $md->save($obj);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                "bonus" => 0,
                "propoint" => 0,
                "register" => 0
            ]);
        }
    }

    /**
     * Updates an existing TblUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $bonus = TblWallets::find()->where(['user_id' => $id, 'type' => TblWallets::TYPE_BONUS])->one();
        $propoint = TblWallets::find()->where(['user_id' => $id, 'type' => TblWallets::TYPE_POINT])->one();
        $register = TblWallets::find()->where(['user_id' => $id, 'type' => TblWallets::TYPE_RESGI])->one();
        $model = $this->findModel($id);

        $post = $_POST;
        if(!empty($_POST)){
            if(!empty($post['User']['parent_id'])){
                $parent = User::find()->where("id = ".$post['User']['parent_id'])->one();
                if($parent->level >= 0){
                    $post['User']['level'] = $parent->level + 1;
                }
            }
        }

        if ($model->load($post) && $model->save()) {
            if(isset($_POST['User']['bonus'])){

                if($_POST['User']['bonus'] > 0)
                    processHelper::addHistory(Yii::$app->user->identity->id, $model->id, ($_POST['User']['bonus'] - $bonus->amount), TblHistoryBonus::TYPE_REG, TblWallets::TYPE_BONUS);

                $bonus->amount = $_POST['User']['bonus'];
                $bonus->save();
            }

            if(isset($_POST['User']['propoint'])){

                if($_POST['User']['propoint'] > 0)
                    processHelper::addHistory(Yii::$app->user->identity->id, $model->id, ($_POST['User']['propoint'] - $propoint->amount), TblHistoryBonus::TYPE_REG, TblWallets::TYPE_POINT);

                $propoint->amount = $_POST['User']['propoint'];
                $propoint->save();
            }
            
            if(isset($_POST['User']['register'])){

                if($_POST['User']['register'] > 0)
                    processHelper::addHistory(Yii::$app->user->identity->id, $model->id, ($_POST['User']['register'] - $register->amount), TblHistoryBonus::TYPE_REG, TblWallets::TYPE_RESGI);

                $register->amount = $_POST['User']['register'];
                $register->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                "bonus" => !empty($bonus->amount) ? $bonus->amount : 0,
                "propoint" => !empty($propoint->amount) ? $propoint->amount : 0,
                "register" => !empty($register->amount) ? $register->amount : 0
            ]);
        }
    }
    /**
     * Updates an existing TblUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionProfile()
    {
        $id = Yii::$app->user->identity->id;
        $bonus = TblWallets::find()->where(['user_id' => $id, 'type' => TblWallets::TYPE_BONUS])->one();
        $propoint = TblWallets::find()->where(['user_id' => $id, 'type' => TblWallets::TYPE_POINT])->one();
        $register = TblWallets::find()->where(['user_id' => $id, 'type' => TblWallets::TYPE_RESGI])->one();
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(Yii::$app->user->identity->role_id == User::ROLE_ADMIN){
                if(isset($_POST['User']['bonus'])){
                    $bonus->amount = $_POST['User']['bonus'];
                    $bonus->save();
                }

                if(isset($_POST['User']['propoint'])){
                    $propoint->amount = $_POST['User']['propoint'];
                    $propoint->save();
                }
                
                if(isset($_POST['User']['register'])){
                    $register->amount = $_POST['User']['register'];
                    $register->save();
                }
            }
            return $this->redirect(['site/index']);
        } else {
            return $this->render('profile', [
                'model' => $model,
                "bonus" => !empty($bonus->amount) ? $bonus->amount : 0,
                "propoint" => !empty($propoint->amount) ? $propoint->amount : 0,
                "register" => !empty($register->amount) ? $register->amount : 0
            ]);
        }
    }

    /**
     * Deletes an existing TblUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->identity->role_id != User::ROLE_SADMIN)
            $this->redirect(["site/index"]);

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetBank($country_id, $currentbank_id)
    {
        $this->layout = 'empty';
        $dt = TblBanks::getList($country_id);
        return $this->render('filter_bank', [
            'dt' => $dt,
            'currentbank_id' => $currentbank_id
        ]);
    }

    /**
     * lay ra danh sach streaming de cho
     * @param null $search
     * @param null $id
     */


    public function actionGetParents($search = null, $id = null)
    {
        $out = ['more' => false];
        if (!is_null($search)) {
            $query = new Query;
            $query->select(['id', new \yii\db\Expression("CONCAT(username, ' | ', fullname) as text")])
                ->from('{{%tbl_users}}')
                ->where('status = '.User::STATUS_ACTIVE.' AND role_id IN ('.implode(",", User::getRoleID(false)).') AND username LIKE "%' . $search .'%"')
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif (!empty($id)) {
            $out['results'] = ['id' => $id, 'text' => User::findOne(['id' => $id])->username." | ".User::findOne(['id' => $id])->fullname];
        }
        else {
            $out['results'] = ['id' => 0, 'text' => 'No matching records found'];
        }
        echo Json::encode($out);
    }
}
