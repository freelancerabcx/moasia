$(document).ready(function(){


  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });
  
});

function dimgx(e)
{
    if (typeof (FileReader) != "undefined") {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        $($(e)[0].files).each(function () {
            var file = $(this);
            var img = $("#preview");
            if (regex.test(file[0].name.toLowerCase())) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    img.attr("src",  e.target.result);
                }
                reader.readAsDataURL(file[0]);
            } else {
                img.attr("src",  "#");
                $("input[type=file]").val('');
                alert(file[0].name + " is not a valid image file.");
                return false;
            }
        });
    } else {
        alert("This browser does not support HTML5 FileReader.");
    }
}