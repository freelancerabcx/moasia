$(function () {
    $('#mlnform').submit(function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: data
        }).success(function (data) {
            if (data == 1) {
                alert('Saved');
            } else {
                alert('Some thing was wrong! Please try again')
            }
        });
    })
    $('.filter .btn').on('click', function () {
        var tg = $(this).data('target');
        var tbl = $(this).closest('.filter').next().find('tbody');
        $('.filter .btn').removeClass('active');
        $(this).addClass('active');
        tbl.find('tr').addClass('hide');
        tbl.find(tg).removeClass('hide');
    });
    $('#move_bonus').on('click', function () {
        $.ajax({
            url: $(this).data('action'),
            method: 'POSt'
        }).success(function (data) {
            if (data == 1) {
                window.location.reload();
            } else {
                alert('Some thing was wrong! Please try again')
            }
        });
    });
});