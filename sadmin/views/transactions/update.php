<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblTransactions */

$this->title = 'Update Tbl Transactions: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-transactions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
