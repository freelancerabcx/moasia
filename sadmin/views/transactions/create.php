<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblTransactions */

$this->title = 'Create Tbl Transactions';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-transactions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
