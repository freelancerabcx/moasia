<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblCountry */

$this->title = 'Update Country: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-products-create">
	<div class="content-box-large">
	    <div class="panel-heading row">
	        <div class="panel-title"><?=$this->title;?></div>
	    </div>
	    <div class="panel-body">
	        <div class="tab-content">
	            <div class="tab-pane active" id="tab1">

				    <?= $this->render('_form', [
				        'model' => $model,
				        'data_person' => $data_person
				    ]) ?>
				</div>
			</div>
		</div>
	</div>
</div>