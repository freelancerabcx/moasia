<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblCountry */
/* @var $form yii\widgets\ActiveForm */
?>
<script type="text/javascript">
    var total_person = <?= json_encode($data_person);?>,unit=0;
    document.addEventListener("DOMContentLoaded", function(event) { 
        showResult();
    });
    function showResult(){
        var new_bronze = $("#tblcountry-bonus_mnl_bronze").val();
        var new_silver = $("#tblcountry-bonus_mnl_silver").val();
        var new_gold = $("#tblcountry-bonus_mnl_gold").val();
        var new_platinum = $("#tblcountry-bonus_mnl_platinum").val();
        var new_diamond = $("#tblcountry-bonus_mnl_diamond").val();
        var total = new_bronze * total_person.bronze.length +
                    new_silver * total_person.silver.length +
                    new_gold * total_person.gold.length +
                    new_platinum * total_person.platinum.length +
                    new_diamond * total_person.diamond.length;
        unit = $("#total").val() * total_person.amount /100 / total_person.total;
        $('#tbronze').html( (total_person.bronze.length >0 ) ? unit * new_bronze : 0);
        $('#tsilver').html( (total_person.silver.length >0 ) ? unit * new_silver : 0);
        $('#tgold').html( (total_person.gold.length >0 ) ? unit * new_gold : 0);
        $('#tplatinum').html( (total_person.platinum.length >0 ) ? unit * new_platinum : 0);
        $('#tdiamond').html( (total_person.diamond.length >0 ) ? unit * new_diamond : 0);
    }
</script>
<div class="tbl-country-form">
    <div class="row">
        <div class="col-md-5">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'pv_rate')->textInput(['maxlength' => true, "type" => "number"]) ?>
            <?= $form->field($model, 'tax')->textInput(['maxlength' => true, "type" => "number"]) ?>

            <?= $form->field($model, 'bonus_MNL_bronze')->textInput(['maxlength' => true, "type" => "number", "onkeyup"=>"showResult()"])->label("Bonus Mnl Bronze [".count($data_person['bronze'])." member(s)]", ["class" => "txtred"]) ?>
            <?= $form->field($model, 'bonus_MNL_silver')->textInput(['maxlength' => true, "type" => "number", "onkeyup"=>"showResult()"])->label("Bonus Mnl Silver [".count($data_person['silver'])." member(s)]", ["class" => "txtred"]) ?>
            <?= $form->field($model, 'bonus_MNL_gold')->textInput(['maxlength' => true, "type" => "number", "onkeyup"=>"showResult()"])->label("Bonus Mnl Gold [".count($data_person['gold'])." member(s)]", ["class" => "txtred"]) ?>
            <?= $form->field($model, 'bonus_MNL_platinum')->textInput(['maxlength' => true, "type" => "number", "onkeyup"=>"showResult()"])->label("Bonus Mnl Platinum [".count($data_person['platinum'])." member(s)]", ["class" => "txtred"]) ?>
            <?= $form->field($model, 'bonus_MNL_diamond')->textInput(['maxlength' => true, "type" => "number", "onkeyup"=>"showResult()"])->label("Bonus Mnl Diamond [".count($data_person['diamond'])." member(s)]", ["class" => "txtred"]) ?>
            <?= $form->field($model, 'bonus_MNL_total')->textInput(['maxlength' => true, "type" => "number", "id" => "total", "onkeyup"=>"showResult()"]) ?>
            <div style="display: none">
                <?= $form->field($model, 'updated_at')->hiddenInput(["value" => time()]) ?>
                <?= $form->field($model, 'bonus_MNL_max')->textInput(['maxlength' => true, "value" => 0]) ?>

                <?= $form->field($model, 'updated_by')->hiddenInput(["value" => Yii::$app->user->id]) ?>
            </div>
            <p class="txtred"><small><i>If you change the red fields, you need to save and refresh the page.</i></small></p>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-5">
            <?php if(!$model->isNewRecord):?>
                <br>
                <div class="panel panel-primary">
                    <div class="panel-heading">Preview Member's Bonus</div>
                    <div class="panel-body">
                         <table class="table borderless">
                                    <tr>
                                        <td>Total Money:</td>
                                        <td><?=$data_person['amount']?></td>
                                    </tr>
                                    <tr>
                                        <td> Bronze:</td>
                                        <td><span id="tbronze">0</span></td>
                                    </tr>
                                    <tr>
                                        <td> Silver:</td>
                                        <td><span id="tsilver">0</span></td>
                                    </tr>
                                    <tr>
                                        <td> Gold:</td>
                                        <td><span id="tgold">0</span></td>
                                    </tr>
                                    <tr>
                                        <td> Platinum:</td>
                                        <td><span id="tplatinum">0</span></td>
                                    </tr>
                                    <tr>
                                        <td> Diamond:</td>
                                        <td><span id="tdiamond">0</span></td>
                                    </tr>
                                </table>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>