<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\TblProductCategory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblProducstSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products Management';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="content-box-large">
        <div class="panel-heading row">
            <div class="panel-title">
                <?=$this->title;?>
            </div>

            <div class="hide">
                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <p>
                <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    [
                        'attribute' => 'category_id',
                        'value' => function($md){
                            return $md->category->name;
                        },
                        'filter' => TblProductCategory::getList()
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function($md){
                            return $md->status == 1 ? '+' : '-';
                        },
                        'filter' => [
                            1 => "Active",
                            0 => "Block"
                        ]
                    ],
                    'name',
                    [
                        'label' => 'Ordered Quantity',
                        'value' => function($md){
                            return $md->getSumOrders();
                        }
                    ],
                    // 'alias',
                    // 'info:ntext',
                    // 'content:ntext',
                    // 'updated_at',
                    // 'updated_by',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update}'
                    ],
                ],
            ]); ?>
        </div>
    </div>