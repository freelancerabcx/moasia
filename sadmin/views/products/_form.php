<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\TblProductCategory;
use common\models\TblCountry;
use common\helpers\generalHelper;


/* @var $this yii\web\View */
/* @var $model common\models\TblProducts */
/* @var $form yii\widgets\ActiveForm */
$status = [
    1 => "Active",
    0 => "Block"
];
$prices = $model['prices'];
$model = $model['md'];

$lstCate = TblProductcategory::find()->asArray()->all();
$checkISVoucher = false;
foreach ($lstCate as $k => $v) {
    if($v['id'] == $model->category_id && $v['is_voucher'] == 1)
        $checkISVoucher = true;
}
?>

<div class="tbl-products-form">

    <?php $form = ActiveForm::begin([
                'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <label>Image</label>
            <input type="file" name="img" id="files" onchange="dimgx(this)">
        </div>
    </div>
    <img src="<?=generalHelper::img('/img/product/'.$model->id.'.jpg').'?t='.time();?>" style="height: 250px" id="preview">
    <br><br>

    <?= $form->field($model, 'status')->dropdownList($status) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropdownList(TblProductcategory::getList(), ["id" => "category_id", 
        "onchange"=>"switchVoucher(this.value)"
    ]) ?>
    <?= $form->field($model, 'voucher_amount')->textInput([
        'maxlength' => true, 
        "id" => "voucher_amount", 
        "placeholder" => "eg. 10 means 10% paid out each month, or 5% paid out every 2 weeks"
    ]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    

    <div class="form-group field-tblproducts-alias">
        <label class="control-label" for="tblproducts-alias">Price (P.V | USD | Country)</label>
        <br>
        <div style="display: block" id="prices">
            <?php if(count($prices) > 0): ?>
                <?php foreach ($prices as $kk => $vv): ?>
                    <div style="display: block" id="re<?=$kk;?>">
                        <input type="number" class="form-control" placeholder="P.V" name="TblProducts[prices_pv][]" value="<?=$vv['price_pv'];?>" style="width: 200px; display: inline-block">
                        <input type="number" class="form-control" placeholder="USD" name="TblProducts[prices][]" value="<?=$vv['price'];?>" style="width: 200px; display: inline-block">
                        <select class="form-control" name="TblProducts[country_ids][]" style="width: 200px; display: inline-block">
                            <?php foreach (TblCountry::getList() as $k => $v): ?>
                                <option value="<?=$k;?>" <?php if($k == $vv['country']):?>selected<?php endif;?>><?=$v;?></option>
                            <?php endforeach; ?>
                        </select>
                        <div class="btn btn-success" onclick="removePrice('re<?=$kk;?>')">Remove</div>
                        <div style="display: block"></div>
                        <br>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <div class="btn btn-primary" style="display: block; width: 100px; margin-top: 20px" onclick="addPrice()">Add Price</div>
        <div class="help-block"></div>
    </div>
    <div style="display: none" id="cnt_price">
        <div style="display: block" id="re__tmp__">
            <input type="number" class="form-control" placeholder="P.V" name="TblProducts[prices_pv][]" value="" style="width: 200px; display: inline-block">
            <input type="number" class="form-control" placeholder="USD" name="TblProducts[prices][]" value="" style="width: 200px; display: inline-block">
            <select class="form-control" name="TblProducts[country_ids][]" style="width: 200px; display: inline-block">
                <?php foreach (TblCountry::getList() as $k => $v): ?>
                    <option value="<?=$k;?>"><?=$v;?></option>
                <?php endforeach; ?>
            </select>
            <div class="btn btn-success" onclick="removePrice('re__tmp__')">Remove</div>
            <div style="display: block"></div>
            <br>
        </div>
    </div>
        <?= $form->field($model, 'description')->textArea() ?>

    <?php 
        echo $form->field($model, 'info')->widget(letyii\tinymce\Tinymce::className(), [
            'configs' => [ // Read more: http://www.tinymce.com/wiki.php/Configuration
                'toolbar1' => 'undo redo | styleselect fontselect fontsizeselect | bold italic underline strikethrough | code',
                'toolbar2' => 'searchreplace link unlink image table forecolor backcolor charmap | alignleft aligncenter alignright | bullist numlist outdent indent | fullscreen',
                'plugins' => "image imagetools table textcolor colorpicker fullscreen link charmap searchreplace code filemanager",
                'menubar' => "",
                'external_filemanager_path' => generalHelper::img("filemanager/"),
                'filemanager_title' => "Responsive Filemanager" ,
                'external_plugins' => [ "filemanager" => generalHelper::img("filemanager/plugin.min.js")]
            ],
        ]);
    ?>
    
    
    
    <?php 
        echo $form->field($model, 'content')->widget(letyii\tinymce\Tinymce::className(), [
            'configs' => [ // Read more: http://www.tinymce.com/wiki.php/Configuration
                'toolbar1' => 'undo redo | styleselect fontselect fontsizeselect | bold italic underline strikethrough | code',
                'toolbar2' => 'searchreplace link unlink image table forecolor backcolor charmap | alignleft aligncenter alignright | bullist numlist outdent indent | fullscreen',
                'plugins' => "image imagetools table textcolor colorpicker fullscreen link charmap searchreplace code filemanager",
                'menubar' => "",
                'external_filemanager_path' => generalHelper::img("filemanager/"),
                'filemanager_title' => "Responsive Filemanager" ,
                'external_plugins' => [ "filemanager" => generalHelper::img("filemanager/plugin.min.js")]
            ],
        ]);
    ?>

    <div style="display: none">
        <?= $form->field($model, 'updated_at')->hiddenInput(["value" => time()]) ?>

        <?= $form->field($model, 'updated_by')->hiddenInput(["value" => Yii::$app->user->id]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    function addPrice () {
        var x = $('#cnt_price').html();
        var t = new Date().getTime();
        x = x.replace(/__tmp__/g, t);
        $('#prices').append(x);
    }

    function removePrice(id){
        $("#"+id).remove();
    }

    function resetVoucher(){
        $('#voucher_amount').val(0);
        $('#voucher_amount').hide();
    }

    function switchVoucher(id){
        var lstCate = <?=json_encode($lstCate);?>;

        $('#voucher_amount').val(0);
        $('.field-voucher_amount').hide();

        $.each(lstCate, function(k, v){
            if(v.id == id && v.is_voucher == 1)
            {
                $('#voucher_amount').val(0);
                $('.field-voucher_amount').show();
            }
        })
    }

    window.onload = function(){
        <?php if(!$checkISVoucher): ?>
            $('.field-voucher_amount').hide();
        <?php endif; ?>
    }
</script>