<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblHistoryDay */

$this->title = 'Create Tbl History Day';
$this->params['breadcrumbs'][] = ['label' => 'Tbl History Days', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-history-day-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
