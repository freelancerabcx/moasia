<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\TblCountry;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblHistoryDaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'History Days';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-history-day-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'income',
            'after_bonus',
            'monoleg',
            [
                'attribute' => 'country_id',
                'value' => function($md){
                    return $md->country->name;
                },
                'filter' => TblCountry::getList()
            ],
            [
                'attribute' => 'created_at',
                'value'=>function($md){
                    return date('H:m:i d-m-Y',$md->created_at);
                }
            ]

        ],
    ]); ?>
</div>