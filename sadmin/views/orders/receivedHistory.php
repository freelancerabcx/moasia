<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblProductOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Received History: '.$order_id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-box-large">
    <div class="panel-heading row">
        <div class="panel-title"><?=$this->title;?></div>

        <div class="hide">
            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <p>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'product_id',
                            'value' => function($md){
                                return $md->product->id." - ".$md->product->name;
                            }
                        ],
                        'order_id',
                        [
                            'attribute' => 'quantity',
                            'label' => "Received/Total",
                            'value' => function($md){
                                return $md->quantity.'/'.$md->total_ordered;
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Time',
                            'value' => function($md){
                                return DATE('d-M-Y H:i:s', $md->created_at);
                            }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>