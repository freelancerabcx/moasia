<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblProductOrders */
/* @var $form yii\widgets\ActiveForm */
$is_receivedx = [
    0 => "x - No",
    1 => "✓ - Yes",
];
?>

<div class="tbl-product-orders-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'is_received')->dropDownList($is_receivedx) ?>
    <?= $form->field($model, 'received_quantity')->textInput(["type" => "number", "min" => 0, "max" => $model->quantity]) ?>
    <?= $form->field($model, 'quantity')->textInput(["readonly"=>true])->label('Total Order') ?>
    <div style="display: none">
        <?= $form->field($model, 'user_id')->textInput() ?>

        <?= $form->field($model, 'amount')->textInput() ?>

        <?= $form->field($model, 'product_id')->textInput() ?>


        <?= $form->field($model, 'is_delivery')->textInput() ?>

        <?= $form->field($model, 'created_at')->textInput() ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
