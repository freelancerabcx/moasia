<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

use kartik\datecontrol\DateControl;
use common\models\TblCountry;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TblProductOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

$is_receivedx = [
    0 => "x - No",
    1 => "✓ - Yes",
];
?>
<div class="content-box-large">
    <div class="panel-heading row">
        <div class="panel-title"><?=$this->title;?></div>
        <?php if(Yii::$app->session->getFlash('error') != null): ?>
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <div class="hide">
            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <p>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'userName',
                        'userFullname',
                        'amount',
                        'productName',
                        [
                            'attribute' => 'quantity',
                            'label' => "Received/Total",
                            'value' => function($md){
                                return $md->received_quantity.'/'.$md->quantity;
                            }
                        ],
                        [
                            'attribute' => 'is_received',
                            'value' => function($md){
                                return $md->is_received == 1 ? '✓': 'x';
                            },
                            'filter' => $is_receivedx
                        ],
                        [
                            'attribute' => 'country_id',
                            'label' => 'Country',
                            'value' => function($md){
                                return $md->country->name;
                            },
                            'filter' => TblCountry::getList()
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Ordered At',
                            'value' => function($md){
                                return DATE('d-M-Y', $md->created_at);
                            },
                            'filter' => DateControl::widget([
                                'model' => $searchModel, 
                                'attribute' => 'created_at',
                                'type'=>DateControl::FORMAT_DATE,
                                'displayFormat' => 'php:d-M-Y',
                                'saveFormat' => 'php:U'
                            ]),
                            "contentOptions" => ["style" => "width: 220px"]
                        ],
                        // 'is_delivery',
                        // 'created_at',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}{viewhistory}',
                            'buttons' => [
                            	'viewhistory' =>  function ($url, $model) {  
	                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['received-history?id='.$model->id]), [
	                                        'title' => Yii::t('yii', 'View Received History'),
	                                ]);                                
	                            }
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>