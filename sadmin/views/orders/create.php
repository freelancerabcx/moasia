<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblProductOrders */

$this->title = 'Create Tbl Product Orders';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Product Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-product-orders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
