<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\TblCountry;

/* @var $this yii\web\View */
/* @var $model common\models\TblBanks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-banks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_id')->dropDownList(TblCountry::getList())->label('Country') ?>

    <div style="display: none">
        <?= $form->field($model, 'updated_at')->hiddenInput(["value" => time()]) ?>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
