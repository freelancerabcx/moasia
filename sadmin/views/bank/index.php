<?php

use yii\helpers\Html;
use yii\grid\GridView;

use common\models\TblCountry;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblBanksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Management';
$this->params['breadcrumbs'][] = $this->title;
?><div class="content-box-large">
    <div class="panel-heading row">
        <div class="panel-title"><?=$this->title;?></div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">

                <p>
                    <?= Html::a('Create Bank', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'name',
                        [
                            'attribute' => 'country_id',
                            'label' => 'Country',
                            'value' => function($md){
                                return $md->country->name;
                            },
                            'filter' => TblCountry::getList()
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}'
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
