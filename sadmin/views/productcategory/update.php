<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblProductCategory */

$this->title = 'Update Product Category: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Product Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-product-category-update">
	<div class="content-box-large">
	    <div class="panel-heading row">
	        <div class="panel-title"><?=$this->title;?></div>

	        <div class="hide">
	            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
	            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
	        </div>
	    </div>
	    <div class="panel-body">
	        <div class="tab-content">
	            <div class="tab-pane active" id="tab1">

				    <?= $this->render('_form', [
				        'model' => $model,
				    ]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
