<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TblProductCategory */
/* @var $form yii\widgets\ActiveForm */
$status = [
    1 => "Active",
    0 => "Block"
];

?>

<div class="tbl-product-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList($status);?>
	<div style="display: none">
	    <?= $form->field($model, 'updated_at')->hiddenInput(["value" => time()]) ?>

	    <?= $form->field($model, 'updated_by')->hiddenInput(["value" => Yii::$app->user->id]) ?>
	</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
