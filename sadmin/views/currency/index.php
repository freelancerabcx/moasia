<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblCurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Currency';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-box-large">
    <div class="panel-heading row">
        <div class="panel-title"><?=$this->title;?></div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">

                <p>
                    <?= Html::a('Create Currency', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'name',
                        'code',
                        'usd_rate',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update} {delete}'
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
