<?php

use yii\helpers\Html;

$this->title = 'Dashboard Product';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="content-box-large">
        <div class="panel-heading row">
            <div class="panel-title">
                <?=$this->title;?>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <?php foreach($data as $coutry): ?>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <?= $coutry->name ?>
                            </div>
                            <div class="panel-body">
                                <table class="table">
                                    <thead style="font-weight: bold">
                                        <tr>
                                            <td>Product</td>
                                            <td>Total Orders</td>
                                        </tr>
                                    </thead>
                                    <?php foreach ($coutry->today_orders as $k => $v):?>
                                        <tr>
                                            <td><?=$v->name;?></td>
                                            <td><?=$v->today_orders;?></td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
            </div>

        </div>
    </div>