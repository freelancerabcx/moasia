<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Request Password Reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<article class="row">
    <div class="col-md-6 col-md-offset-3">

        <header><h3><?= Html::encode($this->title) ?></h3></header>
        <p>Please fill out the following fields to request a new password:</p>
        <div class="">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('SEND REQUEST', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                <p><?=$msg;?></p>

                <?php ActiveForm::end(); ?>
        </div>
    </div>
</article>