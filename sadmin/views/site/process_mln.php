<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Setup Daily Bonus';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="content-box-large">
        <div class="panel-heading row">
            <div class="panel-title">
                <?=$this->title;?>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6" id="handle_caculate">
                    <form action="<?= Url::to(['site/save-mln']); ?>" method="post" id="mlnform">
                        <div class="form-group field-user-email required">
                            <label class="control-label">Bonus for Bronze (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_bronze]" value="<?= $data['config']["bonus_MNL_bronze"] ?>">
                        </div>
                        <div class="form-group field-user-email required">
                            <label class="control-label">Bonus for Silver (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_silver]" value="<?= $data['config']["bonus_MNL_silver"] ?>">
                        </div>

                        <div class="form-group field-user-email required">
                            <label class="control-label">Bonus for Gold (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_gold]" value="<?= $data['config']["bonus_MNL_gold"] ?>">
                        </div>
                        <div class="form-group field-user-email required">
                            <label class="control-label">Bonus for Platinum (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_platinum]" value="<?= $data['config']["bonus_MNL_platinum"] ?>">
                        </div>
                        <div class="form-group field-user-email required">
                            <label class="control-label">Bonus for Diamond (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_diamond]" value="<?= $data['config']["bonus_MNL_diamond"] ?>">
                        </div>
                        <div class="form-group field-user-email required">
                            <label class="control-label">Range (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_max]" value="<?= $data['config']["bonus_MNL_max"] ?>">
                        </div>
                        <div class="form-group field-user-email required">
                            <label class="control-label">Total Bonus (PV)</label>
                            <input type="number" class="form-control" name="MLN[bonus_MNL_total]" value="<?= $data['config']["bonus_MNL_total"] ?>">
                        </div>
                        <div>
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                   
                    <h3>Total income of to day</h3>
                    <h4 id="total"><?= number_format((float)$data['total']->total ? $data['total']->total : 0, 2, '.', ''); ?> PV</h4>
                </div>
            </div>
        </div>
    </div>