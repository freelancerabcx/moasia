<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
$this->title = 'Income';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="content-box-large">
        <div class="panel-heading row">
            <div class="panel-title">
                History Bonus
            </div>
        </div>
        <div class="rows">
            <div class="pull-right">
                <div class="btn-group">
                   <a class="btn btn-success" href="<?= Url::to(['site/update-bonus?type=2']); ?>">Step 1: UPDATE to Processing</a>
                    <a class="btn btn-primary" href="<?= Url::to(['site/export']); ?>">Step 2: EXPORT to excel</a>
                    <a class="btn btn-danger" href="<?= Url::to(['site/update-bonus?type=1']); ?>"> Step 3: UPDATE to Calculated</a>
                </div>
            </div>
        </div>
        <br><br>
        <table class="table">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>From User</th>
                    <th>To User</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Commission</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($models as $key => $his):?>
                    <tr class="<?php 
                                switch($his->payment_status){
                                    case 0:
                                        echo " uncalculated ";
                                        break;
                                    case 1:
                                        echo "uncalculated ";
                                    break;
                                    case 2:
                                        echo "calculated ";
                                    break;
                                }
                            ?>">
                        <td>
                            <?= date('d-m-Y',$his->created_at) ?>
                        </td>
                        <td>
                            <?= $his->type != 5 ? $his->getFromUser()->one()->username." | ".$his->getFromUser()->one()->fullname : $his->getUser()->one()->username." | ".$his->getUser()->one()->fullname; ?>
                        </td>
                        <td>
                            <?= $his->type != 5 ? $his->getUser()->one()->username." | ".$his->getUser()->one()->fullname : $his->getFromUser()->one()->fullname." | ".$his->getFromUser()->one()->username; ?>

                        </td>
                        <td>
                            <?php
                                switch($his->type){
                                    case 1:
                                        echo 'Direct sponsor bonus';
                                        break;
                                    case 2:
                                        echo 'Overiding';
                                        break;
                                    case 3:
                                        echo 'Daily bonus';
                                        break;
                                    case 6:
                                        echo 'Mounthy bonus';
                                        break;
                                    case 4:
                                        echo $his->getFromUser()->one()->username." | ".$his->getFromUser()->one()->fullname.' shares Register to '.$his->getUser()->one()->username." | ".$his->getUser()->one()->fullname;
                                        break;
                                    case 5:
                                        echo $his->getFromUser()->one()->username." | ".$his->getFromUser()->one()->fullname.' recieves Shared Register from '.$his->getUser()->one()->username." | ".$his->getUser()->one()->fullname;
                                        break;
                                };
                                ?>
                        </td>
                        <td>
                            <?php 
                                switch($his->payment_status){
                                    case 0:
                                        echo "Uncalculated";
                                    break;
                                    case 1:
                                        echo "Proccessing";
                                    break;
                                    case 2:
                                        echo "Calculated into payment";
                                    break;
                                }
                            ?>
                        </td>
                        <td>
                            <?= $his->amount ?> PV
                        </td>
                    </tr>
                    <?php endforeach;?>
            </tbody>
        </table>
        <?=  LinkPager::widget(['pagination' => $pages,]);?>
    </div>