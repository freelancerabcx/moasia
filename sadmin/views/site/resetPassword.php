<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<article class="row">
    <div class="col-md-6 col-md-offset-3">

        <header><h3><?= Html::encode($this->title) ?></h3></header>
        <p>Please fill out the following fields to Reset Password:</p>
        <div class="">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>


                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <input type="hidden" value="<?=!empty($_GET['u']) ? $_GET['u'] : '';?>" name="ResetPasswordForm[u]">
                    <input type="hidden" value="<?=!empty($_GET['token']) ? $_GET['token'] : '';?>" name="ResetPasswordForm[token]">
                    <input type="hidden" value="<?=!empty($_GET['t']) ? $_GET['t'] : '';?>" name="ResetPasswordForm[t]">

                    <div class="form-group">
                        <?= Html::submitButton('Change', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
                <p><?=$msg;?></p>
        </div>
    </div>
</article>