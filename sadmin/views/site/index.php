<?php

use yii\helpers\Html;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="content-box-large">
        <div class="panel-heading row">
            <div class="panel-title">
                <?=$this->title;?>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <?php foreach($data as $coutry): ?>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <?= $coutry->name ?>
                            </div>
                            <div class="panel-body">
                                <table class="table borderless">
                                    <tr>
                                        <td>Total Money Before Bonus:</td>
                                        <td>
                                            <?= $coutry->today_orders?> USD</td>
                                    </tr>
                                    <tr>
                                        <td>Total Money After Bonus:</td>
                                        <td>
                                            <?= round($coutry->after_bonus, 2) ?> USD</td>
                                    </tr>
                                    <tr>
                                        <td>Today Registers:</td>
                                        <td>
                                            <?= $coutry->today_registers ? $coutry->today_registers : 0 ?> users</td>
                                    </tr>
                                    <tr>
                                        <td>Total User:</td>
                                        <td>
                                            <?= $coutry->total_user ? $coutry->total_user : 0 ?> users</td>
                                    </tr>
                                    <tr>
                                        <td>Total Active User:</td>
                                        <td>
                                            <?= $coutry->active_user ? $coutry->active_user : 0 ?> users</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
            </div>

        </div>
    </div>