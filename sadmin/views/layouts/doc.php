<div class="row">
                <div class="col-md-6">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Basic Table</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Username</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Mark</td>
                                  <td>Otto</td>
                                  <td>@mdo</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Jacob</td>
                                  <td>Thornton</td>
                                  <td>@fat</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>Larry</td>
                                  <td>the Bird</td>
                                  <td>@twitter</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Striped Rows</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table ">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Username</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Mark</td>
                                  <td>Otto</td>
                                  <td>@mdo</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Jacob</td>
                                  <td>Thornton</td>
                                  <td>@fat</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>Larry</td>
                                  <td>the Bird</td>
                                  <td>@twitter</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Border Table</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Username</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Mark</td>
                                  <td>Otto</td>
                                  <td>@mdo</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Jacob</td>
                                  <td>Thornton</td>
                                  <td>@fat</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>Larry</td>
                                  <td>the Bird</td>
                                  <td>@twitter</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Hover Rows</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Username</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Mark</td>
                                  <td>Otto</td>
                                  <td>@mdo</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Jacob</td>
                                  <td>Thornton</td>
                                  <td>@fat</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>Larry</td>
                                  <td>the Bird</td>
                                  <td>@twitter</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Condensed Table</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Username</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>1</td>
                                  <td>Mark</td>
                                  <td>Otto</td>
                                  <td>@mdo</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>Jacob</td>
                                  <td>Thornton</td>
                                  <td>@fat</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>Larry</td>
                                  <td>the Bird</td>
                                  <td>@twitter</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Table with row classes</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>First Name</th>
                                  <th>Last Name</th>
                                  <th>Username</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="success">
                                  <td>1</td>
                                  <td>Mark</td>
                                  <td>Otto</td>
                                  <td>@mdo</td>
                                </tr>
                                <tr class="danger">
                                  <td>2</td>
                                  <td>Jacob</td>
                                  <td>Thornton</td>
                                  <td>@fat</td>
                                </tr>
                                <tr class="warning">
                                  <td>3</td>
                                  <td>Larry</td>
                                  <td>the Bird</td>
                                  <td>@twitter</td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">Responsive Tables</div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Username</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>1</td>
                              <td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>
                            </tr>
                            <tr>
                              <td>2</td>
                              <td>Jacob</td>
                              <td>Thornton</td>
                              <td>@fat</td>
                            </tr>
                            <tr>
                              <td>3</td>
                              <td>Larry</td>
                              <td>the Bird</td>
                              <td>@twitter</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="content-box-large">
                <div class="panel-heading">
                    <div class="panel-title">Bootstrap dataTables</div>
                </div>
                <div class="panel-body">
                    <table cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" id="example">
                        <thead>
                            <tr>
                                <th>Rendering engine</th>
                                <th>Browser</th>
                                <th>Platform(s)</th>
                                <th>Engine version</th>
                                <th>CSS grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td>Trident</td>
                                <td>Internet
                                     Explorer 4.0</td>
                                <td>Win 95+</td>
                                <td class="center"> 4</td>
                                <td class="center">X</td>
                            </tr>
                            <tr class="even gradeC">
                                <td>Trident</td>
                                <td>Internet
                                     Explorer 5.0</td>
                                <td>Win 95+</td>
                                <td class="center">5</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="odd gradeA">
                                <td>Trident</td>
                                <td>Internet
                                     Explorer 5.5</td>
                                <td>Win 95+</td>
                                <td class="center">5.5</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="even gradeA">
                                <td>Trident</td>
                                <td>Internet
                                     Explorer 6</td>
                                <td>Win 98+</td>
                                <td class="center">6</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="odd gradeA">
                                <td>Trident</td>
                                <td>Internet Explorer 7</td>
                                <td>Win XP SP2+</td>
                                <td class="center">7</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="even gradeA">
                                <td>Trident</td>
                                <td>AOL browser (AOL desktop)</td>
                                <td>Win XP</td>
                                <td class="center">6</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Firefox 1.0</td>
                                <td>Win 98+ / OSX.2+</td>
                                <td class="center">1.7</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Firefox 1.5</td>
                                <td>Win 98+ / OSX.2+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Firefox 2.0</td>
                                <td>Win 98+ / OSX.2+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Firefox 3.0</td>
                                <td>Win 2k+ / OSX.3+</td>
                                <td class="center">1.9</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Camino 1.0</td>
                                <td>OSX.2+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Camino 1.5</td>
                                <td>OSX.3+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Netscape 7.2</td>
                                <td>Win 95+ / Mac OS 8.6-9.2</td>
                                <td class="center">1.7</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Netscape Browser 8</td>
                                <td>Win 98SE+</td>
                                <td class="center">1.7</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Netscape Navigator 9</td>
                                <td>Win 98+ / OSX.2+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.0</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.1</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1.1</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.2</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1.2</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.3</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1.3</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.4</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1.4</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.5</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1.5</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.6</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">1.6</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.7</td>
                                <td>Win 98+ / OSX.1+</td>
                                <td class="center">1.7</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Mozilla 1.8</td>
                                <td>Win 98+ / OSX.1+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Seamonkey 1.1</td>
                                <td>Win 98+ / OSX.2+</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Gecko</td>
                                <td>Epiphany 2.20</td>
                                <td>Gnome</td>
                                <td class="center">1.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>Safari 1.2</td>
                                <td>OSX.3</td>
                                <td class="center">125.5</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>Safari 1.3</td>
                                <td>OSX.3</td>
                                <td class="center">312.8</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>Safari 2.0</td>
                                <td>OSX.4+</td>
                                <td class="center">419.3</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>Safari 3.0</td>
                                <td>OSX.4+</td>
                                <td class="center">522.1</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>OmniWeb 5.5</td>
                                <td>OSX.4+</td>
                                <td class="center">420</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>iPod Touch / iPhone</td>
                                <td>iPod</td>
                                <td class="center">420.1</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Webkit</td>
                                <td>S60</td>
                                <td>S60</td>
                                <td class="center">413</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 7.0</td>
                                <td>Win 95+ / OSX.1+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 7.5</td>
                                <td>Win 95+ / OSX.2+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 8.0</td>
                                <td>Win 95+ / OSX.2+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 8.5</td>
                                <td>Win 95+ / OSX.2+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 9.0</td>
                                <td>Win 95+ / OSX.3+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 9.2</td>
                                <td>Win 88+ / OSX.3+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera 9.5</td>
                                <td>Win 88+ / OSX.3+</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Opera for Wii</td>
                                <td>Wii</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Nokia N800</td>
                                <td>N800</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Presto</td>
                                <td>Nintendo DS browser</td>
                                <td>Nintendo DS</td>
                                <td class="center">8.5</td>
                                <td class="center">C/A<sup>1</sup></td>
                            </tr>
                            <tr class="gradeC">
                                <td>KHTML</td>
                                <td>Konqureror 3.1</td>
                                <td>KDE 3.1</td>
                                <td class="center">3.1</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="gradeA">
                                <td>KHTML</td>
                                <td>Konqureror 3.3</td>
                                <td>KDE 3.3</td>
                                <td class="center">3.3</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeA">
                                <td>KHTML</td>
                                <td>Konqureror 3.5</td>
                                <td>KDE 3.5</td>
                                <td class="center">3.5</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeX">
                                <td>Tasman</td>
                                <td>Internet Explorer 4.5</td>
                                <td>Mac OS 8-9</td>
                                <td class="center">-</td>
                                <td class="center">X</td>
                            </tr>
                            <tr class="gradeC">
                                <td>Tasman</td>
                                <td>Internet Explorer 5.1</td>
                                <td>Mac OS 7.6-9</td>
                                <td class="center">1</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="gradeC">
                                <td>Tasman</td>
                                <td>Internet Explorer 5.2</td>
                                <td>Mac OS 8-X</td>
                                <td class="center">1</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Misc</td>
                                <td>NetFront 3.1</td>
                                <td>Embedded devices</td>
                                <td class="center">-</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="gradeA">
                                <td>Misc</td>
                                <td>NetFront 3.4</td>
                                <td>Embedded devices</td>
                                <td class="center">-</td>
                                <td class="center">A</td>
                            </tr>
                            <tr class="gradeX">
                                <td>Misc</td>
                                <td>Dillo 0.8</td>
                                <td>Embedded devices</td>
                                <td class="center">-</td>
                                <td class="center">X</td>
                            </tr>
                            <tr class="gradeX">
                                <td>Misc</td>
                                <td>Links</td>
                                <td>Text only</td>
                                <td class="center">-</td>
                                <td class="center">X</td>
                            </tr>
                            <tr class="gradeX">
                                <td>Misc</td>
                                <td>Lynx</td>
                                <td>Text only</td>
                                <td class="center">-</td>
                                <td class="center">X</td>
                            </tr>
                            <tr class="gradeC">
                                <td>Misc</td>
                                <td>IE Mobile</td>
                                <td>Windows Mobile 6</td>
                                <td class="center">-</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="gradeC">
                                <td>Misc</td>
                                <td>PSP browser</td>
                                <td>PSP</td>
                                <td class="center">-</td>
                                <td class="center">C</td>
                            </tr>
                            <tr class="gradeU">
                                <td>Other browsers</td>
                                <td>All others</td>
                                <td>-</td>
                                <td class="center">-</td>
                                <td class="center">U</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
<div class="row">
                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Horizontal Form</div>
                              
                                <div class="hide">
                                  <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                  <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Text Field</label>
                                    <div class="col-sm-10">
                                      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Password Field</label>
                                    <div class="col-sm-10">
                                      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Textarea</label>
                                    <div class="col-sm-10">
                                      <textarea class="form-control" placeholder="Textarea" rows="3"></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label">Readonly</label>
                                    <div class="col-sm-10">
                                      <span class="form-control">Read only text</span>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox"> Checkbox
                                        </label>
                                      </div>
                                      <div class="checkbox">
                                        <label>
                                          <input type="checkbox"> Checkbox
                                        </label>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <button type="submit" class="btn btn-primary">Sign in</button>
                                    </div>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Vertical Form</div>
                              
                                <div class="hide">
                                  <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                  <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <form action="">
                                    <fieldset>
                                        <div class="form-group">
                                            <label>Text field</label>
                                            <input class="form-control" placeholder="Text field" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label>Password field</label>
                                            <input class="form-control" placeholder="Password" type="password" value="mypassword">
                                        </div>
                                        <div class="form-group">
                                            <label>Textarea</label>
                                            <textarea class="form-control" placeholder="Textarea" rows="3"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Readonly</label>
                                            <span class="form-control">Read only text</span>
                                        </div>
                                    </fieldset>
                                    <div>
                                        <div class="btn btn-primary">
                                            <i class="fa fa-save"></i>
                                            Submit
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="content-box-header">
                            <div class="panel-title">Inline Form</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="content-box-large box-with-header">
                            <form class="form-inline" role="form">
                            
                                <fieldset>
                                    <div class="form-group col-sm-3">
                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                                    </div>
                                    <div class="form-group  col-sm-3">
                                        <label class="sr-only" for="exampleInputPassword2">Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                                    </div>
                                    <div class="checkbox col-sm-3">
                                        <label>
                                            <input type="checkbox" class="checkbox">
                                            <span>Remember me </span></label>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        Sign in
                                    </button>
                                </fieldset>
                                
                            </form>
                        </div>
                    </div>

                    <div class="col-md-6 panel-warning">
                        <div class="content-box-header  panel-heading">
                            <div class="panel-title">Inline Form Disabled</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="content-box-large box-with-header">
                            <form class="form-inline" role="form">
                            
                                <fieldset>
                                    <div class="form-group col-sm-3">
                                        <label class="sr-only">Email address</label>
                                        <input type="email" class="form-control" disabled="disabled" placeholder="Enter email">
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label class="sr-only">Password</label>
                                        <input type="password" class="form-control" disabled="disabled" placeholder="Password">
                                    </div>
                                    <div class="checkbox col-sm-3">
                                        <label>
                                            <input type="checkbox" disabled="disabled">
                                            Remember me </label>
                                    </div>
                                    <button type="submit" disabled="disabled" class="btn btn-primary">
                                        Sign in
                                    </button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 panel-info">
                        <div class="content-box-header panel-heading">
                            <div class="panel-title ">Column Sizes</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="content-box-large box-with-header">
                            <div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" placeholder=".col-sm-12">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" placeholder=".col-sm-6">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" placeholder=".col-sm-6">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" placeholder=".col-sm-6">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder=".col-sm-3">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder=".col-sm-3">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder=".col-sm-3">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder=".col-sm-3">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" placeholder=".col-sm-6">
                                    </div>

                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder=".col-sm-4">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder=".col-sm-4">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" placeholder=".col-sm-4">
                                    </div>

                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6 panel-default">
                        <div class="content-box-header panel-heading">
                            <div class="panel-title ">Right Aligned</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="content-box-large box-with-header">
                            <div>

                                <div class="row">
                                    <div class="col-sm-2 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-3 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-3">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-4">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-5 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-5">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-6 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-6">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-7 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-7">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-8 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-8">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-9 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-9">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-10 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-10">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-11 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-11">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-12 pull-right">
                                        <input type="text" class="form-control" placeholder=".col-sm-12">
                                    </div>
                                </div>

                        

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 panel-default">
                        <div class="content-box-header panel-heading">
                            <div class="panel-title ">Left Aligned</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="content-box-large box-with-header">
                            <div>

                                <div class="row">
                                    <div class="col-sm-2 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-2">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-3 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-3">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-4 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-4">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-5 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-5">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-6 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-6">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-7 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-7">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-8 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-8">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-9 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-9">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-10 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-10">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-11 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-11">
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-12 pull-left">
                                        <input type="text" class="form-control" placeholder=".col-sm-12">
                                    </div>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Default Elements</div>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" action="">
                            
                                    <fieldset>
                                        <legend>Default Form Elements</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Text field</label>
                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Default Text Field" type="text">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="text-field">Auto Complete</label>
                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Type somethine..." type="text" list="list">
                                                <datalist id="list">
                                                    <option value="Alexandra"></option>
                                                    <option value="Alice"></option>
                                                    <option value="Anastasia"></option>
                                                    <option value="Avelina"></option>
                                                    <option value="Basilia"></option>
                                                    <option value="Beatrice"></option>
                                                    <option value="Cassandra"></option>
                                                    <option value="Cecil"></option>
                                                    <option value="Clemencia"></option>
                                                    <option value="Desiderata"></option>
                                                    <option value="Dionisia"></option>
                                                    <option value="Edith"></option>
                                                    <option value="Eleanora"></option>
                                                    <option value="Elizabeth"></option>
                                                    <option value="Emma"></option>
                                                    <option value="Felicia"></option>
                                                    <option value="Florence"></option>
                                                    <option value="Galiana"></option>
                                                    <option value="Grecia"></option>
                                                    <option value="Helen"></option>
                                                    <option value="Helewisa"></option>
                                                    <option value="Idonea"></option>
                                                    <option value="Isabel"></option>
                                                    <option value="Joan"></option>
                                                    <option value="Juliana"></option>
                                                    <option value="Karla"></option>
                                                    <option value="Karyn"></option>
                                                    <option value="Kate"></option>
                                                    <option value="Lakisha"></option>
                                                    <option value="Lana"></option>
                                                    <option value="Laura"></option>
                                                    <option value="Leona"></option>
                                                    <option value="Mandy"></option>
                                                    <option value="Margaret"></option>
                                                    <option value="Maria"></option>
                                                    <option value="Nanacy"></option>
                                                    <option value="Nicole"></option>
                                                    <option value="Olga"></option>
                                                    <option value="Pamela"></option>
                                                    <option value="Patricia"></option>
                                                    <option value="Qiana"></option>
                                                    <option value="Rachel"></option>
                                                    <option value="Ramona"></option>
                                                    <option value="Samantha"></option>
                                                    <option value="Sandra"></option>
                                                    <option value="Tanya"></option>
                                                    <option value="Teresa"></option>
                                                    <option value="Ursula"></option>
                                                    <option value="Valerie"></option>
                                                    <option value="Veronica"></option>
                                                    <option value="Wilma"></option>
                                                    <option value="Yasmin"></option>
                                                    <option value="Zelma"></option>
                                                </datalist> 
                                                <p class="note"><strong>Note:</strong> works in Chrome, Firefox, Opera and IE10.</p>
                                            </div>
                                            
                                        </div>
            
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Password field</label>
                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Password field" type="password" value="mypassword">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="textarea">Textarea</label>
                                            <div class="col-md-10">
                                                <textarea class="form-control" placeholder="Textarea" rows="4"></textarea>
                                            </div>
                                        </div>
                                    
                                    
                                    </fieldset>
                                    
                                    <fieldset>
                                        <legend>Unstyled Checkbox</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Checkbox default</label>
                                            <div class="col-md-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox">
                                                        Checkbox 1 </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox">
                                                        Checkbox 2 </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox">
                                                        Checkbox 3 </label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Inline</label>
                                            <div class="col-md-10">
                                                <label class="checkbox-inline">
                                                    <input type="checkbox">
                                                    Checkbox 2 </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox">
                                                    Checkbox 2 </label>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox">
                                                    Checkbox 3 </label>
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    <fieldset>
                                        <legend>Unstyled Radiobox</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Radios default</label>
                                            <div class="col-md-10">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio">
                                                        Radiobox 1 </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio">
                                                        Radiobox 2 </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio">
                                                        Radiobox 3 </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Inline</label>
                                            <div class="col-md-10">
                                                <label class="radio radio-inline">
                                                    <input type="radio">
                                                    Radiobox 1 </label>
                                                <label class="radio radio-inline">
                                                    <input type="radio">
                                                    Radiobox 2 </label>
                                                <label class="radio radio-inline">
                                                    <input type="radio">
                                                    Radiobox 3 </label>
                                            </div>
                                        </div>


                                    
                                    </fieldset>

                                    <fieldset>
                                        <legend>File inputs</legend>
                                    
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">File input</label>
                                            <div class="col-md-10">
                                                <input type="file" class="btn btn-default" id="exampleInputFile1">
                                                <p class="help-block">
                                                    some help text here.
                                                </p>
                                            </div>
                                        </div>
                                        
                                    </fieldset>

                                    
                                    <fieldset>
                                        <legend>Unstyled Select</legend>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="select-1">Select</label>
                                            <div class="col-md-10">
            
                                                <select class="form-control" id="select-1">
                                                    <option>Amsterdam</option>
                                                    <option>Atlanta</option>
                                                    <option>Baltimore</option>
                                                    <option>Boston</option>
                                                    <option>Buenos Aires</option>
                                                    <option>Calgary</option>
                                                    <option>Chicago</option>
                                                    <option>Denver</option>
                                                    <option>Dubai</option>
                                                    <option>Frankfurt</option>
                                                    <option>Hong Kong</option>
                                                    <option>Honolulu</option>
                                                    <option>Houston</option>
                                                    <option>Kuala Lumpur</option>
                                                    <option>London</option>
                                                    <option>Los Angeles</option>
                                                    <option>Melbourne</option>
                                                    <option>Mexico City</option>
                                                    <option>Miami</option>
                                                    <option>Minneapolis</option>
                                                </select> 
            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="multiselect1">Multiple select</label>
                                            <div class="col-md-10">
                                                <select multiple="multiple" id="multiselect-1" class="form-control custom-scroll" title="Click to Select a City">
                                                    <option>Amsterdam</option>
                                                    <option selected="selected">Atlanta</option>
                                                    <option>Baltimore</option>
                                                    <option>Boston</option>
                                                    <option>Buenos Aires</option>
                                                    <option>Calgary</option>
                                                    <option selected="selected">Chicago</option>
                                                    <option>Denver</option>
                                                    <option>Dubai</option>
                                                    <option>Frankfurt</option>
                                                    <option>Hong Kong</option>
                                                    <option>Honolulu</option>
                                                    <option>Houston</option>
                                                    <option>Kuala Lumpur</option>
                                                    <option>London</option>
                                                    <option>Los Angeles</option>
                                                    <option>Melbourne</option>
                                                    <option>Mexico City</option>
                                                    <option>Miami</option>
                                                    <option>Minneapolis</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                    
                                    <fieldset>
                                        <legend>Input States</legend>
                                        
                                        <div class="form-group has-warning">
                                            <label class="col-md-2 control-label">Input warning</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <input class="form-control" type="text">
                                                    <span class="input-group-addon"><i class="fa fa-warning"></i></span>
                                                </div>
                                                <span class="help-block">Something may have gone wrong</span>
                                            </div>

                                        </div>
                                        
                                        <div class="form-group has-error">
                                            <label class="col-md-2 control-label">Input error</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <input class="form-control" type="text">
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-remove-circle"></i></span>
                                                </div>
                                                <span class="help-block"><i class="fa fa-warning"></i> Please correct the error</span>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group has-success">
                                            <label class="col-md-2 control-label">Input success</label>
                                            <div class="col-md-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                                    <input class="form-control" type="text">
                                                    <span class="input-group-addon"><i class="fa fa-check"></i></span>
                                                </div>
                                                <span class="help-block">Something may have gone wrong</span>
                                            </div>
                                        </div>
                                        
                                    </fieldset> 
                                    
                                    <fieldset>
                                        <legend>Input sizes</legend>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Extra Small Input</label>
                                            <div class="col-md-10">
                                                <input class="form-control input-xs" placeholder=".input-xs" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Small Input</label>
                                            <div class="col-md-10">
                                                <input class="form-control input-sm" placeholder=".input-sm" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Default Input</label>
                                            <div class="col-md-10">
                                                <input class="form-control" placeholder="Default input" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Large Input</label>
                                            <div class="col-md-10">
                                                <input class="form-control input-lg" placeholder=".input-lg" type="text">
                                            </div>
                                        </div>

                                    </fieldset>
                                    
                                    <fieldset>
                                        <legend>Select Sizes</legend>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Small Select</label>
                                            <div class="col-md-10">
                                                <select class="form-control input-sm">
                                                    <option>Amsterdam</option>
                                                    <option>Atlanta</option>
                                                    <option>Baltimore</option>
                                                    <option>Boston</option>
                                                    <option>Buenos Aires</option>
                                                    <option>Calgary</option>
                                                    <option>Chicago</option>
                                                    <option>Denver</option>
                                                    <option>Dubai</option>
                                                    <option>Frankfurt</option>
                                                    <option>Hong Kong</option>
                                                    <option>Honolulu</option>
                                                    <option>Houston</option>
                                                    <option>Kuala Lumpur</option>
                                                    <option>London</option>
                                                    <option>Los Angeles</option>
                                                    <option>Melbourne</option>
                                                    <option>Mexico City</option>
                                                    <option>Miami</option>
                                                    <option>Minneapolis</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Default Select</label>
                                            <div class="col-md-10">
                                                <select class="form-control">
                                                    <option>Amsterdam</option>
                                                    <option>Atlanta</option>
                                                    <option>Baltimore</option>
                                                    <option>Boston</option>
                                                    <option>Buenos Aires</option>
                                                    <option>Calgary</option>
                                                    <option>Chicago</option>
                                                    <option>Denver</option>
                                                    <option>Dubai</option>
                                                    <option>Frankfurt</option>
                                                    <option>Hong Kong</option>
                                                    <option>Honolulu</option>
                                                    <option>Houston</option>
                                                    <option>Kuala Lumpur</option>
                                                    <option>London</option>
                                                    <option>Los Angeles</option>
                                                    <option>Melbourne</option>
                                                    <option>Mexico City</option>
                                                    <option>Miami</option>
                                                    <option>Minneapolis</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Large Select</label>
                                            <div class="col-md-10">
                                                <select class="form-control input-lg">
                                                    <option>Amsterdam</option>
                                                    <option>Atlanta</option>
                                                    <option>Baltimore</option>
                                                    <option>Boston</option>
                                                    <option>Buenos Aires</option>
                                                    <option>Calgary</option>
                                                    <option>Chicago</option>
                                                    <option>Denver</option>
                                                    <option>Dubai</option>
                                                    <option>Frankfurt</option>
                                                    <option>Hong Kong</option>
                                                    <option>Honolulu</option>
                                                    <option>Houston</option>
                                                    <option>Kuala Lumpur</option>
                                                    <option>London</option>
                                                    <option>Los Angeles</option>
                                                    <option>Melbourne</option>
                                                    <option>Mexico City</option>
                                                    <option>Miami</option>
                                                    <option>Minneapolis</option>
                                                </select>
                                            </div>
                                        </div>

                                    </fieldset>
                                    
                                    <fieldset>
                                        <legend>Prepend &amp; Append</legend>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="prepend">Prepended Input</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">@</span>
                                                            <input class="form-control" id="prepend" placeholder="Username" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="prepend">W/ input &amp; radios</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <span class="onoffswitch">
                                                                    <input type="checkbox" name="start_interval" class="onoffswitch-checkbox" id="st3">
                                                                    <label class="onoffswitch-label" for="st3"> 
                                                                        <div class="onoffswitch-inner" data-swchon-text="YES" data-swchoff-text="NO"></div> 
                                                                        <div class="onoffswitch-switch"></div> 
                                                                    </label> 
                                                                </span>
                                                            </span>
                                                            <input class="form-control" placeholder="" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                            
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="append">Appended Input</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">
                                                            <input class="form-control" id="append" type="text">
                                                            <span class="input-group-addon">.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="appendprepend">Combined</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            <input class="form-control" id="appendprepend" type="text">
                                                            <span class="input-group-addon">.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
                                        <div class="form-group">
                                            <label class="control-label col-md-2" for="appendbutton">With buttons</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="input-group">
                                                            <input class="form-control" id="appendbutton" type="text">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-default" type="button">
                                                                    Search
                                                                </button>
                                                                <button class="btn btn-default" type="button">
                                                                    Options
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
                                        <div class="form-group">
                                            <label class="control-label col-md-2">With dropdowns</label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        
                                                        <div class="input-group">
                                                            <input type="text" class="form-control">
                                                            <div class="input-group-btn">
                                                                <button type="button" class="btn btn-default" tabindex="-1">Action</button>
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu pull-right" role="menu">
                                                                    <li><a href="javascript:void(0);">Action</a></li>
                                                                    <li><a href="javascript:void(0);">Another action</a></li>
                                                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="javascript:void(0);">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-2"></label>
                                            <div class="col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        
                                                        <div class="input-group">
                                                            <div class="input-group-btn">
                                                                <button type="button" class="btn btn-default" tabindex="-1">Action</button>
                                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" role="menu">
                                                                    <li><a href="javascript:void(0);">Action</a></li>
                                                                    <li><a href="javascript:void(0);">Another action</a></li>
                                                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="javascript:void(0);">Cancel</a></li>
                                                                </ul>
                                                            </div>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                    
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-default" type="submit">
                                                    Cancel
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-save"></i>
                                                    Submit
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="content-box-large">
                            <div class="panel-heading">
                                <div class="panel-title">Form Widgets</div>
                                
                                <div class="hide">
                                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                    <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                
                                <div>
                                    <h4>Select</h4>
                                    <p>
                                        <div class="bfh-selectbox" data-name="selectbox3" data-value="12" data-filter="true">
                                          <div data-value="1">Option 1</div>
                                          <div data-value="2">Option 2</div>
                                          <div data-value="3">Option 3</div>
                                          <div data-value="4">Option 4</div>
                                          <div data-value="5">Option 5</div>
                                          <div data-value="6">Option 6</div>
                                          <div data-value="7">Option 7</div>
                                          <div data-value="8">Option 8</div>
                                          <div data-value="9">Option 9</div>
                                          <div data-value="10">Option 10</div>
                                          <div data-value="11">Option 11</div>
                                          <div data-value="12">Option 12</div>
                                          <div data-value="13">Option 13</div>
                                          <div data-value="14">Option 14</div>
                                          <div data-value="15">Option 15</div>
                                        </div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Color Picker</h4>
                                    <p>
                                        <div class="bfh-colorpicker" data-name="colorpicker1"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Date Picker</h4>
                                    <p>
                                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Time Picker</h4>
                                    <p>
                                        <div class="bfh-timepicker" data-mode="12h"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Slider</h4>
                                    <p>
                                        <div class="bfh-slider" data-name="slider1"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Country Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-countries" data-country="US" data-flags="true"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>State Picker</h4>
                                    <p>
                                        <div id="countries_states2" class="bfh-selectbox bfh-countries" data-country="US"></div>
                                        <br><br>
                                        <div class="bfh-selectbox bfh-states" data-country="countries_states2"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Currency Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-currencies" data-currency="EUR" data-flags="true"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Language Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-languages" data-language="en_US" data-flags="true"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Timezone Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-timezones" data-country="US"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Google Font Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-googlefonts" data-font="Lato"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Font Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-fonts" data-font="Arial"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Font Size Picker</h4>
                                    <p>
                                        <div class="bfh-selectbox bfh-fontsizes" data-fontsize="12"></div>
                                    </p>
                                </div>

                                <div>
                                    <h4>Select</h4>
                                    <p>
                                        <select class="selectpicker">
                                            <option>Mustard</option>
                                            <option>Ketchup</option>
                                            <option>Relish</option>
                                          </select>
                                    </p>
                                </div>

                                <div>
                                    <h4>Select With Groups</h4>
                                    <p>
                                        <select class="selectpicker">
                                            <optgroup label="Picnic">
                                              <option>Mustard</option>
                                              <option>Ketchup</option>
                                              <option>Relish</option>
                                            </optgroup>
                                            <optgroup label="Camping">
                                              <option>Tent</option>
                                              <option>Flashlight</option>
                                              <option>Toilet Paper</option>
                                            </optgroup>
                                          </select>
                                    </p>
                                </div>

                                <div>
                                    <h4>Select Multiple</h4>
                                    <p>
                                        <select class="selectpicker" multiple>
                                          <option>Mustard</option>
                                          <option>Ketchup</option>
                                          <option>Relish</option>
                                      </select>
                                    </p>
                                </div>

                                <div>
                                    <h4>Tags</h4>
                                    <p>
                                        <div id="tags"></div>
                                    </p>
                                </div>

                                <fieldset>
                                    <legend>
                                        Masking
                                    </legend>

                                    <div class="form-group">
                                        <label for="h-input">Date masking</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control mask-date" data-mask="99/99/9999" data-mask-placeholder="-">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <p class="note">
                                            Data format **/**/****
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label for="h-input">Phone masking</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="(999) 999-9999" data-mask-placeholder="X">
                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        </div>
                                        <p class="note">
                                            Data format (XXX) XXX-XXXX
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label for="h-input">Credit card masking</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="9999-9999-9999-9999" data-mask-placeholder="*">
                                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                        </div>
                                        <p class="note">
                                            Data format ****-****-****-****
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label for="h-input">Serial number masking</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="***-***-***-***-***-***" data-mask-placeholder="_">
                                            <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                                        </div>
                                        <p class="note">
                                            Data format ***-***-***-***-***-***
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label for="h-input">Tax ID masking</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="99-9999999" data-mask-placeholder="X">
                                            <span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
                                        </div>
                                        <p class="note">
                                            Data format 99-9999999
                                        </p>
                                    </div>

                                </fieldset>

                                <div>
                                    <h4>Inline Edit</h4>
                                    <p>
                                        <table id="user" class="table table-bordered " style="clear: both">
                                            <tbody> 
                                                <tr>         
                                                    <td width="35%">Simple text field</td>
                                                    <td width="65%"><a href="#" id="username" data-type="text" data-pk="1" data-title="Enter username" class="editable editable-click" data-original-title="" title="">superuser</a></td>
                                                </tr>
                                                <tr>         
                                                    <td>Empty text field, required</td>
                                                    <td><a href="#" id="firstname" data-type="text" data-pk="1" data-placement="right" data-placeholder="Required" data-title="Enter your firstname" class="editable editable-click editable-empty">Empty</a></td>
                                                </tr>  
                                                <tr>         
                                                    <td>Select, local array, custom display</td>
                                                    <td><a href="#" id="sex" data-type="select" data-pk="1" data-value="" data-title="Select sex" class="editable editable-click" style="color: gray;">not selected</a></td>
                                                </tr>
                                                <tr>         
                                                    <td>Select, remote array, no buttons</td>
                                                    <td><a href="#" id="group" data-type="select" data-pk="1" data-value="5" data-title="Select group" class="editable editable-click">Admin</a></td>
                                                </tr> 
                                                <tr>         
                                                    <td>Select, error while loading</td>
                                                    <td><a href="#" id="status" data-type="select" data-pk="1" data-value="0" data-title="Select status" class="editable editable-click">Active</a></td>
                                                </tr>  
                                                     
                                                <tr>         
                                                    <td>Combodate (date)</td>
                                                    <td><a href="#" id="dob" data-type="combodate" data-value="1984-05-15" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1" data-title="Select Date of birth" class="editable editable-click">15/05/1984</a></td>
                                                </tr> 
                                                <tr>         
                                                    <td>Combodate (datetime)</td>
                                                    <td><a href="#" id="event" data-type="combodate" data-template="D MMM YYYY  HH:mm" data-format="YYYY-MM-DD HH:mm" data-viewformat="MMM D, YYYY, HH:mm" data-pk="1" data-title="Setup event date and time" class="editable editable-click editable-empty">Empty</a></td>
                                                </tr> 
                                                
                                                                     
                                                                    
                                                <tr>         
                                                    <td>Textarea, buttons below. Submit by <i>ctrl+enter</i></td>
                                                    <td><a href="#" id="comments" data-type="textarea" data-pk="1" data-placeholder="Your comments here..." data-title="Enter comments" class="editable editable-pre-wrapped editable-click">awesome
                            user!</a></td>
                                                </tr>             
                                            </tbody>
                                        </table>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="content-box-large">
                        <div class="panel-heading">
                            <div class="panel-title">Bootstrap Wizard</div>
                            
                            <div class="hide">
                                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="rootwizard">
                                <div class="navbar">
                                  <div class="navbar-inner">
                                    <div class="content">
                                <ul class="nav nav-pills">
                                    <li class="active"><a href="#tab1" data-toggle="tab">First</a></li>
                                    <li><a href="#tab2" data-toggle="tab">Second</a></li>
                                    <li><a href="#tab3" data-toggle="tab">Third</a></li>
                                </ul>
                                 </div>
                                  </div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1">
                                      <form class="form-horizontal" role="form">
                                          <div class="form-group">
                                            <label for="inputEmail3" class="col-sm-2 control-label">Text Field</label>
                                            <div class="col-sm-10">
                                              <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Password Field</label>
                                            <div class="col-sm-10">
                                              <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Textarea</label>
                                            <div class="col-sm-10">
                                              <textarea class="form-control" placeholder="Textarea" rows="3"></textarea>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Readonly</label>
                                            <div class="col-sm-10">
                                              <span class="form-control">Read only text</span>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Checkbox
                                                </label>
                                              </div>
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Checkbox
                                                </label>
                                              </div>
                                            </div>
                                          </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                      <form class="form-inline" role="form">
                            
                                            <fieldset>
                                                <div class="form-group col-sm-3">
                                                    <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                                                </div>
                                                <div class="form-group  col-sm-3">
                                                    <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                    <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                                                </div>
                                                <div class="checkbox col-sm-3">
                                                    <label>
                                                        <input type="checkbox" class="checkbox">
                                                        <span>Remember me </span></label>
                                                </div>
                                                <button type="submit" class="btn btn-primary">
                                                    Sign in
                                                </button>
                                            </fieldset>
                                            
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                        <fieldset>
                                            <legend>Unstyled Checkbox</legend>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Checkbox default</label>
                                                <div class="col-md-10">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox">
                                                            Checkbox 1 </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox">
                                                            Checkbox 2 </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox">
                                                            Checkbox 3 </label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Inline</label>
                                                <div class="col-md-10">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox">
                                                        Checkbox 2 </label>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox">
                                                        Checkbox 2 </label>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox">
                                                        Checkbox 3 </label>
                                                </div>
                                            </div>
                                            
                                        </fieldset>
                                    </div>
                                    <ul class="pager wizard">
                                        <li class="previous first disabled" style="display:none;"><a href="javascript:void(0);">First</a></li>
                                        <li class="previous disabled"><a href="javascript:void(0);">Previous</a></li>
                                        <li class="next last" style="display:none;"><a href="javascript:void(0);">Last</a></li>
                                        <li class="next"><a href="javascript:void(0);">Next</a></li>
                                    </ul>
                                </div>  
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row">
          <div class="col-md-6">
            <div class="content-box-large">
              <div class="panel-body">
                    <h4>Extra Small Buttons</h4>
                    <p>
                      <button class="btn btn-default btn-xs">Default</button>
                      <button class="btn btn-primary btn-xs">Primary</button>
                      <button class="btn btn-info btn-xs">Info</button>
                      <button class="btn btn-success btn-xs">Success</button>
                      <button class="btn btn-warning btn-xs">Warning</button>
                      <button class="btn btn-danger btn-xs">Danger</button>
                    </p>
                    <h4>Small Buttons</h4>
                    <p>
                      <button class="btn btn-default btn-sm">Default</button>
                      <button class="btn btn-primary btn-sm">Primary</button>
                      <button class="btn btn-info btn-sm">Info</button>
                      <button class="btn btn-success btn-sm">Success</button>
                      <button class="btn btn-warning btn-sm">Warning</button>
                      <button class="btn btn-danger btn-sm">Danger</button>
                    </p>
                    <h4>Normal Buttons</h4>
                    <p>
                      <button class="btn btn-default">Default</button>
                      <button class="btn btn-primary">Primary</button>
                      <button class="btn btn-info">Info</button>
                      <button class="btn btn-success">Success</button>
                      <button class="btn btn-warning">Warning</button>
                      <button class="btn btn-danger">Danger</button>
                    </p>
                    <h4>Large Buttons</h4>
                    <p>
                      <button class="btn btn-default btn-lg">Default</button>
                      <button class="btn btn-primary btn-lg">Primary</button>
                      <button class="btn btn-info btn-lg">Info</button>
                      <button class="btn btn-success btn-lg">Success</button>
                      <button class="btn btn-warning btn-lg">Warning</button>
                      <button class="btn btn-danger btn-lg">Danger</button>
                    </p>

                    <div class="well" style="margin-top:30px;">
                        <button type="button" class="btn btn-lg btn-block btn-primary">Block level Primary button</button>
                        <button type="button" class="btn btn-lg btn-block btn-default">Block level Default button</button>
                      </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="content-box-large">
              <div class="panel-body">
                <h4>Button Dropdowns</h4>
                <p>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      Default <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                      Primary <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                      Info <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                      Success <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                      Warning <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                      Danger <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                </p>

                <h4>Split Button Dropdowns</h4>
                <p>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default">Default</button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary">Primary</button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-info">Info</button>
                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-success">Success</button>
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-warning">Warning</button>
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <div class="btn-group">
                    <button type="button" class="btn btn-danger">Danger</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                </p>

                <h4>Buttons With Icons</h4>
                <p>This is an example buttons with icons. About 140 icons are available.</p>
                <p>
                      <button class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i> View</button>
                      <button class="btn btn-info"><i class="glyphicon glyphicon-refresh"></i> Update</button>
                      <button class="btn btn-primary"><i class="glyphicon glyphicon-pencil"></i> Edit</button>
                      <button class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Delete</button>
                </p>

                <p>And there this is a toolbar icon example</p>
                <p>
                      <div class="btn-group">
                        <button class="btn btn-default"><i class="glyphicon glyphicon-step-backward"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-fast-backward"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-backward"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-play"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-pause"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-stop"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-forward"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-fast-forward"></i></button>
                        <button class="btn btn-default"><i class="glyphicon glyphicon-step-forward"></i></button>
                      </div>
                </p>


              </div>
            </div>
          </div>

        </div>

      <!-- Icons -->
      <div class="row">
          <div class="col-md-12 panel-warning">
            <div class="content-box-header panel-heading">
              <div class="panel-title ">Available Icons</div>
            
              <div class="hide">
                <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
              </div>
            </div>
            <div class="content-box-large box-with-header">
              <ul class="bs-glyphicons">
                <li>
                  <span class="glyphicon glyphicon-adjust"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-adjust</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-align-center"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-align-center</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-align-justify"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-align-justify</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-align-left"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-align-left</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-align-right"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-align-right</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-arrow-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-arrow-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-arrow-left"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-arrow-left</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-arrow-right"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-arrow-right</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-arrow-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-arrow-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-asterisk"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-asterisk</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-backward"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-backward</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-ban-circle"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-ban-circle</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-barcode"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-barcode</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-bell"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-bell</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-bold"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-bold</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-book"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-book</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-bookmark"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-bookmark</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-briefcase"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-briefcase</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-bullhorn"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-bullhorn</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-calendar"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-calendar</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-camera"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-camera</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-certificate"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-certificate</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-check"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-check</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-chevron-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-chevron-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-chevron-left</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-chevron-right</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-chevron-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-chevron-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-circle-arrow-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-circle-arrow-left"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-left</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-circle-arrow-right"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-right</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-circle-arrow-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-circle-arrow-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-cloud"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-cloud</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-cloud-download"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-cloud-download</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-cloud-upload"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-cloud-upload</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-cog"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-cog</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-collapse-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-collapse-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-collapse-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-collapse-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-comment"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-comment</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-compressed"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-compressed</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-copyright-mark"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-copyright-mark</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-credit-card"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-credit-card</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-cutlery"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-cutlery</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-dashboard"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-dashboard</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-download"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-download</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-download-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-download-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-earphone"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-earphone</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-edit"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-edit</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-eject"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-eject</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-envelope"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-envelope</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-euro"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-euro</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-exclamation-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-exclamation-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-expand"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-expand</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-export"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-export</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-eye-close"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-eye-close</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-eye-open"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-eye-open</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-facetime-video"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-facetime-video</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-fast-backward"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-fast-backward</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-fast-forward"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-fast-forward</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-file"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-file</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-film"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-film</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-filter"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-filter</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-fire"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-fire</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-flag"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-flag</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-flash"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-flash</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-floppy-disk"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-floppy-disk</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-floppy-open"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-floppy-open</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-floppy-remove"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-floppy-remove</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-floppy-save"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-floppy-save</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-floppy-saved"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-floppy-saved</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-folder-close"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-folder-close</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-folder-open"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-folder-open</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-font"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-font</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-forward"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-forward</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-fullscreen"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-fullscreen</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-gbp"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-gbp</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-gift"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-gift</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-glass"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-glass</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-globe"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-globe</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-hand-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-hand-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-hand-left"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-hand-left</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-hand-right"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-hand-right</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-hand-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-hand-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-hd-video"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-hd-video</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-hdd"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-hdd</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-header"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-header</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-headphones"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-headphones</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-heart"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-heart</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-heart-empty"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-heart-empty</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-home"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-home</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-import"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-import</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-inbox"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-inbox</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-indent-left"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-indent-left</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-indent-right"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-indent-right</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-info-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-info-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-italic"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-italic</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-leaf"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-leaf</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-link"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-link</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-list"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-list</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-list-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-list-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-lock"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-lock</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-log-in"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-log-in</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-log-out"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-log-out</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-magnet"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-magnet</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-map-marker"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-map-marker</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-minus"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-minus</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-minus-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-minus-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-move"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-move</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-music"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-music</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-new-window"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-new-window</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-off"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-off</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-ok"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-ok</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-ok-circle"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-ok-circle</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-ok-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-ok-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-open"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-open</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-paperclip"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-paperclip</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-pause"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-pause</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-pencil"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-pencil</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-phone"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-phone</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-phone-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-phone-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-picture"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-picture</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-plane"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-plane</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-play"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-play</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-play-circle"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-play-circle</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-plus"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-plus</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-plus-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-plus-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-print"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-print</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-pushpin"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-pushpin</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-qrcode"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-qrcode</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-question-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-question-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-random"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-random</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-record"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-record</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-refresh"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-refresh</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-registration-mark"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-registration-mark</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-remove"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-remove</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-remove-circle"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-remove-circle</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-remove-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-remove-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-repeat"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-repeat</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-resize-full"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-resize-full</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-resize-horizontal"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-resize-horizontal</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-resize-small"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-resize-small</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-resize-vertical"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-resize-vertical</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-retweet"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-retweet</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-road"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-road</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-save"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-save</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-saved"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-saved</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-screenshot"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-screenshot</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sd-video"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sd-video</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-search"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-search</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-send"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-send</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-share"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-share</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-share-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-share-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-shopping-cart"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-shopping-cart</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-signal"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-signal</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort-by-alphabet"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort-by-alphabet</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort-by-alphabet-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort-by-attributes"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort-by-attributes</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort-by-attributes-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort-by-attributes-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort-by-order"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort-by-order</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sort-by-order-alt"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sort-by-order-alt</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sound-5-1"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sound-5-1</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sound-6-1"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sound-6-1</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sound-7-1"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sound-7-1</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sound-dolby"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sound-dolby</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-sound-stereo"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-sound-stereo</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-star"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-star</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-star-empty"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-star-empty</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-stats"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-stats</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-step-backward"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-step-backward</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-step-forward"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-step-forward</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-stop"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-stop</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-subtitles"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-subtitles</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tag"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tag</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tags"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tags</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tasks"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tasks</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-text-height"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-text-height</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-text-width"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-text-width</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-th"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-th</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-th-large"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-th-large</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-th-list"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-th-list</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-thumbs-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-thumbs-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-thumbs-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-thumbs-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-time"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-time</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tint"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tint</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tower"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tower</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-transfer"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-transfer</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-trash"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-trash</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tree-conifer"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tree-conifer</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-tree-deciduous"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-tree-deciduous</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-unchecked"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-unchecked</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-upload"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-upload</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-usd"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-usd</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-user"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-user</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-volume-down"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-volume-down</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-volume-off"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-volume-off</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-volume-up"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-volume-up</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-warning-sign"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-warning-sign</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-wrench"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-wrench</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-zoom-in"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-zoom-in</span>
                </li>
                <li>
                  <span class="glyphicon glyphicon-zoom-out"></span>
                  <span class="glyphicon-class">glyphicon glyphicon-zoom-out</span>
                </li>
              </ul>
            </div>
          </div>
      </div>