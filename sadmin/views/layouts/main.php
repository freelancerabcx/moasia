<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use common\models\User;
use yii\helpers\Url;

AppAsset::register($this);
?>
    <?php $this->beginPage() ?>
        <!doctype html>
        <html lang="<?= Yii::$app->language ?>">

        <head>
            <meta charset="utf-8" />
            <?= Html::csrfMetaTags() ?>
                <title>
                    <?= Html::encode($this->title) ?>
                </title>
                <?php $this->head() ?>
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <!-- Bootstrap -->
                <link href="/admin/files/bootstrap/css/bootstrap.min.css" rel="stylesheet">
                <!-- styles -->
                <link href="/admin/files/css/styles.css" rel="stylesheet">
        </head>

        <body>
            <?php $this->beginBody() ?>
                <nav class="navbar nav-moringga">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="/admin" style="padding:5px;">
                                <img src="/admin/files/img/logo.png" style="opacity: 0" height="30" width="0" alt="logo">
                                <big><big>MORINGNA ASIA</big></big> | 
                            </a>
                            <span class="title-page"><?=$this->title;?></span>

                        </div>
                        <ul class="nav navbar-right">
                            <?php if(Yii::$app->user->identity != null && Yii::$app->user->identity->role_id == User::ROLE_SADMIN):?>
                                <li class="item">
                                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">Report<b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Dashboard', ['site/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('History Bonus', ['site/history'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('History Day', ['history-day/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Dashboard Product', ['site/product'], []) ?>
                                        </li>
                                    </ul>
                                </li>
                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings<b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Config', ['config/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Country', ['country/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Currency', ['currency/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Bank', ['bank/index'], []) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Users Management', ['user/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Transactions', ['transactions/index'], []) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Products Management', ['products/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Categories', ['productcategory/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Orders', ['orders/index'], []) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Profile', ['user/profile'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Logout', ['site/logout'], []) ?>
                                        </li>
                                    </ul>
                                </li>
                                <?php endif; ?>
                            <?php if(Yii::$app->user->identity != null && Yii::$app->user->identity->role_id == User::ROLE_ADMIN):?>
     
                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Users Management', ['user/index'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Transactions', ['transactions/index'], []) ?>
                                        </li>
                                    </ul>
                                </li>


                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Profile', ['user/profile'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Logout', ['site/logout'], []) ?>
                                        </li>
                                    </ul>
                                </li>
                                <?php endif; ?>
                            <?php if(Yii::$app->user->identity != null && Yii::$app->user->identity->role_id == User::ROLE_PADMIN):?>

                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Orders', ['orders/index'], []) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li class="item dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li>
                                            <?= Html::a('Profile', ['user/profile'], []) ?>
                                        </li>
                                        <li>
                                            <?= Html::a('Logout', ['site/logout'], []) ?>
                                        </li>
                                    </ul>
                                </li>
                                <?php endif; ?>
                        </ul>
                    </div>
                </nav>

                <div class="container page-content">
                    <?= $content ?>
                </div>

                <footer class="footer">
                    <div class="container">
                        <div class="copy text-center">
                            Copyright 2016 <a href='/'>Moringna Asia</a> All rights reserved
                        </div>
                    </div>
                </footer>
                <script src="/admin/files/js/jquery.js"></script>
                <script src="/admin/files/bootstrap/js/bootstrap.min.js"></script>
                <script src="/admin/files/js/custom.js"></script>
                <script src="/admin/files/js/app.js"></script>
                <script type="text/javascript">
                    var checkAction = false;
                    var countInt = 0;
                    setInterval(function(){
                        if(countInt >= 5*60){
                            location.href="<?=Url::to(['site/logout']);?>";
                        }
                        countInt++;
                    }, 1000);

                    $(window).keyup(function(){
                        countInt = 0;
                    })

                    $(window).mousemove(function(){
                        countInt = 0;
                    })
                </script>
                <?php $this->endBody() ?>
        </body>

        </html>
        <?php $this->endPage() ?>