<?php

use yii\helpers\Html;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model common\models\TblUsers */

$this->title = 'Update User: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Admin', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
if($model->role_id == User::ROLE_ADMIN && Yii::$app->user->identity->role_id == User::ROLE_ADMIN)
	return Yii::$app->response->redirect(['user/index']);
?>

<div class="tbl-users-create">

	<div class="content-box-large">
	    <div class="panel-heading row">
	        <div class="panel-title"><?=$this->title;?></div>

	        <div class="hide">
	            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
	            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
	        </div>
	    </div>
	    <div class="panel-body">
	        <div class="tab-content">
	            <div class="tab-pane active" id="tab1">
				    <?= $this->render('_form', [
				        'model' => $model,
		                "bonus" => $bonus,
		                "propoint" => $propoint,
		                "register" => $register
				    ]) ?>
				</div>
			</div>
		</div>
	</div>
</div>