<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use common\models\TblRank;
use common\models\TblRoles;
use common\models\TblCurrency;
use common\models\TblCountry;
use common\models\TblBanks;
use kartik\select2\Select2;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model common\models\TblUsers */
/* @var $form yii\widgets\ActiveForm */
$dropDownList = [
    0 => "Female",
    1 => "Male",
    3 => "Unknown"
];

$status = [
    1 => "Active",
    0 => "Block"
];

$tmp = TblRoles::getList();
if(empty($isProfile))
    unset($tmp['1']);
?>

<div class="tbl-users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if($model->isNewRecord) echo $form->field($model, 'password')->passwordInput() ?>
    <?php if($model->isNewRecord) echo $form->field($model, 'password_repeat')->passwordInput() ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true, 
            'id' => 'fullname', 
            'onkeyup' => "syncName(1)"
    ]) ?>

    <?= $form->field($model, 'identity')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php if(empty($isProfile)): ?>
        <?= $form->field($model, 'role_id')->dropDownList($tmp, [
            "onchange" => $model->isNewRecord ? "if(this.value != ".User::ROLE_FREE_MEMBER.") $('#user-rank_id').val(0);" : '',
            "disabled" => (Yii::$app->user->identity->role_id != User::ROLE_SADMIN && !$model->isNewRecord)
        ])->label('Role') ?>

        <?= $form->field($model, 'rank_id')->dropDownList(TblRank::getList(), [
            "onchange" => $model->isNewRecord ? "if(this.value != 0) $('#user-role_id').val('".User::ROLE_FREE_MEMBER."');" : '',
            "disabled" => (Yii::$app->user->identity->role_id != User::ROLE_SADMIN && !$model->isNewRecord)
        ]) ?>
    <?php else:?>
        <div style="display: none">
            <?php 
                $model->role_id = Yii::$app->user->identity->role_id;
                $model->rank_id = TblRank::BRONZE;
            ?>
            <?= $form->field($model, 'role_id')->dropDownList($tmp)->label('Role') ?>

            <?= $form->field($model, 'rank_id')->dropDownList(TblRank::getList(), [
                "onchange" => $model->isNewRecord ? "if(this.value != 0) $('#user-role_id').val('".User::ROLE_FREE_MEMBER."');" : '',
                "disabled" => (Yii::$app->user->identity->role_id != User::ROLE_SADMIN && !$model->isNewRecord)
            ]) ?>
        </div>
    <?php endif;?>


    <?php if(empty($isProfile)): ?>
    <?php 
                    $url = \yii\helpers\Url::to(['user/get-parents']);
                    $initScript =
<<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;

                    echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                        'options' => [
                            'placeholder' => 'Searching for parent user ...', 
                            'multiple' => false
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(term,page) { return {search:term}; }'),
                                'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                            ],
                            'initSelection' => new JsExpression($initScript)
                        ],
                    ]);
                    

        ?>
    <?php endif; ?>

    <?php 
        if(!$model->isNewRecord)
            $model->birthday = Date('d-M-Y', $model->birthday);
    ?>

    <?= $form->field($model, 'birthday')->widget(
        DatePicker::className(), [
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'dd-M-yyyy',
                'todayHighlight' => true
            ]
    ]);?>

    <?= $form->field($model, 'gender')->dropDownList($dropDownList) ?>
    <?= $form->field($model, 'status')->dropDownList($status) ?>

    <br><br><br>
    <div class="panel-title">Address information</div>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'country_id')->dropDownList(TblCountry::getList(), [
        "onchange" => "getBank(this.value)"
    ])->label('Country') ?>

    <br><br><br>
    <div class="panel-title">Bank information</div>
    <?= $form->field($model, 'bank_account')->textInput(['maxlength' => true]) ?>
    <div class="form-group field-user-city">
        <label class="control-label" for="user-city">Full Name</label>
        <input type="text" id="fullname2" class="form-control" onkeyup="syncName(2)" value="<?=$model->fullname;?>">
        <div class="help-block"></div>
    </div>
    <div id="cnt"></div>
    <?php if(Yii::$app->user->identity->role_id == User::ROLE_SADMIN && empty($isProfile)):?>
        <br><br><br>
        <div class="panel-title">Wallets</div>
        <br><br>
        <div class="form-group required">
        <label class="control-label" for="user-fullname">Register Wallet</label>
        <input type="number" min="0" class="form-control" name="User[register]" value="<?=$register;?>" required step="0.01">

        <div class="help-block"></div>
        </div>

        <div class="form-group required">
        <label class="control-label" for="user-fullname">Product Point</label>
        <input type="number" min="0" class="form-control" name="User[propoint]" value="<?= $propoint; ?>" required step="0.01">

        <div class="help-block"></div>
        </div>

        <div class="form-group required">
        <label class="control-label" for="user-fullname">Bonus Wallet</label>
        <input type="number" min="0" class="form-control" name="User[bonus]" value="<?=$bonus;?>" required step="0.01">
        <div class="help-block"></div>
    <?php endif;?>
    <?= $form->field($model, 'updated_by')->hiddenInput(['value' => 1])->label('') ?>
    <div style="display: none">
        <?= $form->field($model, 'updated_at')->hiddenInput(['value' => time()])->label('') ?>
        <?= $form->field($model, 'currency_id')->dropDownList(TblCurrency::getList()) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    function getBank(country_id)
    {
        $("#cnt").load("<?=Url::to(["/user/get-bank?country_id="]);?>"+country_id+"&currentbank_id=<?=$model->bank_id;?>");
    }

    window.addEventListener("load", function(){
        getBank($("#user-country_id").val());
    });
    function syncName(t){
        console.log($("#fullname2").val());
        if(t == 2)
            $("#fullname").val($("#fullname2").val());
        else
            $("#fullname2").val($("#fullname").val());
    }
</script>