<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\TblRank;
use common\models\TblRoles;
use common\models\User;
use common\models\TblCountry;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Management';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-box-large">
    <div class="panel-heading row">
        <div class="panel-title"><?=$this->title;?></div>

        <div class="hide">
            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">

                    <p>
                        <?= (Yii::$app->user->identity->role_id == User::ROLE_SADMIN) ? Html::a('Create User', ['create'], ['class' => 'btn btn-success']) : '' ?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions'=> ['class'=>'table '],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'username',
                            'email:email',
                            [
                                'attribute' => 'rank_id',
                                'value' => function($md){
                                    return $md->rank->name;
                                },
                                'filter' => TblRank::getList()
                            ],
                            [
                                'attribute' => 'role_id',
                                'value' => function($md){
                                    return $md->role->name;
                                },
                                'filter' => TblRoles::getList()
                            ],
                            [
                                'attribute' => 'status',
                                'value' => function($md){
                                    return $md->status == 1 ? "Active" : "Inactive";
                                },
                                'filter' => array(
                                    1 => "Active",
                                    0 => "Inactive",
                                )
                            ],
                            [
                                'attribute' => 'country_id',
                                'value' => function($md){
                                    return $md->tblCountry->name;
                                },
                                'filter' => TblCountry::getList()
                            ],
                            // 'role_id',
                            // 'fullname',
                            // 'address',
                            // 'city',
                            // 'country',
                            // 'parent_id',
                            // 'birthday',
                            // 'gender',
                            // 'status',
                            // 'updated_at',
                            // 'updated_by',


                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update}'
                            ],
                        ],
                    ]); ?>
            </div>
        </div>
    </div>
</div>
<!--
<div class="content-box-large">
    <div class="panel-heading row">
        <div class="panel-title"><?=$this->title;?></div>

        <div class="hide">
            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div class="navbar">
            <div class="navbar-inner">
                <div class="content">
                    <p>
                        <?= Html::a('Create Admin', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>                    
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#tab1" data-toggle="tab">Payment</a></li>
                        <li><a href="#tab2" data-toggle="tab">Order</a></li>
                        <li><a href="#tab3" data-toggle="tab">Bonus</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">

                <table class="table ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name 1</th>
                            <th>Last Name 1</th>
                            <th>Username 1</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab2">

                <table class="table ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name 2</th>
                            <th>Last Name 2</th>
                            <th>Username 2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="tab-pane" id="tab3">

                <table class="table ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name 3</th>
                            <th>Last Name 3</th>
                            <th>Username 3</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>-->