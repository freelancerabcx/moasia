<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblConfig */

$this->title = 'Create Tbl Config';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-config-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
