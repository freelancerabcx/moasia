<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblConfig */

$this->title = 'update config';
$this->params['breadcrumbs'][] = ['label' => 'Configs', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-config-update">
	<div class="content-box-large">
	    <div class="panel-heading row">
	        <div class="panel-title"><?=$this->title;?></div>
	    </div>
	    <div class="panel-body">
	        <div class="tab-content">
	            <div class="tab-pane active" id="tab1">

				    <?= $this->render('_form', [
				        'model' => $model,
				    ]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
