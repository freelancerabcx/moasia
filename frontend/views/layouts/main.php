<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
    <?php $this->beginPage() ?>
        <!DOCTYPE html>
        <html lang=en>

        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="/css/bootstrap.min.css">
            <meta charset="UTF-8" />
            <?= Html::csrfMetaTags() ?>
                <title>
                    <?= Html::encode($this->title) ?>
                </title>
                <?php $this->head() ?>

                    <!-- Optional theme -->
                    <link rel="stylesheet" href="/css/style.css">
                    <link rel="stylesheet" href="/css/font-awesome.min.css">

                    <!-- Latest compiled and minified JavaScript -->
                    <script src="/js/jquery.js"></script>
                    <script src="/js/jquery.dataTables.min.js"></script>
                    <!-- Latest compiled and minified JavaScript -->
                    <script src="/js/bootstrap.min.js"></script>
                    <script src="/js/Chart.bundle.min.js"></script>
                    <script src="/js/Chart.min.js"></script>
                    <script type="text/javascript">
                        var checkAction = false;
                        var countInt = 0;
                        setInterval(function () {
                            if (countInt >= 5 * 60) {
                                location.href = "<?=Url::to(['site/logout']);?>";
                            }
                            countInt++;
                        }, 1000);

                        $(window).keyup(function () {
                            countInt = 0;
                        })

                        $(window).mousemove(function () {
                            countInt = 0;
                        })

                        $(function () {
                            $("form").submit(function () {
                                $("input[type=submit]", this).attr("disabled", "disabled");
                            })
                        })
                    </script>
        </head>

        <body class="">
            <div id="">
                <?php $this->beginBody() ?>

                    <?php
    NavBar::begin([
        'brandLabel' => '<img src="/img/logo.png" alt="">',
        'brandUrl' => "/home/",
        'options' => [
            'class' => 'nav-moringga',
        ],
    ]);

    $menuItems = [];

    $menuItems = array_merge($menuItems, [
        ['label' => 'My wallets', 'url' => ['/site/index']],
        ['label' => 'Products', 'url' => ['/site/products']],
        ['label' => 'My network', 'url' => ['/site/network']],
        ['label' => 'My orders', 'url' => ['/site/orders']],
        ['label' => 'My placements', 'url' => ['/site/placements']],
        ['label' => 'Profile', 'url' => ['/site/profile']],
        ['label' => 'Sign Out', 'url' => ['/site/logout']],
    ]);


    if( isset(Yii::$app->user->identity->role_id) && Yii::$app->user->identity->role_id == 3){
        $menuItems[] = ['label' => 'Create Member', 'url' => ['/site/create-member'], 'options' => ['class' => 'button-hl']];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
                        <?php if(!Yii::$app->user->isGuest): ?>
                            <nav class="navbar nav-moringga navbar-menu">
                                <div class="container">
                                    <div class="navbar-header">
                                        <a href="/home/">
                                            <img src="/img/logo.png" alt="">
                                        </a>
                                        <span class="title-page"><?=$this->title;?></span>
                                    </div>
                                    <ul class="nav navbar-right">
                                        <li>
                                            <span>P.V records</span>
                                            <h3><?= $this->params['wallet']['total_pv_ordered'] ?></h3>
                                        </li>
                                        <li>
                                            <span>Bonus records</span>
                                            <h3><?= Yii::$app->user->identity->bonus_record ?></h3>
                                        </li>
                                        <li>
                                            <span>Bonus wallet</span>
                                            <h3><?= $this->params['wallet']['bonus']->amount ?></h3>
                                        </li>
                                        <li>
                                            <span>Product wallet</span>
                                            <h3><?= $this->params['wallet']['point']->amount ?></h3>
                                        </li>
                                        <li>
                                            <span>Register wallet</span>
                                            <h3><?= $this->params['wallet']['register']->amount ?></h3>
                                        </li>
                                        <li>
                                            <span><?= Yii::$app->user->identity->fullname ?></span>
                                            <h3><?= Yii::$app->user->identity->rank->name ?></h3>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <?php endif; ?>
                                <div class="container" style="padding-bottom:150px;">
                                    <?=$content;?>
                                </div>

                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>

                                <footer class="footer">
                                    <div class="container">
                                        <div class="copy text-center">
                                            Copyright 2016 <a href='/'>Moringna Asia</a> All rights reserved
                                        </div>
                                    </div>
                                </footer>
                                <script>
                                    $('.collapse-toggle').on('click', function () {
                                        $(this).toggleClass('updown');
                                        $(this).next('ul').toggleClass('hide');
                                    });
                                    $('.filter .btn').on('click', function () {
                                        var tg = $(this).data('target');
                                        var tbl = $(this).closest('.filter').next().find('tbody');
                                        $('.filter .btn').removeClass('active');
                                        $(this).addClass('active');
                                        tbl.find('tr').addClass('hide');
                                        tbl.find(tg).removeClass('hide');
                                    });

                                    $(".navbar-toggle").on('click', function (e) {
                                        if ($(".navbar-collapse").hasClass('in')) {
                                            $(".navbar-collapse").slideToggle();
                                            e.stopPropagation();
                                            return false;
                                        }
                                    });
                                </script>

                                <script>
                                        $('.set_datatable').DataTable({ "ordering": false});

                                </script>
                                <?php $this->endBody() ?>
        </body>

        </html>
        <?php $this->endPage() ?>