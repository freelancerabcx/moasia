<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'User Login';
$this->params['breadcrumbs'][] = $this->title;
?>
	
    <body class="login text-center container">
        <img class="logo" src="/img/logo.png">
        <h1 class="title"><?= Html::encode($this->title) ?></h1>
        <div class="line"></div>
        <div class="row">
            <div class="col-md-3 auto-margin no-float text-left">
                <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form']]); ?>
                    <input type="text" placeholder="USERNAME" name="LoginForm[username]">
                    <input type="password" placeholder="PASSWORD" name="LoginForm[password]">
                    <input type="checkbox" id="mcheck" name="LoginForm[rememberMe]"> <label class="upcase" for="mcheck">REMEber Me</label>
                    <input type="submit" value="go">
                <?php ActiveForm::end(); ?>
            </div>
            <div class="upcase tips">
                <?=$msg.($msg != "" ? "<br>" : "");?>
                could not login? <a class="link_brown" href="<?=Url::to(["site/request-password-reset"]);?>">reset your password</a>                
            </div>
        </div>
    </body>