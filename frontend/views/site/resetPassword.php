
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
    <body class="login text-center container">
        <img class="logo" src="/img/logo.png">
        <h1 class="title"><?= Html::encode($this->title) ?></h1>
        <div class="line"></div>
        <div class="row">
            <div class="col-md-3 auto-margin no-float text-left">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'options' => ['class' => 'form']]); ?>

                    <input type="password" placeholder="NEW PASSWORD" name="ResetPasswordForm[password]">
                    <input type="hidden" value="<?=!empty($_GET['u']) ? $_GET['u'] : '';?>" name="ResetPasswordForm[u]">
                    <input type="hidden" value="<?=!empty($_GET['token']) ? $_GET['token'] : '';?>" name="ResetPasswordForm[token]">
                    <input type="hidden" value="<?=!empty($_GET['t']) ? $_GET['t'] : '';?>" name="ResetPasswordForm[t]">
                    <input type="submit" value="go">
                <?php ActiveForm::end(); ?>
            </div>
            <div class="upcase tips"><?=$msg;?></div>
        </div>
    </body>