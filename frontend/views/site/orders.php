<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

$this->title = 'Orders';

?>
<br><br>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                        	'attribute' => 'created_at',
                        	'label' => 'Date',
                        	'value' => function($md){
                        		return DATE("d-M-Y H:i", $md->created_at);
                        	}
                        ],
                        [
                        	'attribute' => 'amount',
                        	'label' => 'USD',
                        	'value' => function($md){
                        		return "$".number_format($md->amount, 2, ',', ' ');
                        	}
                        ],
                        [
                        	'attribute' => 'con_amount',
                        	'label' => 'PV',
                        	'value' => function($md){
                        		return number_format($md->con_amount, 2, ',', ' ')." [PV]";
                        	}
                        ],
                        [
                            'attribute' => 'product_id',
                            'label' => 'Product',
                            'value' => function($md){
                                return $md->product->name;
                            }
                        ],
                        [
                            'attribute' => 'quantity',
                            'label' => "Received/Total",
                            'value' => function($md){
                                return $md->received_quantity.'/'.$md->quantity;
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{viewhistory}',
                            'buttons' => [
                                'viewhistory' =>  function ($url, $model) {     
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['received-history?id='.$model->id]), [
                                            'title' => Yii::t('yii', 'View Received History'),
                                    ]);                                
                                }
                            ]
                        ],
                        // 'is_delivery',
                        // 'created_at',
                    ],
                ]); ?>