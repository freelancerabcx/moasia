<?php
use yii\helpers\Html;
use common\models\TblBanks;
?>
<label class="control-label" for="user-bank_id">Bank</label>
<?=Html::dropDownList(
    'User[bank_id]', //name
    $currentbank_id, //selected
    $dt,
    ['onchange'=> 'showUser(this.value)', 'class'=>'form-control']
    )
  ?>
 <br>