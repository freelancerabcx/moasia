<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblUsers */

$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = ['label' => 'Update Profile', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-12">
				    <?= $this->render('_form', [
				        'model' => $model,
				    ]) ?>
		</div>
	</div>
</div>
