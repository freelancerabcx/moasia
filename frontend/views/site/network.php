<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

$this->title = 'My network';
/**
 * Ham nay tra lai tat cac cac Menu con
 */
function getSubcategory($id){
   $users = User::find()->where(['parent_id' => $id])->all();
    
    echo '<ul  class="hide">';
    foreach($users as $u){
        echo '<li><a href="javascript:void(0)" class="collapse-toggle" >'.$u->username.' ('.$u->fullname.')</a>';
        getSubcategory($u->id);
        echo "</li>";
    }
    echo '</ul>';
}
?>
    <h1>My network</h1>
    <br>
    <br>
    <div class="network-box">
        <ul>
            <li>
                <a href="javascript:void(0)" class="collapse-toggle"><?= Yii::$app->user->identity->username ?> (<?= Yii::$app->user->identity->fullname ?>)</a>

                <?php
            echo '<ul>';
        foreach($users as $u){
            echo '<li><a href="javascript:void(0)" class="collapse-toggle" >'.$u->username.' ('.$u->fullname.')</a>';
            getSubcategory($u->id);
            echo '</li>';
        }
        echo '</ul>';   
        ?>
            </li>
        </ul>
    </div>