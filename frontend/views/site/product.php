<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\TblRank;
use yii\helpers\Url;
use common\helpers\generalHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
        $total = Yii::$app->user->identity->tblWallets[0]->amount + Yii::$app->user->identity->tblWallets[1]->amount + Yii::$app->user->identity->tblWallets[2]->amount;

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>

        <br><br>
        <div class="row product">
            <div class="col-md-6">
                <?php foreach($md as $k => $v): ?>
                    <?php $cm = 0; 
                    $conm = 0;
                    $isShow = 0;
                    foreach($v->prices as $k2 => $v2){
                        if($v2->country_id == Yii::$app->user->identity->country_id){
                            $cm = $v2->price;
                            $conm = $v2->price_pv;
                            $isShow = 1;
                        }
                    } ?> 
                        <?php if($isShow == 1 && $v->category->status == 1): ?>
                        <div class="row item">
                            <div class="col-md-5">
                                <div class="img" style="background: url(<?=generalHelper::img('img/product/'.$v['id'].'.jpg');?>)"></div>
                            </div>
                            <div class="col-md-7">
                                <div class="info">
                                    <div class="name"><?=$v->name;?></div>
                                    <br>
                                    <!--<div class="description"><?=$v->description;?></div>-->
                                    <div class="price row">
                                        <div class="col-md-6">
                                            <span class="value">       
                                                <?=$cm;?>
                                                <span class="currency">USD</span>
                                            </span>
                                        </div>
                                        <div class="col-md-6">

                                            <span class="value">       
                                                <?=$conm;?>
                                                <span class="currency">PV</span>
                                            </span>                                        
                                        </div>
                                    </div>                                    <br>

                                    <div class="price row">
                                        <form action="<?=Url::to(['site/add-to-cart']);?>" method="POST">
                                            <div class="col-md-6">
                                                <input type="number" placeholder="Quantity" name="quantity" required min="0" max="99999" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                    <input type="hidden" name="name" value="<?=$v->name;?>">
                                                    <input type="hidden" name="id" value="<?=$v['id'];?>">
                                                    <input type="hidden" name="voucher_amount" value="<?=$v['voucher_amount'];?>">
                                                    <input type="hidden" name="price" value="<?=$cm;?>">
                                                    <input type="hidden" name="price_pv" value="<?=$conm;?>">
                                                    <input type="submit" class="form-control" value="ORDER"/>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-5">
                <div class="order">
                    <div class="title">
                        MY ORDER
                    </div>
                    <div class="line"></div>
                    <br>
                    <br>
                    <?php if(!isset($_SESSION['cart'])):?>
                        <div class="box error">NO ORDER</div>
                    <?php else:?>
                        <form action="<?=Url::to(['site/buy']);?>" method="POST">
                        <div class="list">
                            <?php 
                                $total = 0;
                                $total_sub = 0;
                                $total_quantity = 0;
                                $total_pv = 0;
                            ?>
                            <?php foreach($_SESSION['cart'] as $k => $v):?>
                                <?php 
                                    $total += $v['price'] * $v['quantity'];
                                    $total_sub += $v['price'];
                                    $total_quantity += $v['quantity'];
                                    $total_pv += $v['price_pv'] * $v['quantity'];
                                ?>
                                <ul class="row no-padding">
                                    <li class="col-md-7">
                                        <b><?=$v['name'];?></b><br>
                                        <div class="quantity">Quantity: <?=$v['quantity'];?></div>
                                    </li>
                                    <li class="col-md-5 price">
                                        <span class="value darkred-color"><?=$v['price'];?></span> <span class="currency">USD</span>
                                        <small class="pv"><?=$v['price_pv'];?> <span class="currency">PV</span></small>
                                    </li>

                                    <input type="hidden" required name="TblProductOrders[<?=$v['id'];?>][quantity]" value="<?=$v['quantity'];?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][user_id]" value="<?=Yii::$app->user->identity->id;?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][amount]" value="<?=$v['price_pv'];?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][voucher_amount]" value="<?=$v['voucher_amount'];?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][product_id]" value="<?=$v['id'];?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][is_delivery]" value="0">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][con_amount]" value="<?=$v['price'];?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][created_at]" value="<?=time();?>">
                                    <input type="hidden" name="TblProductOrders[<?=$v['id'];?>][country_id]" value="<?=Yii::$app->user->identity->country_id;?>">
                                </ul>
                            <?php endforeach; ?>
                        </div>
                        <hr>
                        <ul class="row no-padding">
                            <li class="col-md-4">
                                <b class="darkred-color">TOTAL</b><br>
                            </li>
                            <li class="col-md-8 price">
                                <span class="value darkred-color"><?=$total;?></span> <span class="currency">USD</span> <small class="pv"><?=$total_pv;?> <span class="currency">PV</span></small>
                            </li>
                        </ul>
                        <ul class="row no-padding">
                            <li class="col-md-7">
                                <a class="btn btn-primary btn-moasia" onclick="location.href='<?=Url::to(['site/clear-cart']);?>'">CLEAR CART</a>
                            </li>
                            <li class="col-md-5 price">
                                <input type="submit" class="btn btn-primary btn-moasia" value="PURCHASE">
                            </li>
                        </ul>
                        <div class="tip"><?=!empty($_GET['msg']) ? $_GET['msg'] : '';?></div>
                        </form>
                    <?php endif; ?>
                </div>
            </div>
        </div>