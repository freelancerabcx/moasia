<?php
    use yii\widgets\LinkPager;
    $this->title = 'My placements';
?>
<h1>My placements</h1>
<br>
<br>
<table class="table">
    <thead>
        <tr>
            <th>Username</th>
            <th>Rank</th>
            <th>Country</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($users as $key => $user):?>
            <tr>
                <td> <?= $user->username ?></td>
                <td> <?= $user->getRank()->one()->name ?></td>
                <td> <?= $user->getTblCountry()->one()->name ?></td>
                <td> <?= ($user->status)?'Active':'Inactive' ?></td>
            </tr>
            <?php endforeach;?>
    </tbody>
</table>
<?=  LinkPager::widget(['pagination' => $pages,]);?>