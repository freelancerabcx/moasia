<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TblUsers */

$this->title = 'Update Admin: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Admin', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="tbl-users-create">

	<div class="content-box-large">
	    <div class="panel-heading row">
	        <div class="panel-title"><?=$this->title;?></div>

	        <div class="panel-options">
	            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
	            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
	        </div>
	    </div>
	    <div class="panel-body">
	        <div class="tab-content">
	            <div class="tab-pane active" id="tab1">
				    <?= $this->render('_form', [
				        'model' => $model,
                        'wallet' => []
				    ]) ?>
				</div>
			</div>
		</div>
	</div>
</div>