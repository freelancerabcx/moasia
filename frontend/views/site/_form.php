<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use common\models\TblRoles;
use common\models\TblWallets;
use common\models\TblCurrency;
use common\models\TblCountry;
use common\models\TblBanks;

use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\TblUsers */
/* @var $form yii\widgets\ActiveForm */
$dropDownList = [
    0 => "Female",
    1 => "Male",
    3 => "Unknown"
];

$status = [
    1 => "Active",
    0 => "Block"
];

$tmp = TblRoles::getList();
unset($tmp['1']);
?>
                <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-5">
            <div class="tbl-users-form">


             <fieldset>
              <legend>User information:</legend>
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?php if($model->isNewRecord) echo $form->field($model, 'password')->passwordInput() ?>
                <?php if($model->isNewRecord) echo $form->field($model, 'password_repeat')->passwordInput() ?>

                <?php 
                    if(!$model->isNewRecord)
                        $model->birthday = Date('d-M-Y', $model->birthday);
                ?>

                <?= $form->field($model, 'birthday')->widget(
                    DatePicker::className(), [
                        'template' => '{addon}{input}',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-M-yyyy'
                        ]
                ]);?>


                <?= $form->field($model, 'fullname')->textInput(['maxlength' => true, 
                        'id' => 'fullname', 
                        'onkeyup' => "syncName(1)"
                ]) ?>

                <?= $form->field($model, 'identity')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'passport')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                <?php 
                    if($model->isNewRecord){
                                $url = \yii\helpers\Url::to(['site/get-parents?isBoth=true']);
                                $initScript =
<<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}&id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
                                $model->parent_id = Yii::$app->user->identity->id;
                                echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                                    'options' => [
                                        'class' => 'form-control',
                                        'placeholder' => 'Searching for parent user ...', 
                                        'multiple' => false
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 3,
                                        'ajax' => [
                                            'url' => $url,
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(term,page) { return {search:term}; }'),
                                            'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                                        ],
                                        'initSelection' => new JsExpression($initScript)
                                    ],
                                ]);
                    }
                                

                ?>


                <?= $form->field($model, 'gender')->dropDownList($dropDownList) ?>

                <?php if($model->isNewRecord): ?>
                <div class="form-group field-user-city">
                    <label class="control-label" for="user-city">Share money to this User</label>
                    <input type="number" id="user-city" min="0" max="<?=$wallet->amount;?>" class="form-control" name="User[share_amount]" value="0" maxlength="50">
                    Your current amount: <?=$wallet->amount;?>
                    <div class="help-block"></div>
                </div>
                <?php endif; ?>
                </fieldset>
                <?= $form->field($model, 'updated_by')->hiddenInput(['value' => 1])->label('') ?>
                <div style="display: none">
                    <?= $form->field($model, 'currency_id')->dropDownList(TblCurrency::getList()) ?>
                    <?php if($model->isNewRecord) echo $form->field($model, 'rank_id')->hiddenInput(['value' => 0])->label('') ?>
                    <?= $form->field($model, 'updated_at')->hiddenInput(['value' => time()])->label('') ?>
                    <?php if($model->isNewRecord) echo $form->field($model, 'role_id')->hiddenInput(['value' => 4]) ?>
                    <?= $form->field($model, 'status')->dropDownList($status) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            </div>
    </div>
    <div class="col-md-2"></div>
    <div class="col-md-5">
            <fieldset>
                  <legend>Address information:</legend>
                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'country_id')->dropDownList(TblCountry::getList(), [
                        "onchange" => "getBank(this.value)"
                    ])->label('Country') ?>
                </fieldset>
                <br><br>
                <fieldset>
                    <legend>Bank information:</legend>
                    <div id="cnt"></div>
                    <?= $form->field($model, 'bank_account')->textInput(['maxlength' => true]) ?>
                    <div class="form-group field-user-city">
                        <label class="control-label" for="user-city">Full Name</label>
                        <input type="text" id="fullname2" class="form-control" onkeyup="syncName(2)" value="<?=$model->fullname;?>">
                        <div class="help-block"></div>
                    </div>
                </fieldset>
    </div>
</div>
                <?php ActiveForm::end(); ?>
<script type="text/javascript">
    function getBank(country_id)
    {
        $("#cnt").load("<?=Url::to(["/site/get-bank?country_id="]);?>"+country_id+"&currentbank_id=<?=$model->bank_id;?>");
    }

    window.addEventListener("load", function(){
        getBank($("#user-country_id").val());
    });
    function syncName(t){
        console.log($("#fullname2").val());
        if(t == 2)
            $("#fullname").val($("#fullname2").val());
        else
            $("#fullname2").val($("#fullname").val());
    }
</script>