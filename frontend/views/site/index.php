<?php
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = "My wallets";
?>
<div class="wallet-header">
    <div class="container">
        <ul class="nav nav-tabs  row">
            <li class="active col-xs-4">
                <a data-toggle="tab" href="#home">
                    <h3 class="wallet_title">Bonus Wallet</h3>
                    <span class="sub_title">Current total</span>
                    <h2 class="wallet_num" data-currency="USD"><?= $this->params['wallet']['bonus']->amount ?></h2>
                </a>
            </li>
            <li class="col-xs-4">
                <a data-toggle="tab" href="#menu1">
                    <h3 class="wallet_title">Product Wallet</h3>
                    <span class="sub_title">Current total</span>
                    <h2 class="wallet_num" data-currency="USD"><?= $this->params['wallet']['point']->amount ?></h2>
                </a>
            </li>
            <li class="col-xs-4">
                <a data-toggle="tab" href="#menu2">
                    <h3 class="wallet_title">Register Wallet</h3>
                    <span class="sub_title">Current total</span>
                    <h2 class="wallet_num" data-currency="USD"><?= $this->params['wallet']['register']->amount ?></h2>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="tab-content tab-absolute">
    <div id="home" class="tab-pane fade in active">
        <div class="container box-chart">
            <div class="row point" onclick="toggleLine()">
                <div class="col-md-10 text-uppercase"><b>You earned <span class='text-danger'>$<?=$data['this_week_bonus']['amount'];?></span> this week - from <?=$data['this_week_bonus']['start'];?> <span class='text-lowercase'>to</span> <?=$data['this_week_bonus']['end'];?></b></div>
                <div class="col-md-2 text-right" id="minimizeBtn">View All History &nbsp; <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span></div>
            </div>
            <div class="margin-20-top line" style="display: none">
                <svg id="loading" width="38" height="38" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#0D62A4">
                    <g fill="none" fill-rule="evenodd">
                        <g transform="translate(1 1)" stroke-width="2">
                            <circle stroke-opacity=".5" cx="18" cy="18" r="18"/>
                            <path d="M36 18c0-9.94-8.06-18-18-18">
                                <animateTransform
                                    attributeName="transform"
                                    type="rotate"
                                    from="0 18 18"
                                    to="360 18 18"
                                    dur="1s"
                                    repeatCount="indefinite"/>
                            </path>
                        </g>
                    </g>
                </svg>
                <div class="margin-20-top">
                    <b class="text-uppercase nav-button" id='lastBtn' onmouseover='hoverWeek(false)' onclick="moveWeek(false)">Last 10 weeks</b>
                    <b class="pull-right text-uppercase nav-button" id='nextBtn' onmouseover='hoverWeek(true)' onclick="moveWeek(true)">Next 10 weeks</b>
                </div>
                <div class="row margin-20-top">
                    <div class="col-md-2"></div>
                    <div class="col-md-8" id="chartzone">
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            
        </div>
        <br>
        <div class="btn-group filter clearfix" style="display:block;">
            <a href="javascript:void(0)" class="btn btn-default active" data-target="tr">All</a>
            <a href="javascript:void(0)" class="btn btn-default" data-target=".uncalculated">Uncalculated</a>
            <a href="javascript:void(0)" class="btn btn-default" data-target=".calculated">Calculated</a>
        </div>
        <form action="<?=Url::to(['site/convert-bonus']);?>" method="POST" class="row">
            <div class="col-md-3">
                <label><br></label>
                <input name="amount" placeholder="Amount"  class="form-control" type="number" min="1" required max="<?= $this->params['wallet']['bonus']->amount ?>">
            </div>
            <div class="col-md-3">
                <label><br></label><input type="submit" class="form-control" value="Convert to Product Point">
            </div>
        </form>
        <?= $this->render('@frontend/views/shared/table',['data'=>$data['history_bonus']]) ?>
    </div>
    <div id="menu1" class="tab-pane fade">
        <div class="btn-group filter">
            <a href="javascript:void(0)" class="btn btn-default active" data-target="tr">All</a>
            <a href="javascript:void(0)" class="btn btn-default" data-target=".uncalculated">Uncalculated</a>
            <a href="javascript:void(0)" class="btn btn-default" data-target=".calculated">Calculated</a>
        </div>
        <?= $this->render('@frontend/views/shared/table',['data'=>$data['history_ppoint']]) ?>
    </div>
    <div id="menu2" class="tab-pane fade">
        <div class="btn-group filter clearfix" style="display:block;">
            <a href="javascript:void(0)" class="btn btn-default active" data-target="tr">All</a>
            <a href="javascript:void(0)" class="btn btn-default" data-target=".uncalculated">Uncalculated</a>
            <a href="javascript:void(0)" class="btn btn-default" data-target=".calculated">Calculated</a>
        </div>
            <form action="<?=Url::to(['site/convert']);?>" method="POST" class="row">
                <div class="col-md-3">
                    <label><br></label>
                    <input name="amount" placeholder="Amount"  class="form-control" type="number" min="1" required max="<?= $this->params['wallet']['register']->amount ?>">
                </div>
                <div class="col-md-3">
                    <label><br></label>
                    <input type="submit" class="form-control" value="Convert to Product Point">
                    </div>
            </form>

            <br>
            <div class="row">
                <?php $form = ActiveForm::begin(); ?>
                    <div class="col-md-3">
                      <?php 
                                if($model->isNewRecord){
                                            $url = \yii\helpers\Url::to(['site/get-parents?isBoth=true']);
                                            $initScript =
<<< SCRIPT
function (element, callback) {
    var id=\$(element).val();
    if (id !== "") {
        \$.ajax("{$url}?id=" + id, {
            dataType: "json"
        }).done(function(data) { callback(data.results);});
    }
}
SCRIPT;
                                            echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                                                'options' => [
                                                    'class' => 'form-control',
                                                    'required' => true,
                                                    'placeholder' => 'Searching for user ...', 
                                                    'multiple' => false
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'minimumInputLength' => 3,
                                                    'ajax' => [
                                                        'url' => $url,
                                                        'dataType' => 'json',
                                                        'data' => new JsExpression('function(term,page) { return {search:term}; }'),
                                                        'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                                                    ],
                                                    'initSelection' => new JsExpression($initScript)
                                                ],
                                            ])->label("Share money to:");
                                }
                                            

                            ?>
                    </div>
                <div class="col-md-3">
                    <label><br></label>
                    <input name="User[amount]" placeholder="Amount" class="form-control" type="number" min="1" required max="<?= $this->params['wallet']['register']->amount ?>">
                </div>
                <div class="col-md-2">
                    <label><br></label><input type="submit" class="form-control" value="Share Now">
                </div>
        <?php ActiveForm::end(); ?>
        </div>
        <?= $this->render('@frontend/views/shared/table',['data'=>$data['history_regis']]) ?>
    </div>
</div>
<style type="text/css">
    .box-chart {
        border: solid 1px #F9E9A9;
        padding: 20px;
        background: #FFF2C8;
        width: 100%;
    }

    .box-chart .margin-20-top{
        margin-top: 20px;
    }

    .box-chart .line{
        border-top: solid 1px #F9E9A9;
    }

    .box-chart .nav-button {
        color: #EA9446;
        cursor: pointer;
    }

    .box-chart .point {
        cursor: pointer;
    }

    .box-chart #loading {
        position: absolute;
        left: 20px;
        margin-top: 50px;
        width: 20px;
    }
</style>

<script type="text/javascript">
    var currentD = 0;
    var doneRequest = true;
    var allowPre = false;
    $(function(){
        getDataChart(currentD);
    })

    function hoverWeek(isNext){
        $("#nextBtn").css({'cursor': 'pointer'});
        $("#lastBtn").css({'cursor': 'pointer'});

        if(isNext && currentD >= 0)
            $("#nextBtn").css({'cursor': 'no-drop'});

        if(!isNext && !allowPre)
            $("#lastBtn").css({'cursor': 'no-drop'});

    }

    function moveWeek(isNext){
        if(doneRequest == false)
            return;

        if(isNext){
            if(currentD >= 0)
                return;
            currentD++;
        }
        else{
            if(!allowPre)
                return;
            
            currentD--;
        }

        getDataChart(currentD);
    }

    function getDataChart(day){
        doneRequest = false;
        $("#loading").show();
        $.ajax({
            method: "GET",
            url: "<?=Url::to('site/get-data-chart?d=');?>"+day,
            success: function(dt){
                var dt = JSON.parse(dt);                
                var tmp = {
                    labels: dt.labels,
                    datasets: [{
                        label: 'USD',
                        data: dt.data,
                        backgroundColor: '#0D62A4',
                        borderColor: null,
                        borderWidth: 1
                    }]
                }
                drawChart(tmp);
                doneRequest = true;
                allowPre = dt.allowPre;
                $("#loading").hide();
            }
        });
    }

    function drawChart(dt){

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
            $("#chartzone").html('<canvas id="myChart" width="400" height="400"></canvas>');
            $("#minimizeBtn").hide();
        }
        else
            $("#chartzone").html('<canvas id="myChart"></canvas>');

        var ctx = document.getElementById("myChart");
        var chartInstance = new Chart(ctx, {
            type: 'horizontalBar',
            data: dt,
            options: {
                title: {
                    display: true,
                    fontSize: 14,
                    fontColor: '#a94442',
                    text: 'WEEKLY BONUS'
                }
            },
            legend: {
                display: true,
                maintainAspectRatio: true,
                responsive: true,
                labels: {
                    fontColor: 'blue'
                }
            }
        });

    }

    function toggleLine(){
        if($('.line').css('display') != 'block')
            $("#minimizeBtn").html('Minimize &nbsp; <span><i class="fa fa-chevron-up" aria-hidden="true"></i></span>');
        else
            $("#minimizeBtn").html('View All History &nbsp; <span><i class="fa fa-chevron-down" aria-hidden="true"></i></span>');
        $('.line').slideToggle();
    }

</script>