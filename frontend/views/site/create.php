<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TblUsers */

$this->title = 'Create Member';
$this->params['breadcrumbs'][] = ['label' => 'Create Admin', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-12">
				    <?= $this->render('_form', [
				        'model' => $model,
                        'wallet' => $wl
				    ]) ?>
		</div>
	</div>
</div>
