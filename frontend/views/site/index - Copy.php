<?php
    use common\helpers\generalHelper;
    use common\helpers\processHelper
?>
bonus: <?= $data['bonus']->amount ?> <br>
point: <?= $data['point']->amount ?> <br>
register: <?= $data['register']->amount ?> <br>

<br>
<form action="?r=site/convert" method="POST"><input name="amount" type="number" min="1" required max="<?= $data['register']->amount ?>"> from Register to Point <input type="submit" value="Convert">
<br>
<br>

history: <br>
<table class="table">
    <thead>
        <tr>
            <th>id</th>
            <th>amount</th>
            <th>type</th>
            <th>created at</th>
        </tr>
    </thead>
    <tbody>
       <?php foreach($data['hisory'] as $key => $his):?>
        <tr>
            <td><?= $key ?></td>
            <td><?= $his->amount ?></td>
            <td>
                <?php
                switch($his->type){
                    case 1:
                        echo 'Direct sponsor bonus';
                        break;
                    case 2:
                        echo 'Overiding';
                        break;
                    case 3:
                        echo 'Daily bonus';
                        break;
                };
                ?>
            </td>
            <td><?= date('H:m:s d-m-Y') ?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>