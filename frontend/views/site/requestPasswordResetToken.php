<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
    <body class="login text-center container">
        <img class="logo" src="/img/logo.png">
        <h1 class="title"><?= Html::encode($this->title) ?></h1>
        <div class="line"></div>
        <div class="row">
            <div class="col-md-3 auto-margin no-float text-left">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'options' => ['class' => 'form']]); ?>

                    <input type="text" placeholder="USERNAME" name="PasswordResetRequestForm[username]">
                    <input type="submit" value="go">
                <?php ActiveForm::end(); ?>
            </div>
            <div class="upcase tips"><?=$msg;?></div>
        </div>
    </body>
