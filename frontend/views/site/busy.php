<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'User Login';
$this->params['breadcrumbs'][] = $this->title;
?>

    <body class="login text-center container">
       <br><br><br>
        <img class="logo" src="/img/logo.png">
        <h1 class="title">system maintenance</h1>
        <p>Please come back tomorrow at 1:00 am</p>
    </body>