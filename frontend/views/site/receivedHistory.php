<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TblProductOrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Received History';
$this->params['breadcrumbs'][] = $this->title;
?>
<br><br>
<div class="summary"><?='Order ID: '.$order_id;?></div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'product_id',
                            'value' => function($md){
                                return $md->product->id." - ".$md->product->name;
                            }
                        ],
                        'order_id',
                        [
                            'attribute' => 'quantity',
                            'label' => "Received/Total",
                            'value' => function($md){
                                return $md->quantity.'/'.$md->total_ordered;
                            }
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => 'Time',
                            'value' => function($md){
                                return DATE('d-M-Y H:i:s', $md->created_at);
                            }
                        ],
                    ],
                ]); ?>