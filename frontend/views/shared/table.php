<table class="table set_datatable">
    <thead>
        <tr>
            <th>Date</th>
            <th>From User</th>
            <th>Type</th>
            <th>Status</th>
            <th>Commission</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($data as $key => $his):?>
            <tr class="<?php 
                                switch($his->payment_status){
                                    case 0:
                                        echo " uncalculated ";
                                        break;
                                    case 1:
                                        echo "uncalculated ";
                                    break;
                                    case 2:
                                        echo "calculated ";
                                    break;
                                }
                            ?>">
                <td>
                    <?= date('d-m-Y',$his->created_at) ?>
                </td>
                <td>
                    <?= $his->getFromUser()->one()->username." | ".$his->getFromUser()->one()->fullname; ?>
                </td>
                <td>
                    <?php
                                switch($his->type){
                                    case 1:
                                        echo 'Direct sponsor bonus';
                                        break;
                                    case 2:
                                        echo 'Overiding';
                                        break;
                                    case 3:
                                        echo 'Daily bonus';
                                        break;
                                    case 6:
                                        echo 'Mounthy bonus';
                                        break;
                                    case 4:
                                        echo 'Share Register to '.$his->getUser()->one()->username." | ".$his->getUser()->one()->fullname;
                                        break;
                                    case 5:
                                        echo 'Receive Register from '.$his->getUser()->one()->username." | ".$his->getUser()->one()->fullname;
                                        break;
                                };
                                ?>
                </td>
                <td>
                    <?php 
                                switch($his->payment_status){
                                    case 0:
                                        echo "Uncalculated";
                                    break;
                                    case 1:
                                        echo "Proccessing";
                                    break;
                                    case 2:
                                        echo "Calculated into payment";
                                    break;
                                }
                            ?>
                </td>
                <td>
                    <?= $his->amount ?> <span class="money-tag">USD</span>
                </td>
            </tr>
            <?php endforeach;?>
    </tbody>
</table>