<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\PasswordResetRequestForm;
use common\models\ResetPasswordForm;
use common\models\LoginForm;
use common\models\User;
use common\models\TblUserSearch;
use common\models\TblPaidTransaction;
use common\models\TblConfig;
use common\models\TblWallets;
use common\models\TblProducts;
use common\models\TblProductOrders;
use common\models\TblHistoryBonus;
use common\models\TblProductOrdersSearch;
use common\models\TblBanks;
use common\models\TblProductReceivedLogs;
use common\models\TblProductReceivedLogsSearch;
use yii\helpers\Json;
use yii\db\Query;
use yii\helpers\Url;
use common\helpers\generalHelper;
use common\helpers\processHelper;
use yii\data\Pagination;
/**
 * Site controller
 */
class SiteController extends Controller
{
    public $wallet;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login','maintenance', 'rebuildlv', 'error', 'request-password-reset', 'reset-password', 'calpvrecord'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'get-bank', 'orders', 'wallet', 'profile', 'get-parents', 'add-to-cart', 'clear-cart', 'create-member','placements', 'members', 'buy', 'products', 'convert', 'convert-bonus', 'received-history','network', 'get-data-chart'],

                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (in_array(Yii::$app->user->identity->role_id, User::getRoleID(false))) {
                                return true;
                            }
                            return false;
                        }
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        if(date('H') > 22 || date('H') <1){
            if( $_SERVER['REQUEST_URI'] != '/site/maintenance')
                $this->redirect('/site/maintenance');
        }else{
            if( $_SERVER['REQUEST_URI'] == '/site/maintenance')
                $this->redirect('/site/login');
        };
        if(Yii::$app->user->identity != null){
            $user_id = Yii::$app->user->identity->id;
            $data['bonus'] = TblWallets::find()->where(['user_id' => $user_id])->andWhere([ 'type'=>TblWallets::TYPE_BONUS ])->one();
            $data['point'] = TblWallets::find()->where(['user_id' => $user_id])->andWhere([ 'type'=>TblWallets::TYPE_POINT ])->one();
            $data['register'] = TblWallets::find()->where(['user_id' => $user_id])->andWhere([ 'type'=>TblWallets::TYPE_RESGI ])->one();
            /*
            $pv_record = User::find()
                    ->select([
                        'tbl_users.*',
                        'SUM(con_amount * quantity) AS total'
                    ])->where(['=','tbl_users.parent_id',$user_id])
                    ->andWhere(['>=','tbl_users.created_at', time()-86400*30])
                    ->leftJoin('tbl_product_orders','`tbl_product_orders`.`user_id` = `tbl_users`.`id`')
                    ->groupBy('`tbl_users`.`parent_id`')
                    ->one();*/
            //$data['total_pv_ordered'] = $pv_record ? $pv_record->total :0;
            $data['total_pv_ordered'] = User::find()->select("pv_record")->where("id = ".$user_id)->scalar();//is_numeric($data['total_pv_ordered']) ? number_format($data['total_pv_ordered'], 2, ".", "") : 0;
            $this->view->params['wallet'] = $data;
        }
        $this->enableCsrfValidation = false;
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private $tmpRecuisiveRevert2 = [];
    function recuisiveRevert2($xparent, $fix){
        $u = User::find()->where("parent_id = ".$xparent->id."  AND status = ".User::STATUS_ACTIVE."  AND role_id IN (".implode(",", User::getRoleID(false)).")")->all();
        foreach ($u as $k => $v) {
            if(!empty($v->id)){
                if($fix){
                    $u = User::find()->where("id = ".$v->id)->one();
                    $u->level = empty($xparent->level) ? 0 : $xparent->level + 1;
                    $u->save();
                    echo $u->id." Updated!<br>";
                    $this->recuisiveRevert2($v, $fix);
                }else{
                        echo "<ul><li>";
                        echo "<pre>".$v->id." - ".$v->username." - ".$v->parent_id." - ".$v->level."</pre>";
                        
                        echo "<ul><li>";
                            $this->recuisiveRevert2($v, $fix);
                        echo "</li></ul>";
                        echo "</li></ul>";
                }
            }
        }
    }

    public function actionRebuildlv($fix = false){
        $uadmin = User::find()->where("role_id = 1")->one();
        $users = User::find()->select("id, username, parent_id, level")->where('role_id IN (3, 4) AND (parent_id IS null OR parent_id = '.$uadmin->id.")")->all();

        foreach ($users as $k => $v) {
            $this->recuisiveRevert2($v, $fix);
        }

        die;
    }
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //echo generalHelper::fnEncrypt("zxcvbn08121994");die;
        $user_id = Yii::$app->user->identity->id;
        $data['history_bonus'] = TblHistoryBonus::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['wallet' => TblWallets::TYPE_BONUS])->orderBy(['created_at'=>SORT_DESC])->all();
        $data['history_ppoint'] = TblHistoryBonus::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['wallet' => TblWallets::TYPE_POINT])->orderBy(['created_at'=>SORT_DESC])->all();
         $data['history_regis'] = TblHistoryBonus::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['wallet' => TblWallets::TYPE_RESGI])->orderBy(['created_at'=>SORT_DESC])->all();

        $model = new User;
        $post = $_POST;
        if(!empty($post['User'])){
            $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_RESGI])->one();
            $wl->amount -= $post['User']['amount'];
            $wl->save();

            $wl = TblWallets::find()->where(['user_id' => $post['User']['parent_id'], 'type' => TblWallets::TYPE_RESGI])->one();
            $wl->amount += $post['User']['amount'];
            $wl->save();

            if($post['User']['amount'] > 0){
                processHelper::addHistory($post['User']['parent_id'], Yii::$app->user->identity->id, $post['User']['amount'],TblHistoryBonus::TYPE_SHA, TblWallets::TYPE_RESGI);
                processHelper::addHistory(Yii::$app->user->identity->id, $post['User']['parent_id'], $post['User']['amount'],TblHistoryBonus::TYPE_REG, TblWallets::TYPE_RESGI);
            }

            $this->redirect(['site/index']);
        }


        $currentW = strtotime("last saturday");
        $saturday = $currentW;
        $firday = $currentW + 7 * 86400 - 1;
        $start = date("d M", $saturday);
        $end = date("d M", $firday);

        $totalBonusWallet = TblHistoryBonus::find()->select('SUM(amount)')->where("user_id = ".$user_id." AND wallet = ".TblWallets::TYPE_BONUS." AND created_at >= ".$saturday." AND created_at <= ".$firday)->scalar();
        $data['this_week_bonus'] = [
            "start" => $start,
            "end" => $end,
            "amount" => $totalBonusWallet != null ? $totalBonusWallet : 0
        ];
        

        return $this->render('index',[
            'data' => $data,
            'model' => $model
        ]);
    }

    public function actionNetwork(){
        return $this->render('network',[
            'users' => User::find()->where(['parent_id' => Yii::$app->user->identity->id, "status" => User::STATUS_ACTIVE])->all(),
        ]);
    }
    public function actionPlacements(){
        if(count(User::getRoleID(false)) <= 0)
            die("Wrong roles!");
        $query = User::find()->where("status = ".User::STATUS_ACTIVE." AND role_id IN (".implode(",", User::getRoleID(false)).")")->orderBy('id DESC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();

        return $this->render('placements', [
             'users' => $models,
             'pages' => $pages,
        ]);
    }
    public function actionProducts(){
        $md = TblProducts::find()->where(['=','status','1'])->all();
        return $this->render('product', [
            "md" => $md
        ]);
    }

    public function actionOrders(){
        $searchModel = new TblProductOrdersSearch();
        $searchModel->user_id = Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all TblProductOrders models.
     * @return mixed
     */
    public function actionReceivedHistory($id)
    {
        $searchModel = new TblProductReceivedLogsSearch();
        $params = Yii::$app->request->queryParams;
        $params['TblProductReceivedLogsSearch']['order_id'] = $id;
        $dataProvider = $searchModel->search($params);

        $order = TblProductOrders::find()->where(['id'=>$id])->one();
        if(empty($order))
            die("This order is not found");

        if($order->user_id != Yii::$app->user->identity->id)
            die('Permission dined!');

        return $this->render('receivedHistory', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'order_id' => $id,
        ]);
    }

    public function actionBuy(){
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction(); //get parent user
        try{
            $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_POINT])->one();

            $total_con_amount = 0;
            $total_amount = 0;
            foreach ($_POST['TblProductOrders'] as $k => $v) {
                $total_con_amount += $v['amount'] * $v['quantity'];//PV
                $total_amount += $v['con_amount'] * $v['quantity'];//USD
            }


            if($total_amount > $wl->amount)
                return $this->redirect(["products?msg=NOT ENOUGH MONEY! Charging to Product Wallet plz!"]);

            foreach ($_POST['TblProductOrders'] as $k => $v) {
                //$v['amount'] = $v['amount'] * $v['quantity'];
                //$v['con_amount'] = $v['con_amount'] * $v['quantity'];
                //change knowlege about amount and con_amount. amount = USD, con_amount = PV. Before: amount = PV, con_amount = USD
                $tmp = $v['amount'];
                $v['amount'] = $v['con_amount'];
                $v['con_amount'] = $tmp;

                //if(Yii::$app->user->identity->tblWallets[0]->amount)
                //minus money from wallet
                $tm = array(
                    "TblProductOrders" => $v
                );
                $md = new TblProductOrders;
                $md->load($tm);
                $md->save();
            }

            $wl->amount -= $total_amount;
            $wl->save();
            processHelper::updateRankByUser(Yii::$app->user->identity->id,$total_con_amount);
            processHelper::directSponsorBonusByUser(Yii::$app->user->identity->id,$total_con_amount);
            processHelper::overridingBonusByUser(Yii::$app->user->identity->id,$total_con_amount);
            //Change free member to member
            $user = User::find()->where(["id" => Yii::$app->user->identity->id])->one();
            if(Yii::$app->user->identity->role_id == User::ROLE_FREE_MEMBER){
                $user->role_id = 3;
                if($user->firsttime_is_member == null || $user->firsttime_is_member == 0)
                    $user->firsttime_is_member = time();
                $user->lasttime_is_member = time();
                $user->save();
            }

            //send PV to parent User in 30 days, following by firsttime_is_member
            if($user->firsttime_is_member >= time() - 86400 * 30){
                $parentUser = User::find()->where("id = " . $user->parent_id)->one();
                if(!empty($parentUser)){
                    $parentUser->pv_record += $total_con_amount;
                    $parentUser->save();
                }
            }

        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        $transaction->commit();
        return $this->redirect(['clear-cart']);
    }

    //convert Register to Product Point
    public function actionConvert(){
        $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_RESGI])->one();
        if(isset($_POST) && $_POST['amount'] > 0 && $_POST['amount'] <= $wl->amount)
        {
            $wl->amount -= $_POST['amount'];
            if($wl->amount <= 1)
                die("Not enough money!");
            $wl->save();

            $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_POINT])->one();
            $wl->amount += number_format($_POST['amount'] / (1 / Yii::$app->user->identity->tblCountry->pv_rate), 2, '.', '');
            $wl->save();   
        }

        $this->redirect(['index']);
    }

    //convert Bonus to Product Point
    public function actionConvertBonus(){
        $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_BONUS])->one();
        if(isset($_POST) && $_POST['amount'] > 0 && $_POST['amount'] <= $wl->amount)
        {
            $wl->amount -= $_POST['amount'];
            if($wl->amount <= 1)
                die("Not enough money!");
            $wl->save();

            $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_POINT])->one();
            $wl->amount += number_format($_POST['amount'] / (1 / Yii::$app->user->identity->tblCountry->pv_rate), 2, '.', '');
            $wl->save();   

        }
        $this->redirect(['index']);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionWallet()
    {
        return $this->render('wallet',[
            'users' => User::find()->leftJoin('tbl_wallets','`tbl_wallets`.`user_id` = `tbl_users`.`id`')->all()
        ]);
    }



    private $tm = [];
    function recursive($id){
        $x = User::find()->where('id = '.$id)->one();
        if($x){
            $this->tm[] = $x->parent_id;
            if($x->parent_id >0)
                $this->recursive($x->parent_id);    
        }
    }


    /**
     * Creates a new TblUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateMember()
    {

        $wl = TblWallets::find()->where(['user_id' => Yii::$app->user->identity->id, 'type' => TblWallets::TYPE_RESGI])->one();

        if (Yii::$app->user->identity->role_id != User::ROLE_MEMBER) {
            return $this->goHome();
        }

        $model = new User();

        $model->setScenario('isRequired');
        $model->setScenario('hasToCheck');
        $post = $_POST;
        if(!empty($_POST)){
            if($post['User']['share_amount'] > 0 && $wl->amount < $post['User']['share_amount'])
                die("NOT ENOUGH MONEY");
            $post['User']['password'] = generalHelper::fnEncrypt($post['User']['password']);
            $post['User']['password_repeat'] = generalHelper::fnEncrypt($post['User']['password_repeat']);
            $post['User']['level'] = 0;
            if(!empty($post['User']['parent_id'])){
                $parent = User::find()->where("id = ".$post['User']['parent_id'])->one();
                if(!empty($parent->level))
                    $post['User']['level'] = $parent->level + 1;
            }
        }

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction(); //get parent user
        try{
            if ($model->load($post) && $model->save()) {
                $obj = [
                    "TblWallets" => [
                        "user_id" => $model->id,
                        "amount" => 0,
                        "type" => 0,
                        "updated_at" => time()
                    ]
                ];
                
                $wl->amount -= $post['User']['share_amount'];
                $wl->save();
                if($post['User']['share_amount'] > 0){
                    processHelper::addHistory($model->id, $wl->user_id, $post['User']['share_amount'], TblHistoryBonus::TYPE_SHA, TblWallets::TYPE_RESGI);
                    processHelper::addHistory($wl->user_id, $model->id, $post['User']['share_amount'], TblHistoryBonus::TYPE_REG, TblWallets::TYPE_RESGI);
                }

                $md = new TblWallets;
                $md->load($obj);
                $md->amount = $post['User']['share_amount'];
                $md->save($obj);

                $obj["TblWallets"]["type"] = 1;
                $md = new TblWallets;
                $md->load($obj);
                $md->save($obj);

                $obj["TblWallets"]["type"] = 2;
                $md = new TblWallets;
                $md->load($obj);
                $md->save($obj);

                
                //$this->recursive($model->id);
                //$ids = implode(",", $this->tm);
                //Yii::$app->db->createCommand('UPDATE tbl_users SET level = level + 1 WHERE id IN ('.$ids.')')->execute();

                $transaction->commit();
                return $this->redirect(['network']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    "wl" => $wl
                ]);
            }
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }


    public function actionProfile()
    {
        $model = new User();
        $model = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile']);
        } else {
            return $this->render('profile', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Lists all TblUsers models.
     * @return mixed
     */
    public function actionMembers()
    {
        $searchModel = new TblUserSearch();
        $dataProvider = $searchModel->search(["parent_id" => Yii::$app->user->identity->id]);

        return $this->render('member', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionMaintenance()
    {//echo generalHelper::fnEncrypt("123456");die;
        $this->layout="empty";
        return $this->render('busy');
    }
    public function actionLogin()
    {//echo generalHelper::fnEncrypt("123456");die;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout="empty";
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->memLogin()) {
            return $this->goHome();
        } else {
            $msg = "";
            if(!empty($_POST)){
                $msg = "Wrong username or password!";
            }
            return $this->render('login', [
                'model' => $model,
                'msg' => $msg
            ]);
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = "empty";
        $model = new PasswordResetRequestForm;
        $msg = "";
        if (!empty($_POST) && !empty($_POST['PasswordResetRequestForm']['username'])) {
                $user = User::find()->where(["username" => $_POST['PasswordResetRequestForm']['username']])->one();
                if($user == null || ($user != null && in_array($user->role_id, User::getRoleID(true))))
                    $msg = "Can not find this User!";
                else{
                    $time = time();
                    $token = md5($time."moasia".$_POST['PasswordResetRequestForm']['username']);
                    $str = "http://".$_SERVER['SERVER_NAME'].Url::to(['site/reset-password'])."?token=".$token."&t=".$time."&u=".$_POST['PasswordResetRequestForm']['username'];
                    $cnt = "
                    To make sure this is your request, click this link bellow to comfirm and change new password:
                    <br>
                    <a href='".$str."'>".$str."</a>";
                    $x = Yii::$app->mailer->compose()
                    ->setHtmlBody($cnt)
                    ->setFrom(['info@moringnaasia.com' => "MoringnaAsia"])
                    ->setTo($user->email)
                    ->setSubject("Request password reset")
                    ->send();
                    $msg = "A new request password has been sent to your email!";
                }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
            "msg" => $msg
        ]);
    }
    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword()
    {
        $this->layout = "empty";
        if(!empty($_GET)){
            $otoken = $_GET['token'];
            $time = $_GET['t'];
            $u = $_GET['u'];
        }
        else {
            $otoken = $_POST['ResetPasswordForm']['token'];
            $time = $_POST['ResetPasswordForm']['t'];
            $u = $_POST['ResetPasswordForm']['u'];            

        }

        $token = md5($time."moasia".$u);
        $msg = "";
        $model = new ResetPasswordForm();
        if(!($otoken == $token) ){
            die("Wrong request!");
        }
        else{
            if($time <= time() - 3600)
                die("Your request is overtime! Please create new request password reset!");
            else{
                if(!empty($_POST)){
                        $user = User::find()->where(["username" => $u])->one();
                        if($user == null)
                            $msg = "Can not find this User!";
                        else{
                            if(!in_array($user->role_id, User::getRoleID(false)))
                                $msg = "Do not permission!";
                            else{
                                $user->password = generalHelper::fnEncrypt($_POST['ResetPasswordForm']['password']);
                                $user->save();
                                $msg = "Your password has been changed! Will redirect to login page in 5 seconds... <meta http-equiv='refresh' content='5;URL=".Url::to(['site/login'])."'>";
                            }
                        }
                }
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
            "msg" => $msg
        ]);
    }

    public function actionAddToCart(){
        $ob = !isset($_SESSION['cart']) ? array() : $_SESSION['cart'];
        $ob = array_merge($ob, array($_POST));
        $_SESSION['cart'] = $ob;
        $this->redirect(['site/products']);
    }

    public function actionClearCart(){
        $_SESSION['cart'] = null;
        $this->redirect(['site/products']);
    }

    //upline
    private $tmpRecuisive = [];
    function recuisive($parent_id){
        $u = User::find()->where("id = ".$parent_id." AND status = ".User::STATUS_ACTIVE." AND role_id IN (".implode(",", User::getRoleID(false)).")")->all();
        foreach ($u as $k => $v) {
            if(!empty($v->parent_id)){
                $this->tmpRecuisive[] = $v->parent_id;
                $this->recuisive($v->parent_id);
            }
        }
    }

    //downline
    private $tmpRecuisiveRevert = [];
    function recuisiveRevert($id){
        $u = User::find()->where("parent_id = ".$id."  AND status = ".User::STATUS_ACTIVE."  AND role_id IN (".implode(",", User::getRoleID(false)).")")->all();
        foreach ($u as $k => $v) {
            if(!empty($v->id)){
                $this->tmpRecuisiveRevert[] = $v->id;
                $this->recuisiveRevert($v->id);
            }
        }
    }

    //This one for update pv_record
    //upline2
    private $tmpRecuisiveB = [];
    function recuisiveB($parent_id){
        $u = User::find()->where("id = ".$parent_id." AND status = ".User::STATUS_ACTIVE)->all();
        foreach ($u as $k => $v) {
            if(!empty($v->parent_id)){
                $this->tmpRecuisiveB[] = $v->parent_id;
                $this->recuisiveB($v->parent_id);
            }
        }
    }

    //downline2
    private $tmpRecuisiveRevertB = [];
    function recuisiveRevertB($id){
        $u = User::find()->where("parent_id = ".$id."  AND status = ".User::STATUS_ACTIVE)->all();
        foreach ($u as $k => $v) {
            if(!empty($v->id)){
                $this->tmpRecuisiveRevertB[] = $v->id;
                $this->recuisiveRevertB($v->id);
            }
        }
    }


    public function actionGetParents($search = null, $id = null, $isBoth = null)
    {
        $this->recuisive(Yii::$app->user->identity->id);
        if($isBoth){
            $this->recuisiveRevert(Yii::$app->user->identity->id);
            $this->tmpRecuisive = array_merge($this->tmpRecuisive, $this->tmpRecuisiveRevert);
        }

        $out = ['more' => false];
        $data = [];
        if (!is_null($search)) {
            $query = new Query;
            $x = implode(",", $this->tmpRecuisive);
           
            $query->select(['id', new \yii\db\Expression("CONCAT(username, ' | ', fullname) as text")])
                ->from('{{%tbl_users}}')
                ->where(' ('.($x != "" ? "id IN (".$x.") OR " : "").' id = '.Yii::$app->user->identity->id.') AND username LIKE "%' . $search .'%"')
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
        
            $out['results'] = array_values($data);
        }
        elseif (!empty($id)) {
            $out['results'] = ['id' => $id, 'text' => User::findOne(['id' => $id])->username." | ".User::findOne(['id' => $id])->fullname];
        }
        else {
            $out['results'] = ['id' => 0, 'text' => 'No matching records found'];
        }
        echo Json::encode($out);
    }


    public function actionGetBank($country_id, $currentbank_id)
    {
        $this->layout = 'empty';
        $dt = TblBanks::getList($country_id);
        return $this->render('filter_bank', [
            'dt' => $dt,
            'currentbank_id' => $currentbank_id
        ]);
    }

    public function actionCalpvrecord(){
        $lstUser = User::find()->all();
        foreach ($lstUser as $k => $v) {
            //find child for each user
            $childUser = User::find()->where("parent_id = ".$v->id)->all();
            $tmpChildUserID = [];
            foreach ($childUser as $k2 => $v2) {
                $tmpChildUserID[] = $v2->id;
            }

            if(count($tmpChildUserID) > 0){
                //calculating pv_record in orders table
                $toalPV = TblProductOrders::find()->select('SUM(con_amount)')->where('user_id IN ('.implode(',', $tmpChildUserID).') AND created_at >= (UNIX_TIMESTAMP() - 86400 * 1200)')->scalar();
                //save pv_record
                $user = User::find()->where(["id" => $v->id])->one();
                $user->pv_record = $toalPV == null ? 0 : $toalPV;
                $user->save();
            }
        }
    }

    public function actionGetDataChart($d){
        $cUser = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $currentW = strtotime("last saturday") + $d * 7 * 10 *  86400;
        $lstWeeks = [
            "allowPre" => ($currentW - 9 * 7 * 86400 + 7 * 86400 - 1) > $cUser->created_at,
            "user_created_at" => $cUser->created_at,
            "lastDay" => ($currentW - 9 * 7 * 86400 + 7 * 86400 - 1),
            "labels" => [],
            "data" => []
        ];

        for($i = 0; $i < 10; $i++)
        {
            $saturday = $currentW - $i * 7 * 86400;
            $friday = $currentW - $i * 7 * 86400 + 7 * 86400 - 1;
            $start = date("d/m", $saturday);
            $end = date("d/m", $friday);
            $totalBonusWallet = TblHistoryBonus::find()->select('SUM(amount)')->where("user_id = ".Yii::$app->user->identity->id." AND wallet = ".TblWallets::TYPE_BONUS." AND created_at >= ".$saturday." AND created_at <= ".$friday)->scalar();
            $amount = $totalBonusWallet != null ? $totalBonusWallet : 0;
            $lstWeeks['data'][] = $amount;
            $lstWeeks['labels'][] = "$".$amount."  :  " . $end ;
        }

        echo json_encode($lstWeeks);
        exit;
    }

}
