function headerLayout() {
    if ($(window).scrollTop() >= ($('#about-us').offset().top - 70)) {
        if (!$('#nav-stick').is(":visible")) $('#nav-stick').fadeIn();
    } else {
        if ($('#nav-stick').is(":visible")) $('#nav-stick').fadeOut();
    }
}

function scrollpy() {
    $('section').each(function () {
        if ($(window).scrollTop() >= $(this).offset().top && $(window).scrollTop() <= $(this).offset().top + $(this).outerHeight()) {
            var target = $(this).attr('id');
            if ($(this).attr('id') === "info") target = "about-us";
            $('#nav-stick li').removeClass('active');
            $('#nav-stick li a[href="#' + target + '"]').parent().addClass('active');
        }
    });
}
$('.toggle-slide').on('click', function () {
    resetTimer();
    if ($(this).hasClass('next')) {
        slide('next');
    } else {
        slide('prev');
    }
});

function slide(type) {
    var el = $("#slideshow li.active"),
        slide_to;
    if (type === "next") {
        if (el.next().length) {
            slide_to = el.next();
        } else {
            slide_to = $("#slideshow li:first-child");
        }
    } else {
        if (el.prev().length) {
            slide_to = el.prev();
        } else {
            slide_to = $("#slideshow li:last-child");
        }
    }
    slide_to.addClass('process');
    $("#slideshow li.active").animate({
        opacity: 0
    }, 1500, function () {
        $("#slideshow li").removeClass('active process').removeAttr('style');
        slide_to.addClass('active');
    });
}
var timer;

function autoSlide() {
    timer = setInterval(function () {
        slide('next');
    }, 5000);
}

function resetTimer() {
    clearInterval(timer);
    autoSlide();
}
//Init Page
autoSlide();
var el = document.getElementById('parallax');
var parallax = new Parallax(el);
new WOW().init();
$(document).on('scroll ready', function () {
    headerLayout();
    scrollpy();
    $('#slideshow').imagesLoaded({
        background: '.bg-image'
    }, function () {
        $('#loading').fadeOut(function () {
            $('#loading').remove();
        });
    });
});
$('.menu li a,.scroll-down a').on('click', function (e) {
    if(!$(this).hasClass('link')) e.preventDefault();
    var scroll_to = $(this).attr('href');
    $("html, body").stop().animate({
        scrollTop: $(scroll_to).offset().top
    }, '500', 'swing', function () {

    });
});