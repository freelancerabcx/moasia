<?php

namespace common\helpers;

class generalHelper
{
	//AES
	public static function fnEncrypt($sValue, $salt = null)
	{
		if($salt == null)
			$salt = "A3-da-C^p1234567";

	    return rtrim(
	        base64_encode(
	            mcrypt_encrypt(
	                MCRYPT_RIJNDAEL_256,
	                $salt, $sValue, 
	                MCRYPT_MODE_ECB, 
	                mcrypt_create_iv(
	                    mcrypt_get_iv_size(
	                        MCRYPT_RIJNDAEL_256, 
	                        MCRYPT_MODE_ECB
	                    ), 
	                    MCRYPT_RAND)
	                )
	            ), "\0"
	        );
	}

	public static function fnDecrypt($sValue)
	{
	    return rtrim(
	        mcrypt_decrypt(
	            MCRYPT_RIJNDAEL_256, 
	            "A3-da-C^p1234567", 
	            base64_decode($sValue), 
	            MCRYPT_MODE_ECB,
	            mcrypt_create_iv(
	                mcrypt_get_iv_size(
	                    MCRYPT_RIJNDAEL_256,
	                    MCRYPT_MODE_ECB
	                ), 
	                MCRYPT_RAND
	            )
	        ), "\0"
	    );
	}
	public static function showTree($categories, $parent_id = 0, $char = '')
	{
	    $cate_child = array();
	    foreach ($categories as $key => $item)
	    {
	        if ($item['parent_id'] == $parent_id)
	        {
	            $cate_child[] = $item;
	            unset($categories[$key]);
	        }
	    }
	    if ($cate_child)
	    {
	        echo '<ul>';
	        foreach ($cate_child as $key => $item)
	        {
	            echo '<li><table><tr>
                <td class="email">'.$item['email'].'</td>
                <td> RANK: '.$item['rank_id'].'</td>
                <td> MONEY: '.($item['total']?$item['total']:0).'</td>
                <td> DSB: '.($item['DSB']?$item['DSB']:0).'</td>
                <td> MLB: '.($item['MLB']?$item['MLB']:0).'</td>';
                $wallet = $item->getTblWallets()->all();
                $total_bonus = $item['DSB'] + $item['MLB'];
                $total_bonus = $total_bonus ? $total_bonus : 0; 
                $total_money = 0;
                foreach($wallet as $wl){
                    switch($wl->type){
                        case 1:
                            echo '<td> RES: '.$wl['amount'].' | </td>';
                            $total_money+= $wl['amount'];
                            break;
                        case 2:
                            echo '<td> SPO: '.($wl['amount']+ $total_bonus*30/100).' | </td>';
                            $total_money+= $wl['amount']+ $total_bonus*30/100;
                            break;
                        case 0:
                            echo '<td> BON: '.($wl['amount'] + $total_bonus*70/100).' | </td>';
                            $total_money+= $wl['amount']+ $total_bonus*70/100;
                            break;
                    }
                }
                echo '<td> <b>TOT: '.$total_money.'</b></td>';
                echo '</tr></table>';
	            generalHelper::showTree($categories, $item['id'], $char.'');
	            echo '</li>';
	        }
	        echo '</ul>';
	    }
	}
    public static function showWallet($categories, $parent_id = 0, $char = '')
	{
	    $cate_child = array();
	    foreach ($categories as $key => $item)
	    {
	        if ($item['parent_id'] == $parent_id)
	        {
	            $cate_child[] = $item;
	            unset($categories[$key]);
	        }
	    }
	    if ($cate_child)
	    {
	        echo '<ul>';
	        foreach ($cate_child as $key => $item)
	        {
                $wallet = $item->getTblWallets()->all();
	            echo '<li><table><tr><td class="email">'.$item['email'].'</td>';
                foreach($wallet as $wl){
                    switch($wl->type){
                        case 1:
                            echo '<td> Register: '.$wl['amount'].'</td>';
                            break;
                        case 2:
                            echo '<td> Sponsor: '.$wl['amount'].'</td>';
                            break;
                        case 0:
                            echo '<td> Bonus: '.$wl['amount'].'</td>';
                            break;
                    }
                }
                echo '</tr></table>';
	            generalHelper::showWallet($categories, $item['id'], $char.'');
	            echo '</li>';
	        }
	        echo '</ul>';
	    }
	}
	public static function alias($title, $replace = '-')
	{
		/*
		 * Replace with "-" Change it if you want
		 */
		$replacement = $replace;
		$map = array();
		$quotedReplacement = preg_quote($replacement, '/');

		$default = array(
			'/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
			'/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
			'/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
			'/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø/' => 'o',
			'/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
			'/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/' => 'y',
			'/đ|Đ/' => 'd',
			'/ç/' => 'c',
			'/ñ/' => 'n',
			'/ä|æ/' => 'ae',
			'/ö/' => 'oe',
			'/ü/' => 'ue',
			'/Ä/' => 'Ae',
			'/Ü/' => 'Ue',
			'/Ö/' => 'Oe',
			'/ß/' => 'ss',
			'/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
			'/\\s+/' => $replacement,
			sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => ''
		);
		// Some URL was encode, decode first
		$title = htmlentities(str_replace('µ', 'u', $title));
		$title = html_entity_decode($title);
		$title = urldecode($title);

		$map = array_merge($map, $default);
		return strtolower(preg_replace(array_keys($map), array_values($map), $title));
		// ---------------------------------o
	}

	public static function img($path)
	{
		if(!file_exists($_SERVER['DOCUMENT_ROOT']."/sadmin/web/files/".$path))
			$path = "img/no_img.jpg";

		return "/admin/files/".$path;
	}
}