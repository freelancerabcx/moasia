<?php



namespace common\helpers;

use common\models\LoginForm;

use common\models\User;

use common\models\TblPaidTransaction;

use common\models\TblWallets;

use common\models\TblRank;

use common\models\TblConfig;

use common\models\TblHistoryBonus;

use common\models\TblProductOrders;

use common\models\TblCountry;

class processHelper

{

    const day_sec_count = 86400;

    const day_rank = 30;

    const bonus_percent = 0.7;

    const product_percent = 0.3;

    //Update rank by user id

    public static function updateRankByUser($user_id, $amount){

        $user_obj = User::find()->where(['=','id',$user_id])->one();

        //If user created at more than 30 days
        if( time() - $user_obj->created_at > processHelper::day_sec_count*processHelper::day_rank ){
            $rank = processHelper::getRankByAmount($amount);
            //If new rank more than old rank
            if($rank > $user_obj->rank_id){
                $user_obj->rank_id = $rank;
                $user_obj->save();
            }
        }else{
            $user = User::find()
                    ->select([
                        'tbl_users.*',
                        'SUM(con_amount*quantity) AS total'
                    ])->where(['=','tbl_users.id',$user_id])
                    ->andWhere(['>=','tbl_product_orders.created_at', time()-86400*30])
                    ->leftJoin('tbl_product_orders','`tbl_product_orders`.`user_id` = `tbl_users`.`id`')
                    ->groupBy('`tbl_users`.`id`')
                    ->one();
            $rank = processHelper::getRankByAmount($user->total);
            //If new rank more than old rank
            if($rank > $user_obj->rank_id){
                $user_obj->rank_id = $rank;
                $user_obj->save();
            }
        }
    }

    public static  function resetMonoleg($parent_id){

        $parent_user = User::find()->where([ 'id' => $parent_id]);

        $config = TblConfig::getConfig( "rank_" );

        if($parent_user){

            switch($parent_user->rank_id){

                case TblRank::BRONZE:

                        $key = 'rank_bronze';

                    break;

                case TblRank::SILVER:

                        $key = 'rank_rank_silver';

                    break;

                case TblRank::GOLD:

                        $key = 'rank_rank_gold';

                    break;

                case TblRank::PLATINUM:

                        $key = 'rank_rank_platinum';

                    break;

                case TblRank::DIAMOND:

                        $key = 'rank_rank_diamond';

                    break;

                default:

                    break;

            }

            if(isset($key)){

                if( $config[$key] * 2 <= $parent_user->bonus_record ){

                    $parent_user->bonus_record -= $config[$key] * 2;

                    $parent_user->save();

                }

            }

        }

    }

    //Get rank by amount

    public static function getRankByAmount($amount){

        $config = TblConfig::getConfig( "rank_" );

        if($amount >= $config['rank_bronze'] && $amount < $config['rank_silver']){

            $rank_id = 1;

        }elseif($amount >= $config['rank_silver'] && $amount < $config['rank_gold']){

            $rank_id = 2;

        }elseif($amount >= $config['rank_gold'] && $amount < $config['rank_platinum']){

            $rank_id = 3;

        }elseif($amount >= $config['rank_platinum'] && $amount < $config['rank_diamond']){

            $rank_id = 4;

        }elseif($amount >= $config['rank_diamond']){

            $rank_id = 5;

        }else{

            $rank_id = 0;

        }

        return $rank_id;

    }

    //Direct sponsor bonus by User

    public static function directSponsorBonusByUser($user_id, $amount){

        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction(); 

        try {

            //GET current user

            $user_obj = User::find()->where(['=','id',$user_id])->one();

            //GET parrent user

            $parrent_obj = User::find()

                ->where(['=','id',$user_obj->parent_id])

                ->andWhere(['<>','role_id',User::ROLE_FREE_MEMBER])

                ->one();

            if( $user_obj->parent_id > 0 && $parrent_obj ){

                //Get money

                $percent     = processHelper::getBonusPercentByRank($parrent_obj->rank_id);

                $bonus_range = $amount * $percent/100;

                $bonus_amount =  processHelper::bonus_percent * $bonus_range;

                $product_amount = processHelper::product_percent * $bonus_range;

                //Update bonus recor

                $parrent_obj->bonus_record += $bonus_range;

                $parrent_obj->save();

                //Update wallet and add history

                processHelper::addHistory($user_id,$user_obj->parent_id,$bonus_amount,TblHistoryBonus::TYPE_DIRECT,TblWallets::TYPE_BONUS);

                processHelper::addHistory($user_id,$user_obj->parent_id,$product_amount,TblHistoryBonus::TYPE_DIRECT,TblWallets::TYPE_POINT);

                processHelper::updateWalletByUser($parrent_obj->id,TblWallets::TYPE_BONUS,$bonus_amount);

                processHelper::updateWalletByUser($parrent_obj->id,TblWallets::TYPE_POINT,$product_amount);

                

            }

            TblProductOrders::updateAll(['status' => TblProductOrders::IN_OVR],[ 'status' => TblProductOrders::IN_DBS, 'user_id' => $user_id ]);

            $transaction->commit();

        } catch(\Exception $e) {

            $transaction->rollBack();

            throw $e;

        }

    }    

    public static function overridingBonusByUser($user_id, $amount){

        //GET Config

        $config = TblConfig::getConfig( TblConfig::BONUS );

        //Do recursion
        $user_obj = User::find()->where(['=','id',$user_id])->one();
        processHelper::recursion($amount,$user_id,$user_obj->level,$user_obj->parent_id, $config);
        TblProductOrders::updateAll(['status' => TblProductOrders::IN_DEEP],[ 'status' => TblProductOrders::IN_OVR, 'user_id' => $user_id ]);

    }

    public static function recursion($total,$user_id, $level,$parent_id,$config){
        if(!$user_id) return false;

        $parent_user = User::find()->where([ 'id' => $parent_id ])
                        ->andWhere(['<>','role_id',User::ROLE_FREE_MEMBER])
                        ->one();
        if(!$parent_user) return;

            //Lấy ra số lv max được hưởng

            switch($parent_user->rank_id){

                case TblRank::BRONZE:

                    $max_lv = $config['bonus_GB_bronze_maxlv'];

                    break;

                case TblRank::SILVER:

                    $max_lv = $config['bonus_GB_silver_maxlv'];

                    break;

                case TblRank::GOLD:

                    $max_lv = $config['bonus_GB_gold_maxlv'];

                    break;

                case TblRank::PLATINUM:

                    $max_lv = $config['bonus_GB_platinum_maxlv'];

                    break;

                case TblRank::DIAMOND:

                    $max_lv = $config['bonus_GB_diamond_maxlv'];

                    break;

                default:

                    $max_lv = 0;

                    break;

            }
            //Lấy ra số lv chênh lệch

            $level_tree = $level - $parent_user->level - 1;
            //Lấy ra số % được hưởng

            if($level_tree > 0 

                    && $level_tree <= $config['bonus_GB_bronze_maxlv']){

                $percent = $config['bonus_GB_bronze'];

            }elseif($level_tree > $config['bonus_GB_bronze_maxlv'] 

                    && $level_tree <= $config['bonus_GB_silver_maxlv']){

                $percent = $config['bonus_GB_silver'];

            }elseif($level_tree > $config['bonus_GB_silver_maxlv'] 

                    && $level_tree <= $config['bonus_GB_gold_maxlv']){

                $percent = $config['bonus_GB_gold'];

            }elseif($level_tree > $config['bonus_GB_gold_maxlv'] 

                    && $level_tree <= $config['bonus_GB_platinum_maxlv']){

                $percent = $config['bonus_GB_platinum'];

            }elseif($level_tree > $config['bonus_GB_platinum_maxlv'] 

                    && $level_tree <= $config['bonus_GB_diamond_maxlv']){

                $percent = $config['bonus_GB_diamond'];

            }else{

                $percent = 0;

            }

            //Nếu level lớn hơn max rồi thì không được nhận bonus nữa

            if( $level_tree >= $max_lv )  $percent = 0;

            $bonus_range = $total * $percent/100;

            $bonus_amount =  processHelper::bonus_percent * $bonus_range;

            $product_amount = processHelper::product_percent * $bonus_range;
            //Update wallet and add history
            if($bonus_range>0){

                $connection = \Yii::$app->db;

                $transaction = $connection->beginTransaction(); //get parent user

                try {

                    $parent_user->bonus_record += $bonus_range;

                    $parent_user->save();
                    
                    processHelper::addHistory($user_id,$parent_user->id,$bonus_amount,TblHistoryBonus::TYPE_OVER,TblWallets::TYPE_BONUS);
                    processHelper::addHistory($user_id,$parent_user->id,$product_amount,TblHistoryBonus::TYPE_OVER,TblWallets::TYPE_POINT);
                    processHelper::updateWalletByUser($parent_user->id,TblWallets::TYPE_BONUS,$bonus_amount);
                    processHelper::updateWalletByUser($parent_user->id,TblWallets::TYPE_POINT,$product_amount);

                    $transaction->commit();
                } catch(\Exception $e) {

                    $transaction->rollBack();

                    throw $e;

                }

            }

            

        //Update và chạy lại hệ thống

        processHelper::recursion($total,$user_id, $level,$parent_user->parent_id,$config);

    }

    public static function addHistory($from_id, $to_id, $amount, $type_bonus,$type_wallet){
        $history = new TblHistoryBonus();

        $history->user_id = $to_id;

        $history->from_user_id = $from_id;

        $history->amount = $amount;

        $history->type = $type_bonus;

        $history->wallet = $type_wallet;

        $history->status = TblHistoryBonus::STT_ACTIVE;

        $history->created_at = time();

        $history->save();

    }

    public static function updateWalletByUser($user_id, $type_wallet, $amount){
        $wallet = TblWallets::find()->where(['user_id' => $user_id])

                                    ->andWhere([ 'type'=> $type_wallet ])->one();

        $wallet->amount += $amount;

        $wallet->save();

    }

    //Get 

    public static function getBonusPercentByRank($rank_id){

        $config = TblConfig::getConfig( TblConfig::BONUS );

        switch($rank_id){

            case TblRank::BRONZE:

                $percent = $config['bonus_DSB_bronze'];

                break;

            case TblRank::SILVER:

                $percent = $config['bonus_DSB_silver'];

                break;

            case TblRank::GOLD:

                $percent = $config['bonus_DSB_gold'];

                break;

            case TblRank::PLATINUM:

                $percent = $config['bonus_DSB_platinum'];

                break;

            case TblRank::DIAMOND:

                $percent = $config['bonus_DSB_diamond'];

                break;

            default:

                $percent=0;

                break;

        }

        return $percent;

    }

    public static function mlnListUser($users,$country){

        $count_bronze = [];

        $count_silver = [];

        $count_gold = [];

        $count_platinum = [];

        $count_diamond = [];

        $config = TblConfig::getConfig( "rank_" );

        foreach($users as $user){
            if(!$user->total) $user->total = 0;
            if($user->total >= $country->bonus_MNL_max && $user->id > 0){

                switch($user->rank_id){

                    case TblRank::BRONZE:

                        if( processHelper::checkValidMln( $user, $config['rank_bronze'])){

                            $count_bronze[]= $user->id;

                        }

                        break;

                    case TblRank::SILVER:

                        if( processHelper::checkValidMln( $user, $config['rank_silver'])){

                            $count_silver[]= $user->id;

                        }

                        break;

                    case TblRank::GOLD:

                        if( processHelper::checkValidMln( $user, $config['rank_gold'])){

                            $count_gold[]= $user->id;

                        }

                        break;

                    case TblRank::PLATINUM:

                        if( processHelper::checkValidMln( $user, $config['rank_platinum'])){

                            $count_platinum[]= $user->id;

                        }

                        break;

                    case TblRank::DIAMOND:

                        if( processHelper::checkValidMln( $user, $config['rank_diamond'])){

                            $count_diamond[]= $user->id;

                        }

                        break;

                    default:

                        break;

                }

            }

        }
        $country_data = processHelper::countryAna($country->id);
        $total = $country->bonus_MNL_total *  $country_data['bonus_today'];
        $total_person = count($count_bronze)*$country->bonus_MNL_bronze 

            + count($count_silver)*$country->bonus_MNL_silver 

            + count($count_gold)*$country->bonus_MNL_gold

            + count($count_platinum)*$country->bonus_MNL_platinum 

            + count($count_diamond)*$country->bonus_MNL_diamond;

        $money_amount = ($total_person>0) ? $total/$total_person : 0;

        if($money_amount!=0){

            $money_amount = round( $money_amount, 2 );

        }

        return array(
            "unit" => $money_amount,
            'amount' => $country_data['bonus_today'],
            "total" => $total_person,
            "bronze" => $count_bronze,
            "silver" => $count_silver,
            "gold" => $count_gold,
            "platinum" => $count_platinum,
            "diamond" => $count_diamond,
        );
    }
    public static function countryAna($country_id){
        $orders_today    = User::find()
                ->select([
                    'tbl_users.*',
                    'SUM(amount* quantity) AS total'
                ])->where(['>=','tbl_product_orders.created_at', strtotime(date('Y-m-d'))])
                ->andWhere(['=','tbl_users.country_id', $country_id])
                ->leftJoin('tbl_product_orders','`tbl_product_orders`.`user_id` = `tbl_users`.`id`')
                ->groupBy('`tbl_users`.`country_id`')
                ->one();
        $bonus_today    = User::find()
                ->select([
                    'tbl_users.*',
                    'SUM(amount) AS total'
                ])->where(['>=','tbl_history_bonus.created_at', strtotime(date('Y-m-d'))])
                ->andWhere(['=','tbl_users.country_id', $country_id])
                ->andWhere(['<','tbl_history_bonus.type', 4])
                ->leftJoin('tbl_history_bonus','`tbl_history_bonus`.`user_id` = `tbl_users`.`id`')
                ->groupBy('`tbl_users`.`country_id`')
                ->one();
        $orders_total = $orders_today ? $orders_today->total : 0;
        $bonus_total = $bonus_today ? $bonus_today->total : 0;
        return array(
            'order_today' =>$orders_total,
            'bonus_today' =>$orders_total - $bonus_total,
        );
    }
    public static function calMln($list_user,$unit,$country){

        foreach($list_user as $id){

            $user = User::find()->where(['=','id',$id])->one();

            switch($user->rank_id){

                case TblRank::BRONZE:

                    $money = $unit*$country->bonus_MNL_bronze;

                    break;

                case TblRank::SILVER:

                    $money = $unit*$country->bonus_MNL_silver;

                    break;

                case TblRank::GOLD:

                    $money = $unit*$country->bonus_MNL_gold;

                    break;

                case TblRank::PLATINUM:

                    $money = $unit*$country->bonus_MNL_platinum;

                    break;

                case TblRank::DIAMOND:

                    $money = $unit*$country->bonus_MNL_diamond;

                    break;

            }
            $money = $money /100;
            $bonus_amount =  processHelper::bonus_percent * $money;
            $product_amount = processHelper::product_percent * $money;
            $user->bonus_record += $money;
            $user->save();

                //Update wallet and add history

                processHelper::addHistory(0,$user->id,$bonus_amount,TblHistoryBonus::TYPE_MLN,TblWallets::TYPE_BONUS);

                processHelper::addHistory(0,$user->id,$product_amount,TblHistoryBonus::TYPE_MLN,TblWallets::TYPE_POINT);

                processHelper::updateWalletByUser($user->id,TblWallets::TYPE_BONUS,$bonus_amount);

                processHelper::updateWalletByUser($user->id,TblWallets::TYPE_POINT,$product_amount);

        }

    }

    public static function checkValidMln($user, $config){
        if($user->bonus_record > $config*2){
            $pv_record = $user->pv_record;
            if( $pv_record - $config > 0 ){
                return true;
            }else{
                //Gửi mail ở đây
                try{
                    $cnt = "Hi ".$user->username." (".$user->fullname."),
                    <br><br>
                    Your account has been stopped receiving Daily Bonus because of disqualification. Please contact our administrator (<a href='mailto:support@moringna.asia'>support@moringna.asia</a>) if you don’t know clearly why.
                    <br><br>
                    Thank you!";
                    $x = Yii::$app->mailer->compose()
                    ->setHtmlBody($cnt)
                    ->setFrom(['info@moringnaasia.com' => "MoringnaAsia"])
                    ->setTo($user->email)
                    ->setSubject("Stop receiving Daily bonus at MoringnaAsia")
                    ->send();
                }catch(Exception $e){
                    return false;
                }
                return false;
            }
        }else{
            return true;
        }
    }

    public static function getBonus($u,$country,$min_get_bonus){

        switch($u->rank_id){

            case TblRank::BRONZE:

                $mln_bonus = $country->bonus_MNL_bronze * $min_get_bonus;

                break;

            case TblRank::SILVER:

                $mln_bonus = $country->bonus_MNL_silver * $min_get_bonus;

                break;

            case TblRank::GOLD:

                $mln_bonus = $country->bonus_MNL_gold * $min_get_bonus;

                break;

            case TblRank::PLATINUM:

                $mln_bonus = $country->bonus_MNL_platinum * $min_get_bonus;

                break;

            case TblRank::DIAMOND:

                $mln_bonus = $country->bonus_MNL_diamond * $min_get_bonus;

                break;

        }

        return $mln_bonus;

    }

}