<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_product_orders".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $amount
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $is_delivery
 * @property integer $created_at
 *
 * @property TblProducts $product
 * @property TblUsers $user
 */
class TblProductOrders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    const IN_DBS = 2;
    const IN_OVR = 1;
    const IN_DONE = 0;//All done
    const IN_DEEP = 99;//Monoleg
    public static function tableName()
    {
        return 'tbl_product_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id', 'quantity', 'is_delivery', 'created_at', 'country_id', 'is_received', 'received_quantity'], 'integer'],
            [['amount', 'con_amount', 'voucher_amount'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProducts::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'is_delivery' => 'Is Delivery',
            'userFullname' => 'Full Name',
            'productName' => 'Product Name',
            'is_received' => 'Received',
            'received_quantity' => 'Received Quantity',
            'userName' => 'Username',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(TblProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(TblCountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserFullname(){
        return $this->user->fullname;
    }

    public function getUserName(){
        return $this->user->username;
    }

    public function getProductName(){
        return $this->product->name;
    }

}
