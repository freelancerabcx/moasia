<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_paid_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $amount
 * @property string $description
 * @property integer $status
 * @property integer $updated_at
 * @property integer $created_by
 *
 * @property TblUsers $user
 */
class TblPaidTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $total;
    public static function tableName()
    {
        return 'tbl_paid_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'updated_at', 'created_by'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'description' => 'Description',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
