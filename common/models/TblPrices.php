<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_prices".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $currency_id
 * @property double $price
 * @property integer $updated_at
 */
class TblPrices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_prices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'country_id', 'updated_at'], 'integer'],
            [['price', 'price_pv'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'country_id' => 'Country',
            'price_pv' => 'P.V',
            'price' => 'Price',
            'updated_at' => 'Updated At',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(TblCountry::className(), ['id' => 'country_id']);
    }
    /**
     * Gets a list of all the models.
     * Can be used as array in yii\widgets\ActiveForm dropDownList().
     *
     * @param int $excludeId ignoring model with this id
     * @return array list of all models.
     */
    public static function getList($excludeId = 0)
    {
        $list = [];
        $query = self::find();

        if (!empty($excludeId)) {
            $query->where('`product_id` = :excludeId', ['excludeId' => $excludeId]);
        }

        $models = $query->all();

        foreach ($models as $model) {
            $list[$model->id] = [
                "price_pv" => $model->price_pv,
                "price" => $model->price,
                "country" => $model->country_id,
            ];
        }

        return $list;
    }
}
