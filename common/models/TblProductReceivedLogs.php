<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_product_received_logs".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $quantity
 * @property integer $created_at
 */
class TblProductReceivedLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_product_received_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'order_id', 'quantity'], 'required'],
            [['product_id', 'order_id', 'quantity', 'created_at', 'total_ordered'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'order_id' => 'Order ID',
            'quantity' => 'Quantity',
            'total_ordered' => 'Total Order',
            'created_at' => 'Created At',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(TblProducts::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(TblProductOrders::className(), ['id' => 'order_id']);
    }
}
