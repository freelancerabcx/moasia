<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_transactions".
 *
 * @property integer $id
 * @property integer $type
 * @property double $amount
 * @property integer $user_id
 * @property string $paypal_transaction_id
 * @property string $api_respones
 * @property integer $created_at
 *
 * @property TblUsers $user
 */
class TblTransactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'user_id', 'created_at'], 'integer'],
            [['amount'], 'number'],
            [['api_respones'], 'string'],
            [['paypal_transaction_id'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'amount' => 'Amount',
            'user_id' => 'User ID',
            'paypal_transaction_id' => 'Paypal Transaction ID',
            'api_respones' => 'Api Respones',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
