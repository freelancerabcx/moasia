<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_history_bonus".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $amount
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 */
class TblHistoryBonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STT_ACTIVE = 1;
    const STT_INACTIVE = 0;
    const TYPE_DIRECT = 1;
    const TYPE_OVER = 2;
    const TYPE_MLN = 3;
    const TYPE_REG = 4;
    const TYPE_SHA = 5;//Share money to the other user
    const TYPE_MTB = 6; //Mounthy bonus
    public $total;
    public static function tableName()
    {
        return 'tbl_history_bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'type', 'status', 'created_at'], 'required'],
            [['user_id', 'type', 'status', 'created_at'], 'integer'],
            [['amount', 'wallet'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'type' => 'Type',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }
}
