<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblConfig;

/**
 * TblConfigSearch represents the model behind the search form about `common\models\TblConfig`.
 */
class TblConfigSearch extends TblConfig
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_name', 'value'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblConfig::find()->where(['=','status','1']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'key_name', $this->key_name])
            ->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
