<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_currency".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property double $usd_rate
 * @property integer $updated_at
 * @property integer $updated_by
 */
class TblCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usd_rate'], 'number'],
            [['updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'usd_rate' => 'Usd Rate',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    /**
     * Gets a list of all the models.
     * Can be used as array in yii\widgets\ActiveForm dropDownList().
     *
     * @param int $excludeId ignoring model with this id
     * @return array list of all models.
     */
    public static function getList($excludeId = 0)
    {
        $list = [];
        $query = self::find()->orderBy('name');

        if (!empty($excludeId)) {
            $query->where('`id` != :excludeId', ['excludeId' => $excludeId]);
        }

        $models = $query->all();

        foreach ($models as $model) {
            $list[$model->id] = $model->name;
        }

        return $list;
    }
}
