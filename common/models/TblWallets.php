<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_wallets".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $amount
 * @property integer $type
 * @property integer $last_transaction_id
 * @property integer $updated_at
 *
 * @property TblUsers $user
 */
class TblWallets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const TYPE_BONUS = 1;
    const TYPE_POINT = 2;
    const TYPE_RESGI = 0;
    public static function tableName()
    {
        return 'tbl_wallets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'last_transaction_id', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Amount',
            'type' => 'Type',
            'last_transaction_id' => 'Last Transaction ID',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
