<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblProductReceivedLogs;

/**
 * TblProductReceivedLogsSearch represents the model behind the search form about `common\models\TblProductReceivedLogs`.
 */
class TblProductReceivedLogsSearch extends TblProductReceivedLogs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'order_id', 'quantity', 'created_at', 'total_ordered'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblProductReceivedLogs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'order_id' => $this->order_id,
            'quantity' => $this->quantity,
            'total_ordered' => $this->total_ordered,
        ]);

        if($this->created_at != ""){
            $gm = strtotime(DATE("Y-m-d",$this->created_at));
            $query->andWhere('tbl_product_received_logs.created_at >= '.$gm." AND tbl_product_received_logs.created_at <= ".($gm + 86400));
        }

        return $dataProvider;
    }
}
