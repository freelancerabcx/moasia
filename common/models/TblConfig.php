<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_config".
 *
 * @property string $key_name
 * @property string $key_text
 * @property double $value
 * @property string $status
 */
class TblConfig extends \yii\db\ActiveRecord
{
    const BONUS = 'bonus_';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key_name', 'key_text', 'value'], 'required'],
            [['value'], 'number'],
            [['status'], 'string'],
            [['key_name'], 'string', 'max' => 50],
            [['key_text'], 'string', 'max' => 200],
        ];
    }
    
    public static function getConfig($string){
        $config = [];
        $config_data = TblConfig::find()->where(['like','key_name',$string])->all();
        foreach($config_data as $vl){
            $config[$vl->key_name] = $vl->value;
        }
        return $config;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key_name' => 'Key Name',
            'key_text' => 'Config key',
            'value' => 'Value',
            'status' => 'Status',
        ];
    }
}
