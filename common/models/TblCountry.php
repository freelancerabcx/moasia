<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_country".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $bonus_MNL_bronze
 * @property string $bonus_MNL_silver
 * @property string $bonus_MNL_gold
 * @property string $bonus_MNL_platinum
 * @property string $bonus_MNL_diamond
 * @property string $bonus_MNL_total
 * @property string $bonus_MNL_max
 * @property string $pv_rate
 * @property integer $updated_at
 * @property integer $updated_by
 */

class TblCountry extends \yii\db\ActiveRecord
{
    public $today_orders;
    public $today_registers;
    public $after_bonus;
    public $active_user;
    public $total_user;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_MNL_bronze', 'bonus_MNL_silver', 'bonus_MNL_gold', 'bonus_MNL_platinum', 'bonus_MNL_diamond', 'bonus_MNL_total', 'bonus_MNL_max', 'pv_rate', 'tax'], 'number'],
            [['updated_at', 'updated_by'], 'integer'],
            [['bonus_MNL_bronze', 'bonus_MNL_silver', 'bonus_MNL_gold', 'bonus_MNL_platinum', 'bonus_MNL_diamond', 'bonus_MNL_total', 'bonus_MNL_max', 'tax', 'code', 'name'], 'required'],
            [['code'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'bonus_MNL_bronze' => 'Bonus  Mnl Bronze',
            'bonus_MNL_silver' => 'Bonus  Mnl Silver',
            'bonus_MNL_gold' => 'Bonus  Mnl Gold',
            'bonus_MNL_platinum' => 'Bonus  Mnl Platinum',
            'bonus_MNL_diamond' => 'Bonus  Mnl Diamond',
            'bonus_MNL_total' => 'Total MLB to pay (%)',
            'bonus_MNL_max' => 'Bonus Mnl Max',
            'tax' => 'Tax',
            'pv_rate' => 'P.V Rate (Ex: 0.5pv = $1, 5pv = $1)',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    /**
     * Gets a list of all the models.
     * Can be used as array in yii\widgets\ActiveForm dropDownList().
     *
     * @param int $excludeId ignoring model with this id
     * @return array list of all models.
     */
    public static function getList($excludeId = 0)
    {
        $list = [];
        $query = self::find()->orderBy('name');

        if (!empty($excludeId)) {
            $query->where('`id` != :excludeId', ['excludeId' => $excludeId]);
        }

        $models = $query->all();

        foreach ($models as $model) {
            $list[$model->id] = $model->name;
        }

        return $list;
    }
}
