<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblProductOrders;

/**
 * TblProductOrdersSearch represents the model behind the search form about `common\models\TblProductOrders`.
 */
class TblProductOrdersSearch extends TblProductOrders
{
    /**
     * @inheritdoc
     */
    public $userFullname;
    public $userName;
    public $productName;
    public function rules()
    {
        return [
            [['id', 'user_id', 'product_id', 'country_id', 'quantity', 'is_delivery', 'created_at', 'is_received'], 'integer'],
            [['amount'], 'number'],
            [['userFullname', 'userName', 'productName'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblProductOrders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

         $dataProvider->setSort([
            'attributes' => [
                'id',
                'amount',
                'product_id',
                'country_id',
                'quantity',
                'created_at',
                'is_received',
                'userFullname' => [
                    'asc' => ['fullname' => SORT_ASC],
                    'desc' => ['fullname' => SORT_DESC],
                    'label' => 'Full Name',
                    'default' => SORT_ASC
                ],
                'userName' => [
                    'asc' => ['username' => SORT_ASC],
                    'desc' => ['username' => SORT_DESC],
                    'label' => 'Username',
                    'default' => SORT_ASC
                ],
                'productName' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],
                    'label' => 'Product Name',
                    'default' => SORT_ASC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['user']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'product_id' => $this->product_id,
            'tbl_product_orders.country_id' => $this->country_id,
            'quantity' => $this->quantity,
            'is_delivery' => $this->is_delivery,
            'is_received' => $this->is_received,
        ])
        ->orderBy('created_at DESC');

        if($this->created_at != ""){
            $gm = strtotime(DATE("Y-m-d",$this->created_at));
            $query->andWhere('tbl_product_orders.created_at >= '.$gm." AND tbl_product_orders.created_at <= ".($gm + 86400));
        }


        // filter by country name
        $query->joinWith(['user' => function ($q) {
            $q->where('tbl_users.fullname LIKE "%' . $this->userFullname . '%" AND tbl_users.username LIKE "%' . $this->userName . '%"');
        }]);


        // filter by country name
        $query->joinWith(['product' => function ($q) {
            $q->where('tbl_products.name LIKE "%' . $this->productName . '%"');
        }]);


        return $dataProvider;
    }
}