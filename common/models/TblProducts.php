<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_products".
 *
 * @property integer $id
 * @property integer $category_id
 * @property double $price
 * @property integer $status
 * @property string $name
 * @property string $alias
 * @property string $info
 * @property string $content
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property TblProductOrders[] $tblProductOrders
 * @property TblProductCategory $category
 */
class TblProducts extends \yii\db\ActiveRecord
{
    public $today_orders;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'status', 'name', 'updated_at', 'updated_by'], 'required'],
            [['category_id', 'status', 'updated_at', 'updated_by'], 'integer'],
            [['voucher_amount'], 'number'],
            [['info', 'content', 'description'], 'string'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProductCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'status' => 'Status',
            'name' => 'Name',
            'alias' => 'Alias',
            'info' => 'Info',
            'content' => 'Content',
            'voucher_amount' => 'Monthly Bonus (%)',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductOrders()
    {
        return $this->hasMany(TblProductOrders::className(), ['product_id' => 'id']);
    }
    
    public function getSumOrders(){
        return $this->hasMany(TblProductOrders::className(), ['product_id' => 'id'])
                    ->sum('quantity');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(TblPrices::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TblProductCategory::className(), ['id' => 'category_id']);
    }


}