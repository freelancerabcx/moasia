<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_product_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property TblProducts[] $tblProducts
 */
class TblProductCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'updated_at', 'updated_by'], 'required'],
            [['updated_at', 'updated_by','is_voucher', 'status'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProducts()
    {
        return $this->hasMany(TblProducts::className(), ['category_id' => 'id']);
    }
    /**
     * Gets a list of all the models.
     * Can be used as array in yii\widgets\ActiveForm dropDownList().
     *
     * @param int $excludeId ignoring model with this id
     * @return array list of all models.
     */
    public static function getList($excludeId = 0)
    {
        $list = [];
        $query = self::find()->orderBy('name');

        if (!empty($excludeId)) {
            $query->where('`id` != :excludeId', ['excludeId' => $excludeId]);
        }

        $models = $query->all();

        foreach ($models as $model) {
            $list[$model->id] = $model->name;
        }

        return $list;
    }
}
