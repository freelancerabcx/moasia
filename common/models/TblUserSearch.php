<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * TblUserSearch represents the model behind the search form about `common\models\TblUsers`.
 */
class TblUserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'rank_id','country_id','currency_id', 'role_id', 'parent_id', 'birthday', 'gender', 'status', 'updated_at', 'updated_by'], 'integer'],
            [['username', 'email', 'password', 'fullname', 'address', 'city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'role_id' => $this->role_id,
            'parent_id' => $this->parent_id,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'status' => $this->status,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['!=', 'role_id', '1'])
            ->andFilterWhere(['=', 'country_id', $this->country_id])
            ->andFilterWhere(['=', 'currency_id', $this->currency_id]);

        if(Yii::$app->user->identity->role_id == User::ROLE_ADMIN)
            $query->andFilterWhere(['!=', 'role_id', '2']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
