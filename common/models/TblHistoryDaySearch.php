<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TblHistoryDay;

/**
 * TblHistoryDaySearch represents the model behind the search form about `common\models\TblHistoryDay`.
 */
class TblHistoryDaySearch extends TblHistoryDay
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'created_at'], 'integer'],
            [['income', 'after_bonus', 'monoleg'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblHistoryDay::find()->orderBy('id desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'income' => $this->income,
            'after_bonus' => $this->after_bonus,
            'monoleg' => $this->monoleg,
            'country_id' => $this->country_id,
            'created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}