<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_banks".
 *
 * @property integer $id
 * @property string $name
 * @property integer $country_id
 * @property integer $updated_at
 */
class TblBanks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_banks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'country_id' => 'Country ID',
            'updated_at' => 'Updated At',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(TblCountry::className(), ['id' => 'country_id']);
    }
    /**
     * Gets a list of all the models.
     * Can be used as array in yii\widgets\ActiveForm dropDownList().
     *
     * @param int $excludeId ignoring model with this id
     * @return array list of all models.
     */
    public static function getList($country_id = 0)
    {
        $list = [];
        $query = self::find()->orderBy('name');

        if (!empty($country_id)) {
            $query->where('`country_id` = :country_id', ['country_id' => $country_id]);
        }

        $models = $query->all();

        foreach ($models as $model) {
            $list[$model->id] = $model->name;
        }

        return $list;
    }
}
