<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_history_day".
 *
 * @property integer $id
 * @property string $income
 * @property string $bonus
 * @property string $monoleg
 * @property integer $country_id
 * @property integer $created_at
 */
class TblHistoryDay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_history_day';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['income', 'after_bonus', 'monoleg', 'country_id', 'created_at'], 'required'],
            [['income', 'after_bonus', 'monoleg'], 'number'],
            [['country_id', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'income' => 'Income',
            'after_bonus' => 'After Bonus',
            'monoleg' => 'Daily bonus',
            'country_id' => 'Country',
            'created_at' => 'Created At',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(TblCountry::className(), ['id' => 'country_id']);
    }
}
