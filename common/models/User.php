<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use common\helpers\generalHelper;

/**
 * This is the model class for table "tbl_users".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property integer $rank_id
 * @property integer $role_id
 * @property string $fullname
 * @property string $address
 * @property string $city
 * @property string $country
 * @property integer $parent_id
 * @property integer $birthday
 * @property integer $gender
 * @property integer $status
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property TblPaidTransaction[] $tblPaidTransactions
 * @property TblProductOrders[] $tblProductOrders
 * @property TblTransactions[] $tblTransactions
 * @property TblRank $rank
 * @property TblRoles $role
 * @property TblWallets[] $tblWallets
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const ROLE_FREE_MEMBER = 4;
    const ROLE_MEMBER = 3;
    const ROLE_ADMIN = 2; //User Manager
    const ROLE_PADMIN = 5;//Product Manager
    const ROLE_SADMIN = 1;
    /**
     * @inheritdoc
     */
    public $total;
    public $password_repeat;
    public $DSB;
    public $MLB;
    public static function tableName()
    {
        return 'tbl_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password', 'fullname', 'status', 'updated_at', 'updated_by'], 'required'],
            [['parent_id'], 'required', 'on' => 'isRequired'],
            [['password_repeat'], 'required', 'on' => 'hasToCheck'],
            [['password', 'auth_key', 'phone'], 'string'],
            [['rank_id', 'country_id', 'bank_id', 'currency_id', 'is_new', 'is_old', 'level', 'role_id', 'parent_id', 'gender', 'status', 'updated_at', 'updated_by', 'firsttime_is_member', 'lasttime_is_member', 'created_at'], 'integer'],
            [['bonus_record', 'pv_record'],'number'],
            [['username', 'email', 'fullname', 'bank_account'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 255],
            [['city', 'postal_code', 'identity', 'passport'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['birthday', 'password_repeat'], 'safe'],
            [['username'], 'unique'],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message'=>"Passwords don't match"],
            [['rank_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRank::className(), 'targetAttribute' => ['rank_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRoles::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'rank_id' => 'Rank',
            'currency_id' => 'Currency',
            'role_id' => 'Role',
            'fullname' => 'Fullname',
            'address' => 'Address',
            'city' => 'City',
            'country_id' => 'Country',
            'parent_id' => 'Parent User',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'status' => 'Status',
            'identity' => 'Identity',
            'passport' => 'Passport',
            'bank_id' => 'Bank',
            'bank_account' => 'Bank Account',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblPaidTransactions()
    {
        return $this->hasMany(TblPaidTransaction::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblProductOrders()
    {
        return $this->hasMany(TblProductOrders::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblCountry()
    {
        return $this->hasOne(TblCountry::className(), ['id' => 'country_id']);
    }
    public function getTblBanks()
    {
        return $this->hasOne(TblBanks::className(), ['id' => 'bank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblCurrency()
    {
        return $this->hasMany(TblCurrency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTransactions()
    {
        return $this->hasMany(TblTransactions::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRank()
    {
        return $this->hasOne(TblRank::className(), ['id' => 'rank_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(TblRoles::className(), ['id' => 'role_id']);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function sadminLogin($username, $password, $rememberMe)
    {
        $password = generalHelper::fnEncrypt($password);

        $x = static::findOne(['username' => $username, 'password' => $password, 'status' => self::STATUS_ACTIVE, 'role_id' => self::getRoleID(true)]);
        if($x != null){
            return Yii::$app->user->login($x, $rememberMe ? 3600 * 24 * 30 : 0);
        }
        else
            return false;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function memLogin($username, $password, $rememberMe)
    {
        $password = generalHelper::fnEncrypt($password);
        $x = static::findOne(['username' => $username, 'password' => $password, 'status' => self::STATUS_ACTIVE, 'role_id' => self::getRoleID(false)]);
        if($x != null){
            return Yii::$app->user->login($x, $rememberMe ? 3600 * 24 * 30 : 0);
        }
        else
            return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblWallets()
    {
        return $this->hasMany(TblWallets::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // ...custom code here...                
            if($insert)
                $this->created_at = time();
            if($this->birthday != "")
                $this->birthday = strtotime($this->birthday);
            return true;
        } else {
            return false;
        }
    }

    public static function getRoleID($isAdmin){
        if($isAdmin)
            return [1, 2, 5];
        else
            return [3, 4];
    }
}