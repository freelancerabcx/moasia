<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\helpers\processHelper;
use common\models\LoginForm;
use common\models\User;
use common\models\TblPaidTransaction;
use common\models\TblWallets;
use common\models\TblRank;
use common\models\TblConfig;
use common\models\TblHistoryBonus;
use common\models\TblProductOrders;
use common\models\TblCountry;
use common\models\TblHistoryDay;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','proccess-mln'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionProccessMln(){
        $countries = TblCountry::find()->all();
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction(); 
        try {
            echo "Starting Monoleg!<br>";
            //Lấy ra toàn bộ user có số tiền lớn hơn 2 lần rank
            $user_bonus = User::find()->where(['>','bonus_record',300])->all();
            $config = TblConfig::getConfig( "rank_" );
            foreach($user_bonus as $user){
                switch($user->rank_id){
                    case TblRank::BRONZE:
                        $range = $config['rank_bronze'];
                        break;
                    case TblRank::SILVER:
                        $range = $config['rank_silver'];
                        break;
                    case TblRank::GOLD:
                        $range = $config['rank_gold'];
                        break;
                    case TblRank::PLATINUM:
                        $range = $config['rank_platinum'];
                        break;
                    case TblRank::DIAMOND:
                        $range = $config['rank_diamond'];
                        break;
                    default:
                        break;
                }
                //Trừ bonus record và pv record nếu lớn hơn 2 lần
                if($user->bonus_record >= $range*2 ){
                    $user->bonus_record -= $range*2;
                    $user->pv_record -= $range;
                    $user->save();
                }
            }
            foreach($countries as $country){
                $users = User::find()
                        ->select([
                            'tbl_users.*',
                            'SUM(con_amount) AS total'
                        ])
                        ->andWhere(['=','tbl_users.country_id',$country->id])
                        ->leftJoin('tbl_product_orders','`tbl_product_orders`.`user_id` = `tbl_users`.`id`')
                        ->groupBy('`tbl_users`.`id`')
                        ->all();
                $total_obj = processHelper::mlnListUser($users,$country);
                $countryAna = processHelper::countryAna($country->id);
                $history = new TblHistoryDay();
                $history->income = $countryAna['order_today'];
                $history->after_bonus = $countryAna['bonus_today'];
                $history->monoleg = $country->bonus_MNL_total * $countryAna['bonus_today'] / 100;
                $history->country_id = $country->id;
                $history->created_at = time();
                $history->save();
                processHelper::calMln($total_obj['bronze'],$total_obj['unit'],$country);
                processHelper::calMln($total_obj['silver'],$total_obj['unit'],$country);
                processHelper::calMln($total_obj['gold'],$total_obj['unit'],$country);
                processHelper::calMln($total_obj['platinum'],$total_obj['unit'],$country);
                processHelper::calMln($total_obj['diamond'],$total_obj['unit'],$country);
            }
            TblProductOrders::updateAll(['status' => TblProductOrders::IN_DONE],[ 'status' => TblProductOrders::IN_DEEP]);
            echo "Done Monoleg!<br>";
            echo "Starting Mounthy Bonus!..<br>";
            $data = TblProductOrders::find()
                        ->where(['>','voucher_amount',0])
                        ->all();
            foreach($data as $order){
                $date_order = strtotime(date('Y-m-d',$order->created_at));
                $today = strtotime(date('Y-m-d'));
                $day_range_stamp =  ($today - $date_order) / 86400;
                $day_range = $day_range_stamp%14;
                //Nếu ngày là bội của 14 , nhỏ hơn 20 lần bonus , và rank k phải free
                if( $day_range === 0 &&  $day_range_stamp>0 && $day_range <= 20 * 14 && $order->user->rank_id >0){
                    //Lấy ra 1 nửa bonus
                    $monthy_bonus = (($order->voucher_amount * $order->con_amount)/100 * $order->quantity)/2;
                    $user_id = $order->user_id;
                    processHelper::addHistory(0,$user_id,$monthy_bonus,TblHistoryBonus::TYPE_MTB,TblWallets::TYPE_BONUS);
                    processHelper::updateWalletByUser($user_id,TblWallets::TYPE_BONUS,$monthy_bonus);
                }
            }
            echo "Done Mounthy Bonus!<br>";
            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
    
}