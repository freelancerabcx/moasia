<div class="content-box-large">
	<div class="panel-heading row">
		<div class="panel-title">Thống kê tài khoản</div>

		<div class="panel-options">
			<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
			<a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
		</div>
	</div>
	<div class="panel-body">
		<div class="navbar">
			<div class="navbar-inner">
				<div class="content">
					<ul class="nav nav-pills">
						<li class="active"><a href="#tab1" data-toggle="tab">Payment</a></li>
						<li><a href="#tab2" data-toggle="tab">Order</a></li>
						<li><a href="#tab3" data-toggle="tab">Bonus</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="tab-content">
			<div class="tab-pane active" id="tab1">

				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>First Name 1</th>
							<th>Last Name 1</th>
							<th>Username 1</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Mark</td>
							<td>Otto</td>
							<td>@mdo</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Jacob</td>
							<td>Thornton</td>
							<td>@fat</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Larry</td>
							<td>the Bird</td>
							<td>@twitter</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tab-pane" id="tab2">

				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>First Name 2</th>
							<th>Last Name 2</th>
							<th>Username 2</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Mark</td>
							<td>Otto</td>
							<td>@mdo</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Jacob</td>
							<td>Thornton</td>
							<td>@fat</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Larry</td>
							<td>the Bird</td>
							<td>@twitter</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tab-pane" id="tab3">

				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>First Name 3</th>
							<th>Last Name 3</th>
							<th>Username 3</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Mark</td>
							<td>Otto</td>
							<td>@mdo</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Jacob</td>
							<td>Thornton</td>
							<td>@fat</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Larry</td>
							<td>the Bird</td>
							<td>@twitter</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>