<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">
</head>
<body>
<?php $this->beginBody() ?>
    <div class="header">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <!-- Logo -->
                  <div class="logo">
                     <h1><a href="#">Moringga</a></h1>
                  </div>
               </div>
               <div class="col-md-7">
                  <ul class="reset">
                    <li class="item active dropdown">
                      <?= Html::a('Dashboard', ['dashboard'], ['class' => 'dropdown-toggle']) ?>
                    </li>
                    <li class="item dropdown">
                      <?= Html::a('My Order', ['order'], ['class' => 'dropdown-toggle']) ?>
                    </li>
                    <li class="item dropdown">
                      <?= Html::a('My Payment', ['demo'], ['class' => 'dropdown-toggle']) ?>
                    </li>
                    <li class="item dropdown">
                      <?= Html::a('My Bonus', ['demo'], ['class' => 'dropdown-toggle']) ?>
                    </li>
                    <li class="item dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                      <ul class="dropdown-menu animated fadeInUp">
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Logout</a></li>
                      </ul>
                    </li>
                  </ul>
               </div>
            </div>
         </div>
    </div>

    <div class="container page-content">
      <?= $content ?>
    </div>

    <footer>
       <div class="container">
          <div class="copy text-center">
             Copyright 2014 <a href='#'>Website</a>
          </div>
       </div>
    </footer>
    <script src="js/jquery.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>